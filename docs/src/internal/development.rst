.. _development:

*****************
Local Development
*****************

Scripts for running, testing, and building the application are provided as part of the standard configuration. These are
run using YARN and listed in the scripts section of the ``package.json`` file.

To start the app in development mode, run the following from the root of the project directory:

::

    yarn start

The app should then be available at ``http://localhost:8090``. The app will recompile and
restart if you make any edits to the source files. Any linting errors will also be shown in the console.


Backend API Configuration
---------------------------

The UI application requires an instance of ``ska-oso-services`` to save, retrieve and validate SBDs.
This is configured through the ``BACKEND_URL`` environment variable.

The ``yarn start`` script runs two commands: ``make set-dev-env-vars`` to set the URL for the ODT back-end
and ``webpack serve`` to start the development server. The ``make set-dev-env-vars`` set the default

::

    BACKEND_URL=http://<minikube-ip>/<KUBE_NAMESPACE>

This assumes the backend has been deployed via the ``ska-oso-odt-ui`` umbrella chart. The app is then responsible for
knowing the correct API path on this URL.

To use a back-end deployed elsewhere (e.g. localhost), include the back-end URL in the `yarn start`
command, for example

::

    BACKEND_URL=http://localhost/odt/api/v1/sbds yarn start

Other Yarn commands
--------------------

::

    yarn skao:update

yarn will update the repository with the latest SKAO libraries

::

    yarn cypress


Launches Cypress which has been set up to provide component testing. For further information on the use of Cypress, see https://docs.cypress.io/guides/component-testing/overview

::

    yarn test

Launches the test runner in the interactive watch mode. See the [testing](#testing) section for more information.

::

    yarn build

Builds the app for production to the `build` folder. The build is minified and any JSX is transpiled to JavaScript. Your app is ready to be deployed!

::

    yarn audit

Checks the dependencies to see if there are any vulnerabilities.