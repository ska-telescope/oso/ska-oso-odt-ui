.. _deployment_to_kubernetes:

**************************
Deployment to Kubernetes
**************************

The ``ska-oso-odt-ui`` Helm chart will deploy the application with environment variables from a ConfigMap and an
Ingress rule.

The ``ska-oso-odt-ui-umbrella`` Helm chart will deploy ``ska-oso-odt-ui`` and all its dependencies, namely ``ska-oso-services``, an ODA Postgres instance and SKUID.

To deploy the charts, the standard SKAO make targets are used - for example ``make k8s-install-chart``


Configuring Helm values
========================

The umbrella chart will use the default values defined in the dependencies, unless they are explicitly overwritten in the ``ska-oso-odt-ui-umbrella`` ``values.yaml``.

For example, by default ``ska-oso-services`` will store data in Postgres. To configure a more lightweight deployment that stores SBDefinitions on the backend pod's filesystem,
the ``values.yaml`` can be overwritten.

.. code-block:: yaml

    ska-oso-services-umbrella:
      ska-oso-services:
        rest:
          oda:
            backendType: filesystem


