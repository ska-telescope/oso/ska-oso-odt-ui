.. _userguide:

**************
ODT User Guide
**************

Currently, the only part of the ODT that has been implemented is the :ref:`sbdeditor`.

In the future, SBDs will be stored inside Observing Blocks (OBs) which themselves are contained
within projects. If the project started life as a proposal, it will be possible to automatically
generate the SBDs in an OB based on information entered by the user in the proposal.


