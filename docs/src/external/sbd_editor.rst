.. _sbdeditor:

**********
SBD Editor
**********

This page gives a page-by-page description of the ODT UI, including the parameters contained
on each page, their meaning (particularly where this might not be clear) and behaviour of the UI,
including validation. We believe that the UI is fairly intuitive and defaults have been set such
that relatively little needs to be entered in order to define a minimal observation that conforms
to the capabilities expected at AA0.5.

In the absence of the above pages, the ODT contains a temporary start page where it is possible
to

* Create a new SBD (SKA-Mid or SKA-Low)
* Import an SBD from disk or the ODA
* Return to editing an SBD.

Navigation is performed using the breadcrumbs menu at the top of each page. Although not
absolutely necessary, there is a certain logic to entering the information one screen at a
time moving from left to right.

The top of the screen also contains a number of buttons that perform important functions, but
of these, only the ``Save to ODA`` and ``Home`` buttons are currently implemented. The latter
takes a user back to the start page.

General
=======

This page is exactly the same for SKA-Mid and SKA-Low.

The only input that it will be possible to set here is the ``name`` of the SBD and a free-form
text ``description`` of the SBD that might be useful to someone. Software systems will always
identify the SBD by its ``sbd_id`` as this is unique - the name is purely for identification
purposes by a human.

The other information on this page is set automatically by the ODA when the SBD is saved
there and consists of the ``sbd_id`` and various time-stamps. The ``Created by`` and ``Modified by``
fields are currently just set to ``DefaultUser`` - this will change when log-in and authentication
has been added to the OSO tools.

Scripts
=======

This page is exactly the same for SKA-Mid and SKA-Low.

The scripts page allows the user to define the script that will be used to execute the SBD.
The Observation Execution Tool (OET) will run that script which will take its parameters
(targets, correlator set-up and scan sequence) from the SBD.

Scripts can be used that are stored in two locations, one of which is the OET filesystem,
although it is not clear how much this will be used. The more common use-case will be the
pulling of scripts from GitLab and thus ``git`` is the default option.

In order to pull a script from GitLab, it is necessary to define the ``repository`` in which the
script is stored, together with the ``path`` to the script. The path is defined relative to the
root directory of the repository and can include a leading forward slash to indicate root,
although this isn't necessary.

There is currently a single script (``allocate_and_observe_sb.py``) that has been created for
SBD observing. It is stored in the OSO Scripting repository and will run either a Mid or a
Low SBD. The default parameters in the ODT UI are therefore set to use this script without
any user interaction.

If a different script is desired, the ``repository``, ``path`` to the script and ``branch``
(often ``main`` or ``master``) should be given. Alternatively, a specific commit to a GitLab
respository can be defined instead of the branch.

It is possible to define keyword arguments for the scripts using two tables, one for the init
stage of script execution and another for main. For a standard observation, the only parameter
that needs to be entered is ``subarray_id`` (an integer between 1 and 16, although 1 is probably
your best bet at this stage) in the ``init`` stage.

Array
=====

The array page has a certain amount of commonality between the Mid and Low telescopes. The
top row allows the selection of either a standard, pre-defined array (like AA0.5) or a
'custom' option that allows the list of receptors (Mid dishes or Low stations) to be 
edited. If a standard array has been selected, the receptors that make up that array are
shown together with their number.

Low-specific inputs
-------------------

For Low, a lot more information is shown as each station in the array can be separately
configured. Below the array selection, there is a list of stations and if one of these is
selected, the weighting key for that station is shown. This defines the relative weight of
each of the 256 antennas in the station, with a separate weight for each polarisation.

The weighting key can be edited by clicking on the input field which will accept any
text string. The default is ``uniform`` (equal weight for all antennas and polarizations)
and this should not be changed as that is the only value that the telescope-control
software currently recognises.

Targets
=======

This page is nearly identical for SKA-Mid and SKA-Low. The only difference is that it
is not possible to select the five-point field pattern for SKA-Low.

The menu on the left allows the user to add or delete targets - the option to load targets
from file is not yet implemented. Once a target has been added, it appears in the menu with
a randomly generated index to ensure its uniqueness. Clicking on it will then reveal the
individual parameters of that target on the right.

There are currently three separate characteristics of the source that can be defined:
* Target name and coordinates
* Velocity information
* Field pattern

The name and ICRS coordinates must be filled in manually, although it will be possible in the
future to enter the coordinates based on a recognised target name. Instead of ICRS coordinates,
it is possible to select a solar-system object from a pre-defined list, the position of which
will be known to the telescope control system at the time of observation.

The velocity information is not currently used by any part of the SKA software and can be
safely left as it is.

Only two field patterns are available, a single pointing relative to the target position
(``Pointing Centres`` - defaults to 0 offset in R.A. or Dec.) or ``Five Point``, whereby
the dishes will observe at positions above, below and to the sides of a target, as well
as the target position itself. This is for use with pointing calibration and thus is only
available for SKA-Mid.

Signal Processor
================

This page defines a correlator set-up i.e. the centre frequency and bandwidth of spectral
windows (spw) that define frequency ranges over which the correlator will produce correlated
visibilities or form beams for observation of e.g. pulsars. The parameters are significantly
different for SKA-Mid and SKA-Low and thus they will be described separately. The only
commanality is the ability to create multiple correlator set-ups and to select them from a
list on the left.

Two modes are currently supported: ``CORR`` for standard (continuum) correlation and ``PST`` for
pulsar beam-forming, the latter currently for SKA-Low only. The so-called zoom modes are not yet
available, for either telescope.

SKA-Mid
-------

After defining the frequency band (currently only Bands 1 or 2 are supported), the user is able
to add any number of standard-correlation spws. These are defined mainly by their ``centre
frequency` and ``bandwidth`` - the latter must be a multiple of the channel width (13.44 kHz) and
up and down arrows are available which will move user input to the nearest allowed value. The
final user-settable paraneter is the correlator ``averaging time`` - this can take one of ten values
that are selected from a drop-down list with the default being 1.42s.

When a spw is created, it has default parameters that correspond to a wide-bandwidth mode that,
in the case of Band 1, completely covers the available frequency range. For Band 2, the default
bandwidth is just less than 800 MHz, this being the maximum amount of bandwidth that is available
at AA0.5. The spectral ``resolution`` (in frequency and velocity units) is shown in a read-only column
and corresponds to the standard-correlation mode.

Limited validation of inputs is already implemented. As well as the bandwidth validation described
above, the set-up is also validated to check that the spw fits entirely within the frequency band.

A relatively obscure parameter is the ``FS Offset`` at the top-right. The data from each dish are
sent to the correlator having been split into multiple frequency slices, each of which is
approximately 200-MHz wide. The boundaries between the slices are fixed, but can be
shifted up or down in frequency using the FS Offset. This can result in fewer Frequency Slice
Processors (FSPs) being needed to correlate the data e.g. by placing a spw completely inside a FS
instead of crossing a boundary.

SKA-Low
-------

For SKA-Low, a single spw is always present as this telescope will always observe with the 
correlator operating in standard correlation mode. For now, it is only possible to define a
single standard-correlation spw, although the ability to define more than one will be possible
in the future.

The default values correspond to a spw with the maximum bandwidth allowed at AA0.5,
75 MHz, placed close to the centre of the observable frequency band (50-350 MHz). There
are strict and non-obvious rules governing the allowed values of ``bandwidth`` and ``centre
frequency`` and, for example, a ``centre frequency`` of 200 MHz (the exact centre of the
band) is not allowed.

In order to help the user select a valid ``bandwidth`` and ``centre frequency``, these fields have
up and down arrows which will change the input to the nearest valid value. This applies even
if the input is invalid and thus if a value of 200 MHz is entered, clicking the down arrow
will change the input to 199.609375 MHz. Allowed values of the ``bandwidth`` are integer multiples
of 6.25 MHz.

The spectral ``resolution`` (in frequency and velocity units) is shown in a read-only column and the
correlator ``averaging time`` can be selected from a drop-down list. For SKA-Low, there are only two
possible values of this parameter.

For SKA-Low, pulsar-timing observations use the standard-correlation set-up and thus there is
a single button (``usePST``) above the spw table that creates a second spw which is a duplicate
of the CORR one. The displayed spectral resolution and averaging time are values specific to
PST observing and cannot be changed by the user.

Scans
=====

This page is exactly the same for SKA-Mid and SKA-Low.

Once targets have been defined and the signal processor set up, it is possible to combine these
together with a ``scan duration`` to create scan definitions. For each scan definition that is created,
a drop-down menu allows the user to select from a list of targets or signal processor set-ups.

Once the scan definitions have been defined, these can be formed into a scan sequence by moving
them to the right using the arrow buttons and the order of the sequence modified using the up and down
arrows. If a scan definition is to be observed more than once, it must be moved into the sequence
multiple times.

Data Processing
===============

This page is currently blank meaning that the user currently has no control over the contents of the
instructions sent to the SDP subsystem. This will probably change in the future with the user being
allowed to define processing scripts and their parameters, but for now, the OSO Scripting assumes that
a single script (``vis-receive``) will be run by SDP (not shown).
