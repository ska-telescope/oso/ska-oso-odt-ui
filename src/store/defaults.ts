import type { SBDefinitionInput } from '../generated/models/sbdefinition-input';
import { TelescopeType } from '../generated/models/telescope-type';
import {
  DEFAULT_DISH_ALLOCATION_ID,
  DEFAULT_MCCS_ALLOCATION_ID,
  DEFAULT_WEIGHTING_KEY_REF,
  DefaultMidArrayAA05,
  LowArrayConfigTypeEnum,
  LowArrayConfigurationToStationMapping
} from '../models/sbDefinition/array/arrayConfig';
import { SubArrayLOW } from '../generated/models/sub-array-low';
import { Aperture } from '../generated/models/aperture';
import { CBFConfiguration } from '../generated/models/cbfconfiguration';
import { ProjectInput } from '../generated/models/project-input';
import {
  LowBandDefaults,
  MidBandsDefaults,
  MidBand
} from '../models/sbDefinition/signalProcessor/spectralWindow';

export const getDefaultProject = (): ProjectInput => {
  return {
    interface: 'https://schema.skao.int/ska-oso-pdm-prj/0.1',
    name: 'SKA Project',
    author: {
      pis: [],
      cois: []
    },
    obs_blocks: [
      {
        obs_block_id: 'ob-19579',
        name: 'Observing Block 19579',
        sbd_ids: []
      }
    ]
  };
};

export const getDefaultSbDefinition = (
  telescope: TelescopeType
): SBDefinitionInput =>
  telescope === TelescopeType.SkaMid
    ? getDefaultMidSBDefinition()
    : getDefaultLowSBDefinition();

const defaultActivities = {
  observe: {
    kind: 'git',
    path: 'git://scripts/allocate_and_observe_sb.py',
    repo: 'https://gitlab.com/ska-telescope/oso/ska-oso-scripting.git',
    branch: 'master',
    function_args: {
      init: { args: [], kwargs: {} },
      main: { args: [], kwargs: {} }
    }
  }
};

export function getDefaultMidSBDefinition(): SBDefinitionInput {
  return {
    interface: 'https://schema.skao.int/ska-oso-pdm-sbd/0.1',
    telescope: TelescopeType.SkaMid,
    dish_allocations: {
      dish_allocation_id: DEFAULT_DISH_ALLOCATION_ID,
      selected_subarray_definition: 'AA0.5',
      dish_ids: DefaultMidArrayAA05
    },
    csp_configurations: [
      {
        config_id: 'csp-configuration-12754',
        name: 'Config 12754',
        cbf: createDummyCbf(1),
        midcbf: {
          frequency_band: '1',
          subbands: [
            {
              frequency_slice_offset: {
                value: MidBandsDefaults.get(MidBand.BAND_1)!.fsOffsetMhz!,
                unit: 'MHz'
              },
              correlation_spws: []
            }
          ]
        }
      }
    ],
    activities: defaultActivities
  };
}

export function getDefaultLowSBDefinition(): SBDefinitionInput {
  const arrayStations =
    LowArrayConfigurationToStationMapping[LowArrayConfigTypeEnum.AA05P1];
  return {
    interface: 'https://schema.skao.int/ska-oso-pdm-sbd/0.1',
    telescope: TelescopeType.SkaLow,
    mccs_allocation: {
      mccs_allocation_id: DEFAULT_MCCS_ALLOCATION_ID,
      selected_subarray_definition: SubArrayLOW.Aa05,
      subarray_beams: [
        {
          subarray_beam_id: 1,
          apertures: arrayStations.map(
            (stationID) =>
              ({
                station_id: stationID,
                substation_id: 1,
                weighting_key: DEFAULT_WEIGHTING_KEY_REF
              }) as Aperture
          ),
          number_of_channels: 96 // TODO: remove once optional in PDM
        }
      ]
    },
    csp_configurations: [
      {
        config_id: 'csp-configuration-12754',
        name: 'Config 12754',
        lowcbf: {
          do_pst: false,
          correlation_spws: [
            {
              spw_id: 1,
              logical_fsp_ids: [0, 1],
              zoom_factor: 0,
              centre_frequency: LowBandDefaults.centreMhz * 1e6,
              number_of_channels: 96,
              integration_time_ms: 849
            }
          ]
        }
      }
    ],
    activities: defaultActivities
  };
}

export const DEFAULT_INITIALISED_SB: SBDefinitionInput = {
  interface: 'https://schema.skao.int/ska-oso-pdm-sbd/0.1',
  telescope: 'ska_mid'
};

// As of PI24, we are supporting the new ADR-99 format in midcbf but this hasn't been enabled everywhere, so we also fill the old cbf field with a dummy value for now.
export const createDummyCbf = (band: number): CBFConfiguration => ({
  fsps: [
    {
      fsp_id: 1,
      function_mode: 'CORR',
      frequency_slice_id: band === 1 ? 3 : 6,
      integration_factor: 10,
      zoom_factor: 0
    }
  ]
});
