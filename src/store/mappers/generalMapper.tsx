import { GeneralType } from '../../models';
import type { SBDefinitionInput } from '../../generated/models/sbdefinition-input';

export const toGeneral = (sbDefinition: SBDefinitionInput): GeneralType => {
  return {
    name: sbDefinition.name,
    sbdId: sbDefinition.sbd_id,
    version: sbDefinition.metadata?.version,
    createdBy: sbDefinition.metadata?.created_by,
    createdOn: sbDefinition.metadata?.created_on,
    lastModifiedBy: sbDefinition.metadata?.last_modified_by,
    lastModifiedOn: sbDefinition.metadata?.last_modified_on,
    description: sbDefinition.description
  };
};

export function fromGeneral(general: GeneralType): SBDefinitionInput {
  return {
    name: general.name,
    sbd_id: general.sbdId,
    metadata: {
      version: general.version,
      created_by: general.createdBy,
      created_on: general.createdOn,
      last_modified_by: general.lastModifiedBy,
      last_modified_on: general.lastModifiedOn
    },
    description: general.description
  };
}
