import { describe, test, expect } from 'vitest';
import { mapPdmToProject, mapProjectToPdm } from './projectMapper';
import { ProjectInput } from '../../generated/models/project-input';
import { ProjectType } from 'src/models/project/project';

const EXAMPLE_PROJECT_ID: string = 'prj-example-123';

const EXAMPLE_PROJECT: ProjectType = {
  code: EXAMPLE_PROJECT_ID,
  name: 'SKA Project 1',
  type: 'Science',
  status: 'TODO',
  observingBlocks: [
    {
      id: 'observing-block-87693',
      name: 'Observing Block 87693',
      status: 'TODO',
      sbDefinitions: [{ id: 'sbd-test-12345', status: 'TODO' }]
    }
  ]
};

const EXAMPLE_PDM_PROJECT: ProjectInput = {
  prj_id: EXAMPLE_PROJECT_ID,
  name: 'SKA Project 1',
  obs_blocks: [
    {
      obs_block_id: 'observing-block-87693',
      name: 'Observing Block 87693',
      sbd_ids: ['sbd-test-12345']
    }
  ]
};

describe('PDM to Project mappers', () => {
  test('should map PDM Project', () => {
    const result = mapPdmToProject(EXAMPLE_PDM_PROJECT);
    expect(result).toStrictEqual(EXAMPLE_PROJECT);
  });
});

describe('Project to PDM mappers', () => {
  test('should map Project to PDM', () => {
    const result = mapProjectToPdm(EXAMPLE_PROJECT);
    expect(result).toStrictEqual(EXAMPLE_PDM_PROJECT);
  });
});
