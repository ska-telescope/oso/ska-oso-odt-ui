import { describe, it, expect } from 'vitest';
import * as zod from '../../models/sbDefinition/targets/target';
import { ToPdm, FromPdm, pdmCoordinateKind } from './targetsMapper';
import { EquatorialCoordinatesReferenceFrame } from '../../generated/models/equatorial-coordinates-reference-frame';
import { PointingKind } from '../../generated/models/pointing-kind';
import { RadialVelocity as PdmRadialVelocityType } from '../../generated/models/radial-velocity';
import { RadialVelocityDefinition } from '../../generated/models/radial-velocity-definition';
import { RadialVelocityReferenceFrame } from '../../generated/models/radial-velocity-reference-frame';
import { Target as PdmTargetType } from '../../generated/models/target';
import { TelescopeType } from '../../generated/models/telescope-type';
import { useStore } from '../store';
import { EXAMPLE_LOW_ARRAY_CONFIG } from './arrayMapper.test';

const { setPdmSbDefinition } = useStore.getState();

// Sample data for testing
const sampleZodTarget: zod.TargetType = {
  id: 'target-19752',
  name: 'Test Target',
  coordinate: {
    kind: zod.CoordinateKind.ICRS,
    ra: '10:00:00.0',
    dec: '+05:00:00.0'
  },
  radialMotion: {
    kind: zod.RadialMotionKind.REDSHIFT,
    redshift: 0.005
  },
  pointingPattern: {
    kind: zod.FieldPattern.FIVEPOINT,
    offset: 15
  }
};

const samplePdmTarget: PdmTargetType = {
  target_id: 'target-19752',
  name: 'Test Target',
  reference_coordinate: {
    kind: pdmCoordinateKind.Equatorial,
    ra: '10:00:00.0',
    dec: '+05:00:00.0',
    reference_frame: EquatorialCoordinatesReferenceFrame.Icrs
  },
  radial_velocity: {
    redshift: 0.005
  },
  pointing_pattern: {
    active: PointingKind.FivePointParameters,
    parameters: [
      {
        kind: PointingKind.FivePointParameters,
        offset_arcsec: 15
      }
    ]
  }
};

const velocityRadialMotion = {
  kind: zod.RadialMotionKind.VELOCITY,
  velocity: 200,
  referenceFrame: 'lsrk',
  velocityDefinition: 'Radio'
};

// This describe() block written by hand
// the others largely ChatGPT, remove or refine.
describe('Radial velocity tests', () => {
  it('should be possible to convert radialVelocity both directions', () => {
    // This is the initial state of the form when the user has
    // picked REDSHIFT but not entered a value yet
    const initialFormRadialMotion: zod.RadialMotionType = {
      kind: zod.RadialMotionKind.REDSHIFT,
      redshift: 0
    };
    const asPdm = ToPdm._testing.convertRadialMotion(initialFormRadialMotion);
    expect(asPdm).toStrictEqual({
      redshift: 0,
      reference_frame: RadialVelocityReferenceFrame.Barycentric
    });
    const backToZod = FromPdm._testing.convertRadialVelocity(asPdm);
    expect(backToZod).toStrictEqual(initialFormRadialMotion);
  });

  it('should be possible to load from PDM a Velocity RadialMotion', () => {
    // This is the initial state of the form when the user has
    // picked REDSHIFT but not entered a value yet
    const pdmState: Required<PdmRadialVelocityType> = {
      redshift: 0,
      reference_frame: RadialVelocityReferenceFrame.Lsrk,
      definition: RadialVelocityDefinition.Relativistic,
      quantity: { value: 100, unit: 'km / s' }
    };
    const formState = FromPdm._testing.convertRadialVelocity(pdmState);
    const expectedResult: zod.RadialMotionType = {
      kind: zod.RadialMotionKind.VELOCITY,
      velocity: 100,
      referenceFrame: zod.ReferenceFrame.LSRK,
      velocityDefinition: zod.VelocityDefinition.RELATIVISTIC
    };
    expect(formState).toStrictEqual(expectedResult);
  });

  it('should handle radial motion as velocity', () => {
    const result = ToPdm._testing.convertRadialMotion(velocityRadialMotion);
    expect(result.quantity?.value).toBe(200);
    expect(result.reference_frame).toBe(RadialVelocityReferenceFrame.Lsrk);
    expect(result.definition).toBe(RadialVelocityDefinition.Radio);
  });
});

describe('ToPdm.convert', () => {
  it('should convert zod.TargetType to PdmTargetType', () => {
    const result = ToPdm.convert(sampleZodTarget);
    expect(result.target_id).toBe(sampleZodTarget.id);
    expect(result.name).toBe(sampleZodTarget.name);
    expect(result.reference_coordinate?.kind).toBe(
      pdmCoordinateKind.Equatorial
    );
    expect(result.radial_velocity?.redshift).toBe(
      sampleZodTarget.radialMotion.redshift
    );
    expect(result.pointing_pattern?.active).toBe(
      PointingKind.FivePointParameters
    );
  });

  it('should map coordinate kind correctly to Equatorial', () => {
    const icrsCoord = {
      kind: zod.CoordinateKind.ICRS,
      ra: '10:00:00.0',
      dec: '+05:00:00.0'
    };
    const result = ToPdm._testing.convertCoordinate(icrsCoord);
    expect(result.kind).toBe(pdmCoordinateKind.Equatorial);
    expect(result.reference_frame).toBe(
      EquatorialCoordinatesReferenceFrame.Icrs
    );
  });

  it('should handle radial motion as velocity', () => {
    const result = ToPdm._testing.convertRadialMotion(velocityRadialMotion);
    expect(result.quantity.value).toBe(200);
    expect(result.reference_frame).toBe(RadialVelocityReferenceFrame.Lsrk);
    expect(result.definition).toBe(RadialVelocityDefinition.Radio);
  });

  it('should convert pointing pattern with multiple points', () => {
    const multiPointPattern = {
      kind: zod.FieldPattern.POINTINGCENTRES,
      offsets: [{ raOffset: 1.0, decOffset: -1.0 }]
    };

    const result = ToPdm._testing.convertPointingPattern(multiPointPattern);
    expect(result.active).toBe(PointingKind.SinglePointParameters);
    expect(result.parameters[0].offset_x_arcsec).toBe(1.0);
    expect(result.parameters[0].offset_y_arcsec).toBe(-1.0);
  });
});

describe('FromPdm.convert', () => {
  it('should convert PdmTargetType to zod.TargetType', () => {
    const result = FromPdm.convert(samplePdmTarget);
    expect(result.id).toBe(samplePdmTarget.target_id);
    expect(result.coordinate.kind).toBe(zod.CoordinateKind.ICRS);
    expect(result.coordinate.ra).toBe(samplePdmTarget.reference_coordinate.ra);
    expect(result.radialMotion.redshift).toBe(
      samplePdmTarget.radial_velocity.redshift
    );
    expect(result.pointingPattern.offset).toBe(
      samplePdmTarget.pointing_pattern.parameters[0].offset_arcsec
    );
  });

  it('should handle radial velocity with redshift conversion', () => {
    const pdmRadialVelocity = { redshift: 0.002 };
    const result = FromPdm._testing.convertRadialVelocity(pdmRadialVelocity);
    expect(result.kind).toBe(zod.RadialMotionKind.REDSHIFT);
    expect(result.redshift).toBe(0.002);
  });

  it('should convert pointing pattern with single point parameters', () => {
    const pdmPointingPattern = {
      active: PointingKind.SinglePointParameters,
      parameters: [
        {
          kind: PointingKind.SinglePointParameters,
          offset_x_arcsec: 1.5,
          offset_y_arcsec: -1.5
        }
      ]
    };
    const result = FromPdm._testing.convertPointingPattern(pdmPointingPattern);
    expect(result.kind).toBe(zod.FieldPattern.POINTINGCENTRES);
    expect(result.offsets[0].raOffset).toBe(1.5);
    expect(result.offsets[0].decOffset).toBe(-1.5);
  });

  it('should throw error for invalid PDM coordinate kind', () => {
    const invalidCoordinate = { kind: 'InvalidKind' };
    expect(() => FromPdm._testing.convertCoordinate(invalidCoordinate)).toThrow(
      Error
    );
  });

  it('should convert zod.TargetType to PdmTargetType with tied_array_beams when addPstBeam is true', () => {
    setPdmSbDefinition({
      telescope: TelescopeType.SkaLow,
      mccs_allocation: { ...EXAMPLE_LOW_ARRAY_CONFIG }
    });

    const result = ToPdm.convert({ ...sampleZodTarget, addPstBeam: true });
    expect(result.tied_array_beams).toStrictEqual({
      pst_beams: [
        {
          beam_coordinate: {
            dec_str: '+05:00:00.0',
            ra_str: '10:00:00.0',
            reference_frame: 'icrs',
            target_id: 'target-19752_PST'
          },
          beam_id: 1,
          stn_weights: [1.0, 1.0, 1.0, 1.0]
        }
      ]
    });
  });

  it('should convert PdmTargetType with tied_array_beams to zod.TargetType with addPstBeam is true', () => {
    const result = FromPdm.convert({
      ...samplePdmTarget,
      tied_array_beams: { pst_beams: [{ beam_id: 1 }] }
    });
    expect(result.addPstBeam).toBe(true);
  });
});
