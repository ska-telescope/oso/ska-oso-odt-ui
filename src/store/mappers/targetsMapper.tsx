import * as zod from '../../models/sbDefinition/targets/target';
import { useStore } from '../../store/store';
import {
  EquatorialCoordinates,
  EquatorialCoordinatesKindEnum
} from '../../generated/models/equatorial-coordinates';
import { EquatorialCoordinatesReferenceFrame } from '../../generated/models/equatorial-coordinates-reference-frame';
import { HorizontalCoordinatesKindEnum } from '../../generated/models/horizontal-coordinates';
import { PointingKind } from '../../generated/models/pointing-kind';
import { PointingPattern } from '../../generated/models/pointing-pattern';
import { RadialVelocity as PdmRadialVelocityType } from '../../generated/models/radial-velocity';
import { RadialVelocityDefinition } from '../../generated/models/radial-velocity-definition';
import { RadialVelocityQuantityUnitEnum } from '../../generated/models/radial-velocity-quantity';
import { RadialVelocityReferenceFrame } from '../../generated/models/radial-velocity-reference-frame';
import { ReferenceCoordinate } from '../../generated/models/reference-coordinate';
import { SolarSystemObjectKindEnum } from '../../generated/models/solar-system-object';
import { Target as PdmTargetType } from '../../generated/models/target';

//Annoying that the code-generator makes 3 enums with 1 value each...
export const pdmCoordinateKind = {
  ...EquatorialCoordinatesKindEnum,
  ...HorizontalCoordinatesKindEnum,
  ...SolarSystemObjectKindEnum
} as const;

// ChatGPT suggested this 0_0
function invertMap<T extends Record<PropertyKey, PropertyKey>>(
  map: T
): { [K in T[keyof T]]: keyof T } {
  return Object.fromEntries(
    Object.entries(map).map(([key, value]) => [value, key])
  ) as { [K in T[keyof T]]: keyof T };
}

// TODO namespaces seems a good way to split the to/from PDM - should we adopt this pattern in the other mappers?
export namespace ToPdm {
  export const convert = ({
    id,
    name,
    coordinate,
    pointingPattern,
    radialMotion,
    addPstBeam
  }: zod.TargetType): PdmTargetType => {
    let pdmTarget: PdmTargetType = {
      target_id: id,
      name: name,
      reference_coordinate: convertCoordinate(coordinate),
      radial_velocity: convertRadialMotion(radialMotion),
      pointing_pattern: convertPointingPattern(pointingPattern)
    };
    if (coordinate.kind === zod.CoordinateKind.ICRS && addPstBeam) {
      pdmTarget = {
        ...pdmTarget,
        tied_array_beams: convertTiedArrayBeams(pdmTarget)
      };
    }
    return pdmTarget;
  };

  export const referenceFrameMap: Record<
    zod.ReferenceFrameType,
    RadialVelocityReferenceFrame
  > = {
    lsrk: RadialVelocityReferenceFrame.Lsrk,
    bary: RadialVelocityReferenceFrame.Barycentric,
    topo: RadialVelocityReferenceFrame.Topocentric
  };

  export const radialVelocityMap: Record<
    zod.VelocityDefinitionType,
    RadialVelocityDefinition
  > = {
    Radio: RadialVelocityDefinition.Radio,
    Optical: RadialVelocityDefinition.Optical,
    Relativistic: RadialVelocityDefinition.Relativistic
  };

  const convertTiedArrayBeams = (pdmTarget: PdmTargetType) => {
    const stn_weights = useStore
      .getState()
      .sbDefinition.mccs_allocation?.subarray_beams[0].apertures.map(() => 1.0);
    return {
      pst_beams: [
        {
          beam_id: 1,
          beam_coordinate: {
            target_id: `${pdmTarget.target_id}_PST`,
            reference_frame: 'icrs',
            ra_str: (pdmTarget.reference_coordinate as EquatorialCoordinates)
              .ra,
            dec_str: (pdmTarget.reference_coordinate as EquatorialCoordinates)
              .dec
          },
          stn_weights
        }
      ]
    };
  };

  const convertRadialMotion = (
    rm: zod.RadialMotionType
  ): PdmRadialVelocityType => {
    switch (rm.kind) {
      case zod.RadialMotionKind.REDSHIFT:
        return {
          redshift: rm.redshift,
          reference_frame: referenceFrameMap[zod.ReferenceFrame.BARY]
        };
      case zod.RadialMotionKind.VELOCITY:
        return {
          reference_frame:
            referenceFrameMap[rm.referenceFrame as zod.ReferenceFrameType],
          definition:
            radialVelocityMap[
              rm.velocityDefinition as zod.VelocityDefinitionType
            ],
          quantity: {
            value: rm.velocity,
            unit: RadialVelocityQuantityUnitEnum.KmS
          }
        };
    }
  };

  const convertCoordinate = (
    coord: zod.CoordinatesType
  ): ReferenceCoordinate => {
    switch (coord.kind) {
      case zod.CoordinateKind.ICRS:
        return {
          kind: pdmCoordinateKind.Equatorial,
          ra: coord.ra,
          dec: coord.dec,
          reference_frame: EquatorialCoordinatesReferenceFrame.Icrs
        };
      case zod.CoordinateKind.SSO:
        return { kind: pdmCoordinateKind.Special, name: coord.name };
    }
  };

  const convertPointingPattern = (
    point: zod.FieldPatternType
  ): PointingPattern => {
    switch (point.kind) {
      case zod.FieldPattern.FIVEPOINT:
        return {
          active: PointingKind.FivePointParameters,
          parameters: [
            {
              kind: PointingKind.FivePointParameters,
              offset_arcsec: point.offset
            }
          ]
        };
      case zod.FieldPattern.POINTINGCENTRES:
        return {
          active: PointingKind.SinglePointParameters,
          parameters: point.offsets.map(({ raOffset, decOffset }) => ({
            kind: PointingKind.SinglePointParameters,
            offset_x_arcsec: raOffset,
            offset_y_arcsec: decOffset
          }))
        };
    }
  };

  export const _testing = {
    convertCoordinate: convertCoordinate,
    convertPointingPattern: convertPointingPattern,
    convertRadialMotion: convertRadialMotion
  };
}

export namespace FromPdm {
  export const convert = ({
    target_id,
    name,
    reference_coordinate,
    radial_velocity,
    pointing_pattern,
    tied_array_beams
  }: PdmTargetType): zod.TargetType => {
    const res = {
      id: target_id || '',
      name: name || '',
      coordinate: convertCoordinate(reference_coordinate!),
      radialMotion: convertRadialVelocity(radial_velocity!),
      pointingPattern: convertPointingPattern(pointing_pattern!),
      addPstBeam: tied_array_beams?.pst_beams?.length > 0
    };
    return res;
  };

  const reverseReferenceFrameMap = invertMap(ToPdm.referenceFrameMap);

  const reverseRadialVelocityMap = invertMap(ToPdm.radialVelocityMap);

  const convertCoordinate = (
    coord: ReferenceCoordinate
  ): zod.CoordinatesType => {
    switch (coord.kind) {
      case pdmCoordinateKind.Sso:
      case pdmCoordinateKind.Special:
        return { kind: zod.CoordinateKind.SSO, name: coord.name };
      case pdmCoordinateKind.Equatorial:
        return {
          kind: zod.CoordinateKind.ICRS,
          // String() coerce here is a code smell.
          // What's wrong with the generated types?
          ra: String(coord.ra!),
          dec: String(coord.dec!)
        };
      default:
        throw new Error(
          `Invalid PDM coordinate, unable to convert: ${coord.kind}`
        );
    }
  };

  const convertRadialVelocity = (
    rv: PdmRadialVelocityType
  ): zod.RadialMotionType => {
    if (rv.redshift !== undefined && !rv.quantity?.value) {
      return { redshift: rv.redshift, kind: zod.RadialMotionKind.REDSHIFT };
    } else {
      return {
        kind: zod.RadialMotionKind.VELOCITY,
        velocity: rv.quantity?.value ?? 0, // FIXME: Convert units here!
        referenceFrame:
          reverseReferenceFrameMap[
            rv.reference_frame ?? RadialVelocityReferenceFrame.Lsrk
          ],
        velocityDefinition:
          reverseRadialVelocityMap[
            rv.definition ?? RadialVelocityDefinition.Optical
          ]
      };
    }
  };

  const convertPointingPattern = (
    pp: PointingPattern
  ): zod.FieldPatternType => {
    const activeParams = pp.parameters!.find((p) => p.kind == pp.active);
    if (activeParams === undefined) {
      throw new Error(
        'Invalid PDM pointing pattern. No parameters match active'
      );
    }
    switch (activeParams.kind) {
      case PointingKind.FivePointParameters:
        return {
          kind: zod.FieldPattern.FIVEPOINT,
          offset: activeParams.offset_arcsec ?? 0
        };

      case PointingKind.SinglePointParameters:
        return {
          kind: zod.FieldPattern.POINTINGCENTRES,
          offsets: [
            {
              raOffset: activeParams.offset_x_arcsec ?? 0,
              decOffset: activeParams.offset_y_arcsec ?? 0
            }
          ]
        };
      default:
        throw new Error(
          `Unable to convert pointing pattern: '${activeParams.kind}'`
        );
    }
  };

  export const _testing = {
    convertCoordinate: convertCoordinate,
    convertPointingPattern: convertPointingPattern,
    convertRadialVelocity: convertRadialVelocity
  };
}
