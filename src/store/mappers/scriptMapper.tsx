import {
  ArgFormatType,
  DEFAULT_GIT_BRANCH,
  ScriptKind,
  ScriptType
} from '../../models/sbDefinition/script/script';
import { Activities } from '../../generated/models/activities';

const DEFAULT_ACTIVITY_NAME = 'observe';

const toScriptArgs = (activityArgs: Record<string, string | number>) => {
  return Object.keys(activityArgs).map(
    (keyWord: string) =>
      ({
        kw: keyWord,
        value: activityArgs[keyWord]
      }) as ArgFormatType
  );
};

const fromScriptArgs = (scriptArgs: ArgFormatType[] | undefined) => {
  if (!scriptArgs || scriptArgs.length == 0) {
    return {};
  }
  const activityArgs: Record<string, string | number> = {};
  scriptArgs.forEach(
    (arg: ArgFormatType) => (activityArgs[arg.kw] = arg.value)
  );
  return activityArgs;
};

export const toScript = (
  activities: Record<string, Activities>
): ScriptType => {
  if (!Object.hasOwn(activities, DEFAULT_ACTIVITY_NAME)) {
    return {};
  }
  const activity = activities[DEFAULT_ACTIVITY_NAME];
  const initArgs = toScriptArgs(activity.function_args?.init.kwargs ?? {});
  const mainArgs = toScriptArgs(activity.function_args?.main.kwargs ?? {});
  switch (activity.kind) {
    case 'filesystem': {
      return {
        kind: ScriptKind.filesystem,
        path: activity.path.slice(7),
        initArgs: initArgs ?? [],
        mainArgs: mainArgs ?? []
      };
    }
    case 'git': {
      return {
        kind: ScriptKind.git,
        path: activity.path.slice(6),
        repo: activity.repo,
        branch: activity.branch ?? DEFAULT_GIT_BRANCH,
        commit: activity.commit ?? '',
        selected: activity.branch ? 'branch' : 'commit',
        initArgs: initArgs ?? [],
        mainArgs: mainArgs ?? []
      };
    }
    default: {
      throw new Error(`Activity type ${activity.kind} not supported`);
    }
  }
};

export const fromScript = (script: ScriptType): Record<string, Activities> => {
  const activityArgs = {
    init: {
      args: [],
      kwargs: fromScriptArgs(script.initArgs)
    },
    main: {
      args: [],
      kwargs: fromScriptArgs(script.mainArgs)
    }
  };
  switch (script.kind) {
    case ScriptKind.filesystem: {
      return {
        [DEFAULT_ACTIVITY_NAME]: {
          kind: ScriptKind.filesystem,
          path: 'file://' + script.path,
          function_args: activityArgs
        }
      };
    }
    case ScriptKind.git: {
      return {
        [DEFAULT_ACTIVITY_NAME]: {
          kind: ScriptKind.git,
          path: 'git://' + script.path,
          repo: script.repo,
          ...(script.selected == 'branch'
            ? { branch: script.branch }
            : { commit: script.commit }),
          function_args: activityArgs
        }
      };
    }
  }
};
