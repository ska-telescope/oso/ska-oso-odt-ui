import { CSPConfiguration } from '../../generated/models/cspconfiguration';
import { LowSignalProcessorType, MidSignalProcessorType } from '../../models';
import { velocityEquivalentKmsDisplayValue } from '../../lib/astro';
import {
  LOW_CONTINUUM_CHANNEL_WIDTH_KHZ,
  LOW_PST_CHANNEL_WIDTH_KHZ,
  LOW_STATION_CHANNEL_WIDTH_MHZ,
  MID_CHANNEL_WIDTH_KHZ
} from '../../lib/osd';

import { createDummyCbf } from '../defaults';
import { SpectralWindowMode } from '../../models/sbDefinition/signalProcessor/spectralWindow';

export const toLowSignalProcessor = (
  cspConfiguration: CSPConfiguration
): LowSignalProcessorType => {
  const spectralWindows = cspConfiguration.lowcbf?.correlation_spws?.map(
    (correlationSpw) => ({
      spwId: correlationSpw.spw_id,
      mode: 'CORR',
      centreFrequencyMhz: correlationSpw.centre_frequency
        ? correlationSpw.centre_frequency * 1e-6
        : '',
      bandwidthMhz: parseFloat(
        (
          correlationSpw.number_of_channels * LOW_STATION_CHANNEL_WIDTH_MHZ
        ).toFixed(5)
      ),
      resolution: getLowContinuumResolutionDisplayValue(
        correlationSpw.centre_frequency
      ),
      averageTimeS: parseFloat(
        (correlationSpw.integration_time_ms * 1e-3).toFixed(3)
      )
    })
  );

  if (cspConfiguration.lowcbf.do_pst) {
    // As of PI24 we expect Low to always just have 1 correlation spw
    const correlationSpectralWindow = spectralWindows[0];
    spectralWindows?.push({
      spwId: 2,
      mode: 'PST',
      centreFrequencyMhz: correlationSpectralWindow?.centreFrequencyMhz,
      bandwidthMhz: correlationSpectralWindow.bandwidthMhz,
      resolution: getLowPstResolutionDisplayValue(
        correlationSpectralWindow.centreFrequencyMhz * 1e6
      ),
      averageTimeS: 0.207
    });
  }

  return {
    id: cspConfiguration.config_id,
    name: cspConfiguration.name,
    doPst: cspConfiguration.lowcbf.do_pst,
    spectralWindows
  };
};

export const toMidSignalProcessor = (
  cspConfiguration: CSPConfiguration
): MidSignalProcessorType => {
  // As of PI24, we assume there is only one subband
  const firstSubband = (cspConfiguration.midcbf?.subbands ?? [])[0];
  const spectralWindows = (firstSubband.correlation_spws ?? []).map(
    (correlationSpw) => ({
      spwId: correlationSpw.spw_id,
      mode: 'CORR',
      centreFrequencyMhz: correlationSpw.centre_frequency
        ? parseFloat((correlationSpw.centre_frequency * 1e-6).toFixed(5))
        : '',
      bandwidthMhz: parseFloat(
        (
          correlationSpw.number_of_channels *
          MID_CHANNEL_WIDTH_KHZ *
          1e-3
        ).toFixed(5)
      ),
      resolution: getMidResolutionDisplayValue(correlationSpw.centre_frequency),
      averageTimeFactor: correlationSpw.time_integration_factor
    })
  );
  return {
    id: cspConfiguration.config_id,
    name: cspConfiguration.name,
    band: cspConfiguration.midcbf?.frequency_band ?? 1,
    fsOffsetMhz: firstSubband.frequency_slice_offset?.value,
    spectralWindows
  };
};

export const fromLowSignalProcessor = (
  lowSignalProcessor: LowSignalProcessorType
): CSPConfiguration => {
  return {
    config_id: lowSignalProcessor.id,
    name: lowSignalProcessor.name,
    lowcbf: {
      do_pst: lowSignalProcessor.doPst,
      correlation_spws: lowSignalProcessor.spectralWindows
        ?.filter(
          (spectralWindow) =>
            spectralWindow.mode === SpectralWindowMode.enum.CORR
        ) // The PST row isn't actually saved in the PDM, as we just store the doPst flag and the information in the row can be derived.
        .map((spectralWindow, index) => ({
          spw_id: index + 1,
          number_of_channels: Math.round(
            spectralWindow.bandwidthMhz / LOW_STATION_CHANNEL_WIDTH_MHZ
          ),
          centre_frequency: spectralWindow.centreFrequencyMhz * 1e6,
          integration_time_ms: parseFloat(
            (spectralWindow.averageTimeS * 1e3).toFixed(3)
          ),
          zoom_factor: 0,
          logical_fsp_ids: []
        }))
    }
  };
};

export const fromMidSignalProcessor = (
  midSignalProcessor: MidSignalProcessorType
): CSPConfiguration => {
  return {
    config_id: midSignalProcessor.id,
    name: midSignalProcessor.name,
    cbf: createDummyCbf(midSignalProcessor.band),
    midcbf: {
      frequency_band: midSignalProcessor.band,
      // As of PI24, we assume there is only one subband
      subbands: [
        {
          frequency_slice_offset: {
            value: midSignalProcessor.fsOffsetMhz,
            unit: 'MHz'
          },
          correlation_spws: midSignalProcessor.spectralWindows?.map(
            (spectralWindow, index) => ({
              spw_id: index + 1,
              number_of_channels: Math.round(
                spectralWindow.bandwidthMhz / (MID_CHANNEL_WIDTH_KHZ * 1e-3)
              ),
              centre_frequency: spectralWindow.centreFrequencyMhz * 1e6,
              time_integration_factor: spectralWindow.averageTimeFactor,
              zoom_factor: 0,
              logical_fsp_ids: []
            })
          )
        }
      ]
    }
  };
};

export const getMidResolutionDisplayValue = (centreFrequencyHz: number) =>
  `${MID_CHANNEL_WIDTH_KHZ} (${velocityEquivalentKmsDisplayValue(MID_CHANNEL_WIDTH_KHZ * 1e3, centreFrequencyHz)})`;

export const getLowContinuumResolutionDisplayValue = (centreFrequencyHz) =>
  `${LOW_CONTINUUM_CHANNEL_WIDTH_KHZ.toFixed(2)} (${velocityEquivalentKmsDisplayValue(LOW_CONTINUUM_CHANNEL_WIDTH_KHZ * 1e3, centreFrequencyHz)})`;

export const getLowPstResolutionDisplayValue = (centreFrequencyHz) =>
  `${LOW_PST_CHANNEL_WIDTH_KHZ.toFixed(2)} (${velocityEquivalentKmsDisplayValue(LOW_PST_CHANNEL_WIDTH_KHZ * 1e3, centreFrequencyHz)})`;
