import React from 'react';
import { Route, Routes } from 'react-router-dom';

import NoMatch from '../../pages/noMatch';
import {
  SbdEditorLanding,
  Scripts,
  SignalProcessor,
  Array,
  General,
  Targets,
  DataProcessing,
  Scans,
  SbdEditor
} from '../../pages/sbd';
import { Project } from '../../pages/project/project';
import { ObservingBlock } from '../../pages/project/observingBlock';
import { Home } from '../../pages/home';

export const RoutePaths = {
  Home: '/',
  ImportSb: '/importsb',
  Project: '/project',
  ObservingBlock: '/project/obs-block',
  SbdEditor: {
    root: '/sbd/*',
    Admin: '/sbd/admin',
    General: '/sbd/general',
    Array: '/sbd/array',
    Targets: '/sbd/targets',
    DataProcessing: '/sbd/dataprocessing',
    Scans: '/sbd/scans',
    Scripts: '/sbd/scripts',
    SignalProcessor: '/sbd/signalprocessor'
  }
};

export const OdtRoutes = () => {
  // This is the top level set of routes for the application
  return (
    <Routes>
      <Route path={RoutePaths.Home} element={<Home />} />
      <Route path={RoutePaths.Project} element={<Project />} />
      <Route path={RoutePaths.ObservingBlock} element={<ObservingBlock />} />
      <Route path={RoutePaths.SbdEditor.Admin} element={<SbdEditorLanding />} />
      <Route path={RoutePaths.SbdEditor.root} element={<SbdEditor />} />
      <Route path="*" element={<NoMatch />} />
    </Routes>
  );
};

export const SbdEditorRoutes = () => {
  // These routes are for the SbdEditor, so are expected to be nested within the /sbd path already
  const subPath = (path: string) => path.replace('/sbd', '');
  return (
    <Routes>
      <Route
        path={subPath(RoutePaths.SbdEditor.General)}
        element={<General />}
      />
      <Route
        path={subPath(RoutePaths.SbdEditor.Scripts)}
        element={<Scripts />}
      />
      <Route path={subPath(RoutePaths.SbdEditor.Array)} element={<Array />} />
      <Route
        path={subPath(RoutePaths.SbdEditor.Targets)}
        element={<Targets />}
      />
      <Route path={subPath(RoutePaths.SbdEditor.Scans)} element={<Scans />} />
      <Route
        path={subPath(RoutePaths.SbdEditor.SignalProcessor)}
        element={<SignalProcessor />}
      />
      <Route
        path={subPath(RoutePaths.SbdEditor.DataProcessing)}
        element={<DataProcessing />}
      />
    </Routes>
  );
};
