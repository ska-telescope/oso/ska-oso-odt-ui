import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';

import App from './App';

describe('App', () => {
  test('should return appropriate components', () => {
    render(<App />);

    const header = screen.getByLabelText('SKAOHeader');
    expect(header).toBeInTheDocument();
    expect(header).toBeVisible();

    const skoImg = screen.getByRole('img', { name: /skao logo/i });
    expect(skoImg).toBeInTheDocument();
    expect(skoImg).toBeVisible();
    expect(skoImg).toHaveAttribute(
      'aria-describedby',
      'svg-title svg-description'
    );
    expect(skoImg).toHaveAttribute('viewBox', '-60 30 70 100');

    const siteButton = screen.getByLabelText('skaWebsite');
    expect(siteButton).toBeInTheDocument();
    expect(siteButton).toBeVisible();
    expect(siteButton).toHaveAttribute('type', 'button');
    expect(siteButton).toHaveAttribute('id', 'skaWebsite');
    expect(siteButton).toHaveAttribute(
      'data-mui-internal-clone-element',
      'true'
    );

    const heading = screen.getByRole('heading', {
      name: /\| observation design tool/i
    });
    expect(heading).toBeInTheDocument();
    expect(heading).toBeVisible();

    const documentButton = screen.getByRole('button', {
      name: /document icon/i
    });
    expect(documentButton).toBeInTheDocument();
    expect(documentButton).toBeVisible();
    expect(documentButton).toHaveAttribute('aria-label', 'document icon');
    expect(documentButton).toHaveAttribute('type', 'button');

    const themeButton = screen.getByRole('button', {
      name: /light\/dark mode/i
    });
    expect(themeButton).toBeInTheDocument();
    expect(themeButton).toBeVisible();
    expect(themeButton).toHaveAttribute('type', 'button');
    expect(themeButton).toHaveAttribute(
      'data-mui-internal-clone-element',
      'true'
    );
  });
});
