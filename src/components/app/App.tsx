import { useState } from 'react';
import { CssBaseline, Paper, ThemeProvider } from '@mui/material';
import {
  CopyrightModal,
  Footer,
  Spacer,
  SPACER_VERTICAL,
  THEME_DARK,
  THEME_LIGHT
} from '@ska-telescope/ska-gui-components';
import { version as releaseVersion } from '../../../package.json';
import theme from '../../services/theme/theme';
import { OdtHeader } from '../index';
import { fetchOsd } from '../../lib/osd';
import { AlertDialog } from '../shared/dialogs';
import { OdtRoutes } from './routes';
import { BrowserRouter as Router } from 'react-router-dom';

const HEADER_HEIGHT = 70;
const FOOTER_HEIGHT = 20;

const App = () => {
  const [showCopyright, setShowCopyright] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');
  const [themeMode, setThemeMode] = useState(THEME_LIGHT);

  const toggleTheme = () =>
    themeMode === THEME_LIGHT
      ? setThemeMode(THEME_DARK)
      : setThemeMode(THEME_LIGHT);

  const version = releaseVersion;
  const basename = window.env?.BASE_URL;

  fetchOsd().then((result) => {
    if (result.error) {
      setErrorMessage(result.error);
    }
  });

  return (
    <ThemeProvider theme={theme(themeMode)}>
      <AlertDialog
        open={!!errorMessage}
        title="Error while fetching OSD data."
        content={errorMessage}
        onClose={() => setErrorMessage('')}
      />
      <CssBaseline enableColorScheme />
      <CopyrightModal
        copyrightFunc={(value: boolean) => setShowCopyright(value)}
        show={showCopyright}
      />
      <OdtHeader
        title="Observation Design Tool"
        skao="SKAO Website"
        themeMode={themeMode}
        toggleThemeMode={toggleTheme}
      />
      <Paper>
        <Spacer size={HEADER_HEIGHT} axis={SPACER_VERTICAL} />
        <Router
          basename={basename}
          future={{
            v7_relativeSplatPath: true,
            v7_startTransition: true,
            v7_fetcherPersist: true,
            v7_normalizeFormMethod: true,
            v7_partialHydration: true,
            v7_skipActionStatusRevalidation: true
          }}
        >
          <OdtRoutes />
        </Router>
        <Spacer size={FOOTER_HEIGHT} axis={SPACER_VERTICAL} />
      </Paper>
      <Footer testId="footerId" version={`Version ${version}`} />
    </ThemeProvider>
  );
};

export default App;
