import React from 'react';
import {
  Button,
  ButtonColorTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';
import { RoutePaths } from '../app/routes';

export const HomeButton = (): JSX.Element => {
  const gotoHome = () => {
    const base_url = window.env?.BASE_URL;
    const homeHref = base_url
      ? (base_url.substring(0, base_url.length - 1) || '') + RoutePaths.Home
      : '';
    if (window.location.pathname !== homeHref) {
      const server_base_url = window.location.href.replace(
        window.location.pathname,
        ''
      );
      window.location.href = server_base_url + homeHref;
    }
  };
  return (
    <Button
      testId="btnHome"
      ariaDescription="Click to go to the home page"
      label="Home"
      name="Home"
      onClick={(e) => {
        e.preventDefault();
        gotoHome();
      }}
      color={ButtonColorTypes.Inherit}
      variant={ButtonVariantTypes.Contained}
    />
  );
};
