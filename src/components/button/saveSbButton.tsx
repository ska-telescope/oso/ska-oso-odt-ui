import React, { useState } from 'react';
import {
  Button,
  ButtonColorTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';
import type { SBDefinitionInput } from '../../generated/models/sbdefinition-input';
import apiService from '../../services/apiService';
import { Alert, Snackbar } from '@mui/material';
import { AlertDialog } from '../shared/dialogs';
import { useStore } from '../../store/store';
import { useFormContext } from 'react-hook-form';
import { toSbDefinition } from '../../store/mappers/sbDefinitionMapper';

export const SaveSbButton = () => {
  const [successAlert, setSuccessAlert] = useState(false);
  const [errorMessage, setErrorMessage] = useState('');

  const { reset } = useFormContext();

  const [sbDefinition, setPdmSbDefinition] = useStore((store) => [
    store.sbDefinition,
    store.setPdmSbDefinition
  ]);

  const handleSubmitSb = async () => {
    try {
      const response = sbDefinition.sbd_id
        ? await apiService.updateSBFn(sbDefinition)
        : await apiService.saveSBFn(sbDefinition);

      if (response.data) {
        // Reset the form state to reflect the API response
        reset(toSbDefinition(response.data as SBDefinitionInput));
        setPdmSbDefinition(response.data as SBDefinitionInput);
        setSuccessAlert(true);
      } else {
        setErrorMessage(response.error!);
      }
      setTimeout(() => {}, 2000);
    } catch (e) {
      setErrorMessage(e?.message);
    }
  };

  return (
    <div>
      <Snackbar
        open={successAlert}
        onClose={() => setSuccessAlert(false)}
        autoHideDuration={3000} // Automatically hide after 3 seconds
        anchorOrigin={{ vertical: 'top', horizontal: 'center' }} // Position
      >
        <Alert severity="success" variant="filled">
          SBDefinition has been saved successfully to the ODA.
        </Alert>
      </Snackbar>
      <AlertDialog
        open={!!errorMessage}
        title="Error while saving to the ODA."
        content={errorMessage}
        onClose={() => setErrorMessage('')}
      />
      <Button
        testId="save-sb"
        ariaDescription="Click to save the changes back to backend"
        label="Save to ODA"
        onClick={handleSubmitSb}
        color={ButtonColorTypes.Secondary}
        variant={ButtonVariantTypes.Contained}
      />
    </div>
  );
};
