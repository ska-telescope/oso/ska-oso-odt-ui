import React from 'react';
import {
  Button,
  ButtonColorTypes,
  ButtonVariantTypes
} from '@ska-telescope/ska-gui-components';

export const MarkAsReady = () => {
  return (
    <Button
      testId="mark-sbd-as-ready"
      ariaDescription="Mark the current SBDefinition as ready."
      label="Mark as ready"
      color={ButtonColorTypes.Success}
      variant={ButtonVariantTypes.Contained}
      disabled
    />
  );
};
