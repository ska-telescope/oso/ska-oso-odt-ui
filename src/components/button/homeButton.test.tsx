import '@testing-library/jest-dom';
import React from 'react';
import { act, fireEvent, render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { HomeButton } from './homeButton';

describe('HomeButton', () => {
  test('should return home button and appropriate attributes', () => {
    render(<HomeButton />);

    const homeButton = screen.getByRole('button', { name: /home/i });

    // home button
    expect(homeButton).toBeInTheDocument();
    expect(homeButton).toBeVisible();
    expect(homeButton).toHaveAttribute('aria-label', 'Home');
    expect(homeButton).toHaveAttribute('data-testid', 'btnHome');
  });
  test('should return home button and appropriate attributes', () => {
    window.env = { BASE_URL: '/' };
    render(<HomeButton />);

    const homeButton = screen.getByRole('button', { name: /home/i });

    act(() => {
      fireEvent.click(homeButton);
    });
    // redirect to the home page
    expect(window.location.href).eq('http://localhost:3000/');
  });
});
