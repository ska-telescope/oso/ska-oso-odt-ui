import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { ValidateSB } from './validateSB';

describe('ValidateSB', () => {
  test('should return Validate button with appropriate components', () => {
    render(<ValidateSB />);

    const validateButton = screen.getByRole('button', {
      name: 'Validate'
    });
    expect(validateButton).toBeInTheDocument();
    expect(validateButton).toHaveAttribute('type', 'submit');
    expect(validateButton).toHaveAttribute(
      'aria-describedby',
      'Click to validate current SB'
    );
    expect(validateButton).toHaveAttribute('data-testid', 'validate-sb');
  });
});
