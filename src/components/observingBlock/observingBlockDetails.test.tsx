import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import {
  WithProjectFormContext,
  TEST_PROJECT_FORM
} from 'src/components/testUtils';
import React from 'react';
import { ObservingBlockDetails } from './observingBlockDetails';

describe('ObservingBlockDetails', () => {
  test('should display the values from the form', () => {
    render(
      <WithProjectFormContext initialState={TEST_PROJECT_FORM}>
        <ObservingBlockDetails />
      </WithProjectFormContext>
    );
    const firstObservingBlock = TEST_PROJECT_FORM.observingBlocks[0];
    expect(screen.getByLabelText('Name').value).eq(firstObservingBlock.name);
    expect(screen.getByLabelText('Status').value).eq(
      firstObservingBlock.status
    );
  });
});
