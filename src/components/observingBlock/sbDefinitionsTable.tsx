import React, { useState } from 'react';
import {
  createTableDataFromFormValues,
  DisplayType,
  OsoDataTableColumnDefinitions,
  OsoFieldSet
} from '../shared';
import Grid from '@mui/material/Grid2';
import { OsoDataTable } from '../shared';
import { useFormContext } from 'react-hook-form';
import { useNavigate } from 'react-router-dom';
import { RoutePaths } from '../app/routes';
import { CreateSbDefinitionInProjectButton } from './createSbDefinitionInProjectButton';
import { SBDefinitionInput } from '../../generated/models/sbdefinition-input';
import apiService from '../../services/apiService';
import { useStore } from '../../store/store';
import { useProjectStore } from '../../store/projectStore';
import { AlertDialog } from '../shared/dialogs';
import { TelescopeType } from '../../generated/models/telescope-type';

export const SbDefinitionsTable = (): JSX.Element => {
  const [error, setError] = useState('');
  const obsBlockIndex = useProjectStore(
    (state) => state.inProgressObservingBlock!.index
  );
  const setPdmSbDefinition = useStore((state) => state.setPdmSbDefinition);
  const { getValues, setValue } = useFormContext();

  const ctrlName = `observingBlocks.${obsBlockIndex}.sbDefinitions`;

  const navigate = useNavigate();

  const deleteSbDefinitionFromObservingBlock = (index: number) => {
    setValue(ctrlName, getValues(ctrlName).toSpliced(index, 1));
  };

  const findSbDefinitionIdByIndex = (sbDefinitionIndex: number): string =>
    getValues(ctrlName)[sbDefinitionIndex].id;

  // TODO could this kind of functionality be made into a custom hook?
  const updateAndNavigateOnSBLoad = (newSB: SBDefinitionInput) => {
    setPdmSbDefinition(newSB);
    navigate(RoutePaths.SbdEditor.General);
  };

  const navigateToSbDefinitionByIndex = async (sbDefinitionIndex: number) => {
    const response = await apiService.loadSBFn(
      findSbDefinitionIdByIndex(sbDefinitionIndex)
    );
    if (!response?.error && response?.data) {
      updateAndNavigateOnSBLoad(response!.data as SBDefinitionInput);
    } else {
      setError(
        response.error ??
          'Unknown error while loading Scheduling Block Definition'
      );
    }
  };

  const columns: OsoDataTableColumnDefinitions = [
    {
      displayType: [DisplayType.StaticField],
      title: 'ID',
      columnName: 'id',
      isIdField: true
    },
    {
      displayType: [DisplayType.StaticField],
      title: 'Status',
      columnName: 'status'
    },
    {
      displayType: [DisplayType.EditButton],
      title: '',
      columnName: 'edit',
      description: 'Click to open this Scheduling Block Definition',
      onClick: navigateToSbDefinitionByIndex
    },
    {
      displayType: [DisplayType.DeleteButton],
      title: '',
      columnName: 'delete',
      description:
        'Click to delete this Scheduling Block Definition from the Observing Block',
      onClick: deleteSbDefinitionFromObservingBlock
    }
  ];

  const tableContent = createTableDataFromFormValues(
    columns,
    getValues(ctrlName)
  );

  return (
    <OsoFieldSet title={'Scheduling Block Definitions'}>
      <AlertDialog
        title="Error while entering Scheduling Block Definition editor"
        content={error}
        open={!!error}
        onClose={() => setError('')}
      />
      <OsoDataTable baseName={ctrlName} data={tableContent} columns={columns} />
      <Grid container alignItems="center" spacing={1} pt={2} pb={2}>
        <Grid>
          <CreateSbDefinitionInProjectButton telescope={TelescopeType.SkaMid} />
        </Grid>
        <Grid>
          <CreateSbDefinitionInProjectButton telescope={TelescopeType.SkaLow} />
        </Grid>
      </Grid>
    </OsoFieldSet>
  );
};
