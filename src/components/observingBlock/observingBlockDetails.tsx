import { Stack } from '@mui/material';
import { OsoFieldSet } from '../shared';
import { ControlledTextField } from '../shared/controlled';
import { useProjectStore } from '../../store/projectStore';
import { useShallow } from 'zustand/react/shallow';

export const ObservingBlockDetails = () => {
  const obsBlockIndex = useProjectStore(
    useShallow((state) => state.inProgressObservingBlock!.index)
  );
  const ctrlName = `observingBlocks.${obsBlockIndex}`;

  return (
    <OsoFieldSet title="General">
      <Stack spacing={2}>
        <ControlledTextField name={`${ctrlName}.name`} label="Name" />
        <ControlledTextField
          name={`${ctrlName}.status`}
          label="Status"
          disabled
        />
      </Stack>
    </OsoFieldSet>
  );
};
