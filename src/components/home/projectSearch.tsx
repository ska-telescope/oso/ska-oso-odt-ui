import { useState } from 'react';
import { TextField } from '@mui/material';
import Grid from '@mui/material/Grid2';
import {
  Button,
  ButtonVariantTypes,
  ButtonColorTypes
} from '@ska-telescope/ska-gui-components';
import { useNavigate } from 'react-router-dom';
import { useProjectStore } from '../../store/projectStore';
import { OsoContentTitle } from '../shared/contentTitle';
import { OsoFieldSet } from '../shared/osoFieldSet';
import { RoutePaths } from '../../components/app/routes';
import apiService from '../../services/apiService';
import { ProjectInput } from '../../generated/models/project-input';
import { AlertDialog } from '../shared/dialogs';

export const ProjectSearch = (): JSX.Element => {
  const navigate = useNavigate();
  const setPdmProject = useProjectStore((state) => state.setPdmProject);
  const [prjId, setPrjId] = useState('');
  const [importError, setImportError] = useState('');

  const updateAndNavigateOnSuccess = (project: ProjectInput) => {
    setPdmProject(project);
    navigate(RoutePaths.Project);
  };

  const getProjectFromOda = async () => {
    const response = await apiService.loadProject(prjId);
    if (response?.data) {
      updateAndNavigateOnSuccess(response!.data as ProjectInput);
    } else {
      setImportError(response.error ?? 'Unknown error while importing Project');
    }
  };
  return (
    <OsoFieldSet>
      <OsoContentTitle title="Search for a Project" textAlign="center" />
      <AlertDialog
        title="Failed to import Project"
        content={importError}
        open={!!importError}
        onClose={() => setImportError('')}
      />
      <Grid container alignItems="center" spacing={4} pt={2} pb={2}>
        <Grid size={{ xs: 10 }} pl={3}>
          <TextField
            value={prjId}
            label="Project identifier"
            variant={'outlined'}
            fullWidth={true}
            id="project-search"
            onChange={(event) => setPrjId(event.target.value)}
          />
        </Grid>
        <Grid size={{ xs: 2 }}>
          <Button
            testId="btn-project-search"
            data-testid="btn-project-search"
            label="SEARCH"
            onClick={getProjectFromOda}
            color={ButtonColorTypes.Secondary}
            variant={ButtonVariantTypes.Contained}
            toolTip="Click to load SB from database"
          />
        </Grid>
      </Grid>
    </OsoFieldSet>
  );
};
