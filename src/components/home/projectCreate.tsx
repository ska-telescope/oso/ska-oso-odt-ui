import React from 'react';
import { Box } from '@mui/material';
import {
  Button,
  ButtonVariantTypes,
  ButtonColorTypes
} from '@ska-telescope/ska-gui-components';
import { useNavigate } from 'react-router-dom';
import { useProjectStore } from '../../store/projectStore';
import { OsoContentTitle } from '../shared/contentTitle';
import { OsoFieldSet } from '../shared';
import { RoutePaths } from '../app/routes';

export const ProjectCreate = (): JSX.Element => {
  const navigate = useNavigate();
  const newProject = useProjectStore((state) => state.newProject);

  const createNewProject = () => {
    // This landing page does not sit under a form context, so we do not need to update the form
    newProject();
    navigate(RoutePaths.Project);
  };

  return (
    <OsoFieldSet>
      <OsoContentTitle title="Create a new Project" textAlign="center" />
      <Box textAlign={'center'} pt={2} pb={2}>
        <Button
          testId="btn-continue-sb"
          data-testid="btn-continue-sb"
          label="CLICK HERE"
          onClick={createNewProject}
          color={ButtonColorTypes.Secondary}
          variant={ButtonVariantTypes.Contained}
        />
      </Box>
    </OsoFieldSet>
  );
};
