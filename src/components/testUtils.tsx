import * as zod from '../models/sbDefinition/targets/target';
import {
  LowSbDefinitionSchema,
  LowSbDefinitionType,
  MidSbDefinitionType,
  MidSbDefinitionSchema,
  SbDefinitionType
} from '../models/sbDefinition/sbDefinition';
import React from 'react';
import { FormProvider, useForm } from 'react-hook-form';
import {
  DefaultMidArrayAA05,
  MidArrayConfigTypeEnum
} from '../models/sbDefinition/array/arrayConfig';
import { ScriptKind } from '../models/sbDefinition/script/script';
import { zodResolver } from '@hookform/resolvers/zod';
import { TelescopeType } from '../generated/models/telescope-type';
import { screen, within } from '@testing-library/react';
import { ProjectSchema, ProjectType } from '../models/project/project';
import { useProjectStore } from '../store/projectStore';
import { TelescopeContext } from '../pages/sbd/sbdEditor';

interface WithSbDefinitionFormContextProps {
  initialState?: SbDefinitionType;
  telescopeType?: TelescopeType;
  children: React.ReactNode;
}

export const WithSbDefinitionFormContext = ({
  initialState,
  telescopeType = TelescopeType.SkaMid,
  children
}: WithSbDefinitionFormContextProps) => {
  const formMethods = useForm({
    mode: 'all',
    defaultValues: initialState,
    resolver: zodResolver(
      telescopeType === TelescopeType.SkaMid
        ? MidSbDefinitionSchema
        : LowSbDefinitionSchema
    )
  });

  // This ensures a rerender when there is a change
  formMethods.watch();

  return (
    <TelescopeContext.Provider value={telescopeType}>
      <FormProvider {...formMethods}>{children}</FormProvider>
    </TelescopeContext.Provider>
  );
};

interface WithProjectFormContextProps {
  initialState?: ProjectType;
  initialObservingBlockIndex?: number;
  children: React.ReactNode;
}

export const WithProjectFormContext = ({
  initialState,
  initialObservingBlockIndex,
  children
}: WithProjectFormContextProps) => {
  const setInProgressObservingBlock = useProjectStore(
    (state) => state.setInProgressObservingBlock
  );
  const updateProject = useProjectStore((state) => state.updateProject);

  updateProject(initialState);
  if (initialState?.observingBlocks[initialObservingBlockIndex ?? 0]) {
    setInProgressObservingBlock(
      initialState.observingBlocks[initialObservingBlockIndex ?? 0].id
    );
  }
  const formMethods = useForm({
    mode: 'all',
    defaultValues: initialState,
    resolver: zodResolver(ProjectSchema)
  });

  // This ensures a rerender when there is a change
  formMethods.watch();

  return <FormProvider {...formMethods}>{children}</FormProvider>;
};

export const getTableRows = (tableName: string) => {
  /***
   * Returns the content rows for the given table name, does not return header or footer of the table
   * */
  const tableGroup = screen.getByRole('group', { name: tableName });
  // first 'rowgroup' is the header, return the content rows only
  const tableBody = within(tableGroup).getAllByRole('rowgroup')[1];
  return within(tableBody).getAllByRole('row');
};

export const TEST_FORM_GENERAL = {
  general: {
    sbdId: 'sbd-123',
    name: 'SBD Name',
    description: 'SB Description',
    version: 1,
    createdBy: 'Example User',
    createdOn: '2024-12-12',
    lastModifiedBy: 'Example User',
    lastModifiedOn: '2024-12-12'
  }
};

export const TEST_FORM_LOW_ARRAY: LowSbDefinitionType = {
  array: {
    configuration: 'AA0.5 (Phase 1)',
    stations: 'S8-1, S8-6, S9-2, S10-3',
    subarrayBeam: {
      subarrayBeamID: 'Subarray Beam 1',
      apertures: {
        'S8-1': [
          {
            substationID: 1,
            weightingKey: 'uniform'
          }
        ],
        'S8-6': [
          {
            substationID: 1,
            weightingKey: 'uniform'
          }
        ],
        'S9-2': [
          {
            substationID: 1,
            weightingKey: 'uniform'
          }
        ],
        'S10-3': [
          {
            substationID: 1,
            weightingKey: 'uniform'
          }
        ]
      }
    }
  }
};
export const TEST_FORM_MID_ARRAY: MidSbDefinitionType = {
  array: {
    configuration: MidArrayConfigTypeEnum.AA05,
    dishes: DefaultMidArrayAA05.join(', ')
  }
};

export const TEST_FORM_TARGETS: SbDefinitionType = {
  targets: [
    {
      id: 'target-123',
      name: 'Target 123',
      addPstBeam: false,
      coordinate: {
        kind: zod.CoordinateKind.ICRS,
        ra: '10:00:00.0',
        dec: '+05:00:00.0'
      },
      radialMotion: {
        kind: zod.RadialMotionKind.VELOCITY,
        velocity: 1,
        referenceFrame: zod.ReferenceFrame.BARY
      },
      pointingPattern: {
        kind: zod.FieldPattern.FIVEPOINT,
        offset: 15
      }
    },
    {
      id: 'target-456',
      name: 'Target 456',
      addPstBeam: false,
      coordinate: {
        kind: zod.CoordinateKind.SSO,
        name: 'Sun'
      },
      radialMotion: {
        kind: zod.RadialMotionKind.REDSHIFT,
        redshift: 0.005
      },
      pointingPattern: {
        kind: zod.FieldPattern.POINTINGCENTRES,
        offsets: [{ raOffset: 1, decOffset: 2 }]
      }
    },
    {
      id: 'target-789',
      name: 'Target 789',
      addPstBeam: false,
      coordinate: {
        kind: zod.CoordinateKind.ICRS,
        ra: '10:00:00.0',
        dec: '+05:00:00.0'
      },
      radialMotion: {
        kind: zod.RadialMotionKind.REDSHIFT,
        redshift: 0.005
      },
      pointingPattern: {
        kind: zod.FieldPattern.FIVEPOINT,
        offset: 15
      }
    }
  ]
};

export const TEST_FORM_SCRIPT: SbDefinitionType = {
  script: {
    kind: ScriptKind.git,
    path: '/scripts/hello_world_script.py',
    repo: 'http://foo.bar/non-default-repo',
    branch: 'xx-xxx-branch',
    commit: '',
    selected: 'branch',
    initArgs: [
      {
        kw: 'subarray_id',
        value: '1'
      },
      {
        kw: 'foo',
        value: 'bar'
      },
      {
        kw: 'bar',
        value: 'foo'
      }
    ],
    mainArgs: [
      {
        kw: 'kw1',
        value: 'value1'
      },
      {
        kw: 'kw2',
        value: 'value2'
      }
    ]
  }
};

export const TEST_FORM_MID_SIGNAL_PROCESSOR: MidSbDefinitionType = {
  signalProcessor: [
    {
      id: 'csp-configuration-123',
      name: 'Config 123',
      band: '1',
      fsOffsetMhz: 0,
      spectralWindows: [
        {
          spwId: 1,
          mode: 'CORR',
          centreFrequencyMhz: 500,
          bandwidthMhz: 700.00896,
          resolution: '13.44 (8.06)',
          averageTimeFactor: 2
        }
      ]
    },
    {
      id: 'csp-configuration-456',
      name: 'Config 456',
      band: '1',
      fsOffsetMhz: 1.23,
      spectralWindows: []
    },
    {
      id: 'csp-configuration-789',
      name: 'Config 789',
      band: '1',
      fsOffsetMhz: 1.23,
      spectralWindows: []
    }
  ]
};

export const TEST_FORM_LOW_SIGNAL_PROCESSOR: LowSbDefinitionType = {
  signalProcessor: [
    {
      name: 'CSP Config 123',
      doPst: false,
      spectralWindows: [
        {
          spwId: 1,
          mode: 'CORR',
          centreFrequencyMhz: 500,
          bandwidthMhz: 700.00896,
          resolution: '5.43 (3.25)',
          averageTimeS: 0.283
        },
        {
          spwId: 2,
          mode: 'CORR',
          centreFrequencyMhz: 500,
          bandwidthMhz: 700.00896,
          resolution: '5.43 (3.25)',
          averageTimeS: 0.283
        }
      ]
    }
  ]
};

export const TEST_FORM_SCANS: SbDefinitionType = {
  scans: {
    scanDefinitions: [
      {
        scanDefinitionId: 'scan-definition-123',
        target: 'target-123',
        csp: 'csp-configuration-123',
        durationS: 14
      },
      {
        scanDefinitionId: 'scan-definition-456',
        target: 'target-456',
        csp: 'csp-configuration-123',
        durationS: 8
      },
      {
        scanDefinitionId: 'scan-definition-789',
        target: 'target-789',
        csp: 'csp-configuration-123',
        durationS: 8
      }
    ],
    scanSequence: [
      {
        scanSequenceId: Math.random(),
        scanDefinitionId: 'scan-definition-123',
        target: 'Target 123'
      },
      {
        scanSequenceId: Math.random(),
        scanDefinitionId: 'scan-definition-456',
        target: 'Target 456'
      },
      {
        scanSequenceId: Math.random(),
        scanDefinitionId: 'scan-definition-789',
        target: 'Target 789'
      }
    ]
  }
};

export const TEST_FORM_MID_SB_DEFINITION = {
  ...TEST_FORM_GENERAL,
  ...TEST_FORM_SCRIPT,
  ...TEST_FORM_MID_SIGNAL_PROCESSOR,
  ...TEST_FORM_SCANS,
  ...TEST_FORM_TARGETS
};

export const TEST_FORM_LOW_SB_DEFINITION = {
  ...TEST_FORM_GENERAL,
  ...TEST_FORM_SCRIPT,
  ...TEST_FORM_LOW_SIGNAL_PROCESSOR,
  ...TEST_FORM_SCANS,
  ...TEST_FORM_TARGETS
};

export const TEST_PROJECT_FORM: ProjectType = {
  name: 'SKAO Project',
  code: 'prj-t0001-20250127-00003',
  type: 'Science',
  status: 'TODO',
  observingBlocks: [
    {
      id: 'observing-block-97521',
      name: 'Observing Block 97521',
      status: 'TODO',
      sbDefinitions: [
        { id: 'sbd-123', status: 'TODO' },
        { id: 'sbd-456', status: 'TODO' }
      ]
    },
    {
      id: 'observing-block-58258',
      name: 'Observing Block 58258',
      status: 'TODO',
      sbDefinitions: []
    }
  ]
};
