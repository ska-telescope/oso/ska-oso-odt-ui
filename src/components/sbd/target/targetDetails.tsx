import Grid from '@mui/material/Grid2';
import {
  ControlledCheckbox,
  ControlledTextField
} from '../../../components/shared/controlled';
import { OsoFieldSet } from '../../shared/osoFieldSet';
import { RadialMotionComponent } from './radialMotion';
import { FieldPatternComponent } from './fieldPattern';
import { CoordinateComponent } from './coordinate';
import { useFormContext } from 'react-hook-form';
import { CoordinateKind } from '../../../models/sbDefinition/targets/target';
import { ResolveCoordinatesComponent } from './resolveCoordinates';
import { useContext } from 'react';
import { TelescopeContext } from '../../../pages/sbd/sbdEditor';
import { TelescopeType } from '../../../generated/models/telescope-type';

type TargetDetailsProps = {
  selectedTargetIndex: number;
};

export const TargetDetails = ({
  selectedTargetIndex
}: TargetDetailsProps): JSX.Element => {
  const ctrlName = `targets.${selectedTargetIndex}` as const;

  const telescopeType = useContext(TelescopeContext);
  const { getValues } = useFormContext();
  const coordKind = getValues(ctrlName).coordinate.kind;

  return (
    <Grid
      // 'key' here triggers re-rendering when selectedTarget changes:
      key={selectedTargetIndex}
      container
      direction="row"
      width="100%"
      alignItems={'top'}
      spacing={3}
    >
      <Grid size={{ xs: 12, md: 6, lg: 4 }}>
        <Grid
          container
          direction="row"
          alignItems={'center'}
          justifyContent={'center'}
          spacing={1}
        >
          <Grid size={{ xs: coordKind === CoordinateKind.ICRS ? 9 : 12 }}>
            <ControlledTextField
              name={`targets.${selectedTargetIndex}.name`}
              label="Name"
            />
          </Grid>
          <Grid size={{ xs: 3 }}>
            {coordKind === CoordinateKind.ICRS && (
              <ResolveCoordinatesComponent
                selectedTargetIndex={selectedTargetIndex}
              />
            )}
          </Grid>
        </Grid>
        <OsoFieldSet title="Coordinate">
          <CoordinateComponent selectedTargetIndex={selectedTargetIndex} />
        </OsoFieldSet>
        {telescopeType === TelescopeType.SkaLow && (
          <OsoFieldSet title="PST" style={{ marginTop: '10px' }}>
            <ControlledCheckbox
              name={`targets.${selectedTargetIndex}.addPstBeam`}
              label="Add 'boresight' pulsar timing beam"
              disabled={coordKind === CoordinateKind.SSO}
            />
          </OsoFieldSet>
        )}
      </Grid>
      <Grid size={{ xs: 12, md: 6, lg: 4 }}>
        <OsoFieldSet title="Radial Motion">
          <RadialMotionComponent selectedTargetIndex={selectedTargetIndex} />
        </OsoFieldSet>
      </Grid>
      <Grid size={{ xs: 12, md: 6, lg: 4 }}>
        <OsoFieldSet title="Field Pattern">
          <FieldPatternComponent selectedTargetIndex={selectedTargetIndex} />
        </OsoFieldSet>
      </Grid>
    </Grid>
  );
};
