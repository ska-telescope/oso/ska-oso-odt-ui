import { Button, ButtonGroup } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import FlareIcon from '@mui/icons-material/Flare';
import UploadFileIcon from '@mui/icons-material/UploadFile';
import { TargetSchema } from '../../../models/sbDefinition/targets/target';
import { makeTargetIdAndName } from '../../../lib/id-generator';
import { useFormContext } from 'react-hook-form';
import { OsoListComponent } from '../../shared/list';

const ctrlName = 'targets';

interface Props {
  selectedTargetIndex: number;
  setSelectedTargetIndex: () => void;
}

export const TargetsList = ({
  selectedTargetIndex,
  setSelectedTargetIndex
}: Props): JSX.Element => {
  const { setValue, getValues } = useFormContext();

  const targets = getValues(ctrlName);

  const addTarget = () => {
    const newTarget = TargetSchema.parse({
      ...makeTargetIdAndName()
    });
    setValue(ctrlName, [...targets, newTarget]);
  };

  const deleteTargetItem = (index: number) => {
    setValue(ctrlName, targets.toSpliced(index, 1));
  };

  const importTarget = (): void => {
    alert('not implemented yet');
  };

  const targetNames = getValues(ctrlName).map((target) => target.name);

  return (
    <OsoListComponent
      title="Targets"
      name={'targets-list'}
      selectedIndex={selectedTargetIndex}
      setSelectedIndex={setSelectedTargetIndex}
      names={targetNames}
      Icon={FlareIcon}
      deleteItem={deleteTargetItem}
    >
      <ButtonGroup
        disableElevation
        variant="contained"
        aria-label="Manage target buttons"
        sx={{ mt: '15px' }}
      >
        <Button
          aria-label="add target"
          startIcon={<AddIcon />}
          onClick={addTarget}
          variant="contained"
          color="secondary"
        >
          Add
        </Button>
        <Button
          aria-label="import target from file"
          startIcon={<UploadFileIcon />}
          onClick={importTarget}
          variant="contained"
          color="inherit"
          disabled
        >
          From file
        </Button>
      </ButtonGroup>
    </OsoListComponent>
  );
};
