import { act, fireEvent, render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { CoordinateComponent } from './coordinate';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_TARGETS
} from 'src/components/testUtils';

describe('CoordinateComponent', () => {
  describe('for mid', () => {
    test('render should return appropriate components', () => {
      render(
        <WithSbDefinitionFormContext initialState={TEST_FORM_TARGETS}>
          <CoordinateComponent selectedTargetIndex={0} />
        </WithSbDefinitionFormContext>
      );

      const combobox = screen.getByRole('combobox');
      expect(combobox).toBeInTheDocument();
      expect(combobox).toBeVisible();
      expect(combobox).toHaveAttribute('aria-expanded', 'false');
      expect(combobox).toHaveAttribute('aria-haspopup', 'listbox');
      expect(combobox).toHaveAttribute(
        'aria-labelledby',
        'targets.0.coordinate.kind-label mui-component-select-targets.0.coordinate.kind'
      );

      const hiddenInputs = screen.getAllByRole('textbox', {
        hidden: true
      });
      expect(hiddenInputs).toHaveLength(3);
      expect(hiddenInputs[0]).toBeInTheDocument();
      expect(hiddenInputs[0]).toHaveAttribute('aria-invalid', 'false');
      expect(hiddenInputs[1]).toBeInTheDocument();
      expect(hiddenInputs[1]).toHaveAttribute('aria-invalid', 'false');
      expect(hiddenInputs[2]).toBeInTheDocument();
      expect(hiddenInputs[2]).toHaveAttribute('aria-invalid', 'false');

      const hiddenGroups = screen.getAllByRole('group', {
        hidden: true
      });
      expect(hiddenGroups).toHaveLength(3);

      const raInput = screen.getByRole('textbox', {
        name: /r\.a\./i
      });
      expect(raInput).toBeInTheDocument();
      expect(raInput).toHaveAttribute('aria-invalid', 'false');
      expect(raInput).toHaveAttribute('id', 'targets.0.coordinate.ra');
      expect(raInput).toHaveAttribute('name', 'targets.0.coordinate.ra');
      expect(raInput).toHaveAttribute('value', '10:00:00.0');

      const decInput = screen.getByRole('textbox', {
        name: /dec\./i
      });
      expect(decInput).toBeInTheDocument();
      expect(decInput).toHaveAttribute('aria-invalid', 'false');
      expect(decInput).toHaveAttribute('id', 'targets.0.coordinate.dec');
      expect(decInput).toHaveAttribute('name', 'targets.0.coordinate.dec');
      expect(decInput).toHaveAttribute('value', '+05:00:00.0');
    });
    test('trigger inputs should return appropriate data', () => {
      render(
        <WithSbDefinitionFormContext initialState={TEST_FORM_TARGETS}>
          <CoordinateComponent selectedTargetIndex={0} />
        </WithSbDefinitionFormContext>
      );

      const raInput = screen.getByRole('textbox', {
        name: /r\.a\./i
      });
      expect(raInput).toHaveAttribute('value', '10:00:00.0');
      const raData = '123';
      act(() => {
        fireEvent.change(raInput, { target: { value: raData } });
      });
      const decInput = screen.getByRole('textbox', {
        name: /dec\./i
      });
      expect(decInput).toHaveAttribute('value', '+05:00:00.0');
      const decData = '451';
      act(() => {
        fireEvent.change(decInput, { target: { value: decData } });
      });
    });
  });

  test('low should return appropriate components', () => {
    const container = render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_TARGETS}>
        <CoordinateComponent selectedTargetIndex={0} />
      </WithSbDefinitionFormContext>
    );
    expect(container.children).eq(undefined);
    expect(container.innerHTML).eq(undefined);
  });
});
