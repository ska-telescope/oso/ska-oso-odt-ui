import { render, screen, within } from '@testing-library/react';
import { vi, describe, expect, test } from 'vitest';
import { RadialMotionComponent } from './radialMotion';
import { WithSbDefinitionFormContext } from 'src/components/testUtils';
import { ResolveCoordinatesComponent } from './resolveCoordinates';
import { CoordinateComponent } from './coordinate';
import apiService from '../../../services/apiService';
import * as zod from '../../../models/sbDefinition/targets/target';
import userEvent from '@testing-library/user-event';

describe('ResolveCoordinatesComponent', () => {
  const TARGET_WITH_NAME_ONLY = {
    targets: [
      {
        id: 'target-123',
        name: '47 Tuc',
        coordinate: {
          kind: zod.CoordinateKind.ICRS,
          ra: '00:00:00',
          dec: '00:00:00'
        },
        radialMotion: {
          kind: zod.RadialMotionKind.VELOCITY,
          velocity: 0,
          referenceFrame: zod.ReferenceFrame.LSRK,
          velocityDefinition: zod.VelocityDefinition.RADIO
        }
      }
    ]
  };

  beforeEach(() => {
    render(
      <WithSbDefinitionFormContext initialState={TARGET_WITH_NAME_ONLY}>
        <CoordinateComponent selectedTargetIndex={0} />
        <RadialMotionComponent selectedTargetIndex={0} />
        <ResolveCoordinatesComponent selectedTargetIndex={0} />
      </WithSbDefinitionFormContext>
    );

    const raInput = screen.getByLabelText('R.A.');
    const decInput = screen.getByLabelText('Dec.');
    const velocityInput = screen.getByLabelText('Velocity (km/s)');
    const referenceFrameDropdown = screen.getByLabelText('Reference Frame');
    const velocityDefDropdown = screen.getByLabelText('Velocity Definition');

    expect(raInput).toHaveValue('00:00:00');
    expect(decInput).toHaveValue('00:00:00');
    expect(velocityInput).toHaveValue(0);
    expect(referenceFrameDropdown).toHaveTextContent('lsrk');
    expect(velocityDefDropdown).toHaveTextContent('Radio');
  });

  test('Should display resolved coordinates in correct fields', async () => {
    const mockGetCoordinatesFn = vi.spyOn(apiService, 'getSystemCoordinates');
    mockGetCoordinatesFn.mockReturnValue(
      Promise.resolve({
        data: {
          equatorial: {
            ra: '00:24:05.3590',
            dec: '-72:04:53.200',
            redshift: 0.0,
            velocity: -17.2
          }
        }
      })
    );

    const raInput = screen.getByLabelText('R.A.');
    const decInput = screen.getByLabelText('Dec.');
    const velocityInput = screen.getByLabelText('Velocity (km/s)');
    const referenceFrameDropdown = screen.getByLabelText('Reference Frame');
    const velocityDefDropdown = screen.getByLabelText('Velocity Definition');

    const resolveBtn = screen.getByText('Resolve');
    await userEvent.click(resolveBtn);

    expect(raInput).toHaveValue('00:24:05.3590');
    expect(decInput).toHaveValue('-72:04:53.200');
    expect(velocityInput).toHaveValue(-17.2);
    expect(referenceFrameDropdown).toHaveTextContent('bary');
    expect(velocityDefDropdown).toHaveTextContent('Optical');
  });

  test('Should display redshift if velocity is 0', async () => {
    const mockGetCoordinatesFn = vi.spyOn(apiService, 'getSystemCoordinates');
    mockGetCoordinatesFn.mockReturnValue(
      Promise.resolve({
        data: {
          equatorial: {
            ra: '10:00:58.0008',
            dec: '+01:48:15.156',
            redshift: 6.541,
            velocity: 0.0
          }
        }
      })
    );

    const resolveBtn = screen.getByText('Resolve');
    await userEvent.click(resolveBtn);

    const radModTypeDropdown = screen.getByLabelText('Radial Motion Type');
    expect(radModTypeDropdown).toHaveTextContent('Redshift');

    // Using getByRole instead of getByLabelText since both dropdown and text input
    // include the text 'Redshift'.
    const redshiftInput = screen.getByRole('spinbutton', { name: 'Redshift' });
    expect(redshiftInput).toHaveValue(6.541);
  });

  test('Should keep selected radial motion if redshift and velocity are both 0', async () => {
    const mockGetCoordinatesFn = vi.spyOn(apiService, 'getSystemCoordinates');
    mockGetCoordinatesFn.mockReturnValue(
      Promise.resolve({
        data: {
          equatorial: {
            ra: '03:59:50.6448',
            dec: '+67:07:41.592',
            redshift: 0.0,
            velocity: 0.0
          }
        }
      })
    );
    const velocityInput = screen.getByLabelText('Velocity (km/s)');

    // Set the velocity to a value to check that it will be reset to 0
    await userEvent.clear(velocityInput);
    await userEvent.type(velocityInput, '5.4');

    const resolveBtn = screen.getByText('Resolve');
    await userEvent.click(resolveBtn);

    expect(velocityInput).toHaveValue(0);

    // Change radial motion to redshift
    const rmDropdown = screen.getByRole('combobox', {
      name: 'Radial Motion Type'
    });
    await userEvent.click(rmDropdown);
    const optionsPopup = await screen.findByRole('listbox', {
      name: 'Radial Motion Type'
    });
    await userEvent.click(within(optionsPopup).getByText('Redshift'));

    const redshiftInput = screen.getByRole('spinbutton', { name: 'Redshift' });

    // Set the redshift to a value to check that it will be reset to 0
    await userEvent.clear(redshiftInput);
    await userEvent.type(redshiftInput, '5.4');

    await userEvent.click(resolveBtn);

    expect(redshiftInput).toHaveValue(0);
  });

  test('Should display error returned from the apiService', async () => {
    const errorMsg = 'Object not found in SIMBAD or NED';
    const mockGetCoordinatesFn = vi.spyOn(apiService, 'getSystemCoordinates');
    mockGetCoordinatesFn.mockReturnValue(
      Promise.resolve({
        error: errorMsg
      })
    );
    const resolveBtn = screen.getByText('Resolve');
    await userEvent.click(resolveBtn);

    const errorDialog = screen.getByRole('dialog');
    expect(errorDialog).toHaveTextContent(errorMsg);
  });
});
