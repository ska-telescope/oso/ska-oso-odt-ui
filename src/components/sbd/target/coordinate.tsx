import { Stack } from '@mui/material';
import { ControlledSelect, ControlledTextField } from '../../shared/controlled';
import {
  CoordinateKind,
  CoordinatesSchema
} from '../../../models/sbDefinition/targets/target';
import { Fragment } from 'react';
import { useFormContext, useWatch } from 'react-hook-form';

import { SolarSystemObjectName } from '../../../generated/models/solar-system-object-name';
import { createSelectOptions } from 'src/components/shared/controlled/controlledSelect';

const renderICRSFields = (ctrlBaseName: string): JSX.Element => (
  <Fragment>
    <ControlledTextField
      sx={{ m: 1 }}
      name={`${ctrlBaseName}.ra`}
      label="R.A."
    />
    <ControlledTextField
      sx={{ m: 1 }}
      name={`${ctrlBaseName}.dec`}
      label="Dec."
    />
  </Fragment>
);

const renderSSOFields = (
  ctrlBaseName: string,
  onSSOChange: (targetName: SolarSystemObjectName) => void
): JSX.Element => (
  <ControlledSelect
    sx={{ m: 1 }}
    name={`${ctrlBaseName}.name`}
    options={createSelectOptions(SolarSystemObjectName)}
    onChange={(evt) => onSSOChange(evt.target.value)}
    label="SSO Name"
  />
);

type CoordinateProps = {
  selectedTargetIndex: number;
};

export const CoordinateComponent = ({
  selectedTargetIndex
}: CoordinateProps): JSX.Element => {
  const { setValue } = useFormContext();
  const ctrlName = `targets.${selectedTargetIndex}.coordinate` as const;
  const coordKind = useWatch({
    name: `${ctrlName}.kind`
  });

  const setTargetName = (newSSOName: SolarSystemObjectName) => {
    setValue(`targets.${selectedTargetIndex}.name`, newSSOName);
  };

  const setCoordinateDefaults = (event) => {
    // Set defaults for the new selection based on the defaults
    // declared in the zod Coordinates schema
    const newCoordinateType = event.target.value;
    const newCoordinateDefaults = CoordinatesSchema.parse({
      kind: newCoordinateType
    });
    setValue(ctrlName, newCoordinateDefaults);

    // Also set the target name if coordinate type changes to Solar System Object
    if (newCoordinateDefaults.kind === CoordinateKind.SSO) {
      setTargetName(newCoordinateDefaults.name);
    }
  };

  let dataFields = <></>;
  switch (coordKind) {
    case CoordinateKind.ICRS:
      dataFields = renderICRSFields(ctrlName);
      break;
    case CoordinateKind.SSO:
      dataFields = renderSSOFields(ctrlName, setTargetName);
      break;
    default:
      console.error(`Error: unknown coordinate kind '${coordKind}'.`);
  }

  return (
    <Stack>
      <ControlledSelect
        sx={{ m: 1 }}
        name={`${ctrlName}.kind`}
        options={createSelectOptions(CoordinateKind)}
        onChange={setCoordinateDefaults}
        label={'Coordinate Type'}
      />
      {dataFields}
    </Stack>
  );
};
