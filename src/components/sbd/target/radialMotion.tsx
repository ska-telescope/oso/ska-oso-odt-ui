import { Box } from '@mui/material';
import { useFormContext, useWatch } from 'react-hook-form';
import { Fragment } from 'react/jsx-runtime';
import {
  ControlledSelect,
  ControlledNumberField
} from '../../shared/controlled';

import {
  RadialMotionSchema,
  RadialMotionKind,
  ReferenceFrame,
  VelocityDefinition
} from '../../../models/sbDefinition/targets/target';
import { createSelectOptions } from 'src/components/shared/controlled/controlledSelect';

const renderVelocityFields = (ctrlName: string): JSX.Element => {
  return (
    <Fragment>
      <ControlledNumberField
        sx={{ m: 1 }}
        name={`${ctrlName}.velocity`}
        min={Number.NEGATIVE_INFINITY}
        label="Velocity (km/s)"
      />
      <ControlledSelect
        sx={{ m: 1 }}
        name={`${ctrlName}.referenceFrame`}
        options={createSelectOptions(ReferenceFrame)}
        label="Reference Frame"
      />
      <ControlledSelect
        sx={{ m: 1 }}
        name={`${ctrlName}.velocityDefinition`}
        options={createSelectOptions(VelocityDefinition)}
        label="Velocity Definition"
      />
    </Fragment>
  );
};

const renderRedshiftField = (ctrlName: string): JSX.Element => {
  return (
    <ControlledNumberField
      sx={{ m: 1 }}
      name={`${ctrlName}.redshift`}
      label="Redshift"
    />
  );
};

type RadialMotionProps = {
  selectedTargetIndex: number;
};

export const RadialMotionComponent = ({
  selectedTargetIndex
}: RadialMotionProps): JSX.Element => {
  const ctrlName = `targets.${selectedTargetIndex}.radialMotion` as const;
  const { setValue } = useFormContext();

  const selectedRMK = useWatch({
    name: `${ctrlName}.kind`
  });

  const setRMKDefaults = (event) => {
    // Set defaults for the new selection based on the defaults
    // declared in the zod RadialMotion schema
    const newRMK = event.target.value;
    setValue(ctrlName, RadialMotionSchema.parse({ kind: newRMK }));
  };

  let dataFields = <></>;
  switch (selectedRMK) {
    case RadialMotionKind.VELOCITY:
      dataFields = renderVelocityFields(ctrlName);
      break;
    case RadialMotionKind.REDSHIFT:
      dataFields = renderRedshiftField(ctrlName);
      break;
    default:
      console.error(`Error: unknown kind '${selectedRMK}'.`);
  }

  return (
    <Box>
      <ControlledSelect
        sx={{ m: 1 }}
        name={`${ctrlName}.kind`}
        onChange={setRMKDefaults}
        options={createSelectOptions(RadialMotionKind)}
        label="Radial Motion Type"
      />
      {dataFields}
    </Box>
  );
};
