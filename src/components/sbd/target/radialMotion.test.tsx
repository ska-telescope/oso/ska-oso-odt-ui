import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { RadialMotionComponent } from './radialMotion';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_TARGETS
} from 'src/components/testUtils';

describe('RadialMotionComponent', () => {
  test('mid should return appropriate components', () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_TARGETS}>
        <RadialMotionComponent selectedTargetIndex={1} />
      </WithSbDefinitionFormContext>
    );
    const radialMotionTypeSelect = screen.getByRole('combobox');
    expect(radialMotionTypeSelect).toBeInTheDocument();
    expect(radialMotionTypeSelect).toBeVisible();
    expect(radialMotionTypeSelect).toHaveAttribute('aria-expanded', 'false');
    expect(radialMotionTypeSelect).toHaveAttribute('aria-haspopup', 'listbox');
    expect(radialMotionTypeSelect).toHaveAttribute(
      'aria-labelledby',
      'targets.1.radialMotion.kind-label mui-component-select-targets.1.radialMotion.kind'
    );

    // This is the Velocity input, as the radial motion type is redshift
    const hiddenInput = screen.getByRole('textbox', {
      hidden: true
    });
    expect(hiddenInput).toBeInTheDocument();
    expect(hiddenInput).not.toBeVisible();
    expect(hiddenInput).toHaveAttribute('aria-hidden', 'true');
    expect(hiddenInput).toHaveAttribute('aria-invalid', 'false');
  });

  test('low should return appropriate components', () => {
    const container = render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_TARGETS}>
        <RadialMotionComponent selectedTargetIndex={0} />
      </WithSbDefinitionFormContext>
    );
    expect(container.children).eq(undefined);
    expect(container.innerHTML).eq(undefined);
  });
});
