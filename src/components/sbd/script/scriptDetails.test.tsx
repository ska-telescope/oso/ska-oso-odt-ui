import React from 'react';
import { render, screen, within } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { ScriptDetails } from './scriptDetails';
import userEvent from '@testing-library/user-event';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_SCRIPT
} from 'src/components/testUtils';
import { TelescopeType } from '../../../generated/models/telescope-type';

// Script component should work exactly the same way for both Mid and Low SBs
describe.each([TelescopeType.SkaLow, TelescopeType.SkaMid])(
  'Script',
  (telescope: TelescopeType) => {
    test('Field values are populated as expected', async () => {
      render(
        <WithSbDefinitionFormContext
          telescopeType={telescope}
          initialState={TEST_FORM_SCRIPT}
        >
          <ScriptDetails />
        </WithSbDefinitionFormContext>
      );

      const pathInput = screen.getByLabelText('Path');
      expect(pathInput).toHaveValue(TEST_FORM_SCRIPT.script.path);
      const repoInput = screen.getByLabelText('Repo');
      expect(repoInput).toHaveValue(TEST_FORM_SCRIPT.script.repo);

      const branchRadio = screen.getByRole('radio', { name: 'Branch' });
      expect(branchRadio).toBeChecked();
      const commitRadio = screen.getByRole('radio', { name: 'Commit' });
      expect(commitRadio).not.toBeChecked();

      const branchInput = screen.queryByRole('textbox', { name: 'Branch' });
      expect(branchInput).toHaveValue(TEST_FORM_SCRIPT.script.branch);
    });

    test('Radio button toggles between branch and commit', async () => {
      render(
        <WithSbDefinitionFormContext
          telescopeType={telescope}
          initialState={TEST_FORM_SCRIPT}
        >
          <ScriptDetails />
        </WithSbDefinitionFormContext>
      );

      const branchRadio = screen.getByRole('radio', { name: 'Branch' });
      const commitRadio = screen.getByRole('radio', { name: 'Commit' });

      await userEvent.click(commitRadio);
      expect(branchRadio).not.toBeChecked();
      expect(commitRadio).toBeChecked();

      expect(screen.queryByRole('textbox', { name: 'Branch' })).toBeNull();
      expect(
        screen.getByRole('textbox', { name: 'Commit' })
      ).toBeInTheDocument();
    });

    test('Kind dropdown displays/hides appropriate fields', async () => {
      render(
        <WithSbDefinitionFormContext
          telescopeType={telescope}
          initialState={TEST_FORM_SCRIPT}
        >
          <ScriptDetails />
        </WithSbDefinitionFormContext>
      );

      // First check that git-related fields are present
      let repoInput: HTMLElement | null = screen.getByLabelText('Repo');
      expect(repoInput).toBeInTheDocument();
      let branchInput: HTMLElement | null = screen.getByRole('textbox', {
        name: 'Branch'
      });
      expect(branchInput).toBeInTheDocument();

      // Change kind from dropdown to filesystem instead
      const dropdown = screen.getByRole('combobox');
      await userEvent.click(dropdown);
      const optionsPopupEl = await screen.findByRole('listbox', {
        name: 'Kind'
      });
      await userEvent.click(within(optionsPopupEl).getByText('filesystem'));

      // Check that path input still exists and input has not changed
      const pathInput = screen.getByLabelText('Path');
      expect(pathInput).toHaveValue(TEST_FORM_SCRIPT.path);

      // Check that the appropriate git inputs have been removed
      repoInput = screen.queryByRole('textbox', { name: 'Repo' });
      expect(repoInput).toBeNull();
      branchInput = screen.queryByRole('textbox', { name: 'Branch' });
      expect(branchInput).toBeNull();
    });

    test('User input is recorded and validated correctly', async () => {
      render(
        <WithSbDefinitionFormContext
          telescopeType={telescope}
          initialState={TEST_FORM_SCRIPT}
        >
          <ScriptDetails />
        </WithSbDefinitionFormContext>
      );

      const newFilePath = '/new/path/to/file.py';
      const pathInput = screen.getByLabelText('Path');
      expect(pathInput).toHaveValue(TEST_FORM_SCRIPT.script.path);

      await userEvent.clear(pathInput);
      expect(pathInput).toHaveAccessibleDescription('Path is required');
      await userEvent.type(pathInput, newFilePath);
      expect(pathInput).toHaveValue(newFilePath);

      const newRepoPath = 'http://new-repo.path';
      const repoInput = screen.getByLabelText('Repo');
      expect(repoInput).toHaveValue(TEST_FORM_SCRIPT.script.repo);

      await userEvent.clear(repoInput);
      expect(repoInput).toHaveAccessibleDescription('Repository is required');
      await userEvent.type(repoInput, newRepoPath);
      expect(repoInput).toHaveValue(newRepoPath);

      await userEvent.clear(repoInput);
      await userEvent.type(repoInput, 'invalid-url');
      expect(screen.getByLabelText('Repo')).toHaveAccessibleDescription(
        'Git repository must be a valid URL'
      );
    });
  }
);
