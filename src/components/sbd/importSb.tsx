import React from 'react';
import { TextField, Typography } from '@mui/material';
import Grid from '@mui/material/Grid2';
import {
  Button,
  ButtonColorTypes,
  ButtonVariantTypes,
  TextEntry
} from '@ska-telescope/ska-gui-components';
import type { ChangeEvent } from 'react';
import { useState } from 'react';
import { useNavigate } from 'react-router-dom';

import { SBDefinitionInput } from '../../generated/models/sbdefinition-input';
import { useStore } from '../../store/store';
import apiService from '../../services/apiService';
import { RoutePaths } from '../app/routes';
import { AlertDialog } from '../shared/dialogs';

const importSb = () => {
  const setPdmSbDefinition = useStore((state) => state.setPdmSbDefinition);
  const navigate = useNavigate();
  const [sbdID, setSbdID] = useState('');
  const [importError, setImportError] = useState('');

  const updateAndNavigateOnSBLoad = (newSB: SBDefinitionInput) => {
    setPdmSbDefinition(newSB);
    navigate(RoutePaths.SbdEditor.General);
  };

  const loadNewSb = async () => {
    const response = await apiService.loadSBFn(sbdID);
    if (!response?.error && response?.data) {
      updateAndNavigateOnSBLoad(response!.data as SBDefinitionInput);
    } else if (response?.error) {
      setImportError(`Failed to load SBDefinition ${sbdID}: ${response.error}`);
    } else {
      setImportError(`Failed to load SB with ID: ${sbdID}`);
    }
  };

  const handleFileSelect = (event: ChangeEvent<HTMLInputElement>) => {
    const file = event.target.files?.[0];
    if (file) {
      const reader = new FileReader();
      reader.onload = (e) => {
        try {
          const apiData = e.target?.result as string;
          updateAndNavigateOnSBLoad(JSON.parse(apiData));
        } catch (error) {
          setImportError(`Error parsing JSON file: ${error}`);
        }
      };
      reader.readAsText(file);
    }
    return null;
  };

  return (
    <Grid container alignItems="center" spacing={4} pt={2} pb={2}>
      <Grid size={{ xs: 5 }}>
        <Typography>Load from ODA:</Typography>
      </Grid>
      <Grid size={{ xs: 4 }}>
        <TextEntry
          ariaTitle="Load Scheduling Block"
          ariaDescription="Load Scheduling Block"
          testId="input-sb"
          name="input-sb"
          label=""
          value={sbdID}
          setValue={setSbdID}
          helperText="Enter an SBD identifier"
        />
      </Grid>
      <Grid size={{ xs: 3 }}>
        <Button
          testId="btn-load-sb"
          data-testid="btn-load-sb"
          label="LOAD SB"
          onClick={loadNewSb}
          color={ButtonColorTypes.Secondary}
          variant={ButtonVariantTypes.Contained}
          toolTip="Click to load SB from database"
        />
      </Grid>
      <Grid size={{ xs: 5 }}>
        <Typography>Import from local filesystem:</Typography>
      </Grid>
      <Grid size={{ xs: 7 }}>
        <TextField
          name="filepicker"
          data-testid="filepicker"
          variant="filled"
          fullWidth
          type="file"
          value=""
          onChange={handleFileSelect}
        />
      </Grid>
      <Grid size={{ xs: 4 }}>
        <AlertDialog
          title="Error importing Scheduling Block Definition"
          content={importError}
          open={!!importError}
          onClose={() => setImportError('')}
        />
      </Grid>
    </Grid>
  );
};

export default importSb;
