import React from 'react';
import { render, screen, within } from '@testing-library/react';
import { ScanSequence } from './scanSequence';
import userEvent from '@testing-library/user-event';
import {
  TEST_FORM_SCANS,
  TEST_FORM_TARGETS,
  WithSbDefinitionFormContext
} from '../../testUtils';

const PAGE_BASE_NAME = 'scans.scanSequence';

const TEST_INITIAL_STATE = {
  ...TEST_FORM_SCANS,
  ...TEST_FORM_TARGETS
};

describe('ScanSequence component', () => {
  test('should display the data from the store in the correct fields', () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_INITIAL_STATE}>
        <ScanSequence />
      </WithSbDefinitionFormContext>
    );

    const rows = getScanSequenceTableRows();

    expect(rows).toHaveLength(3); // 3 scans (header not included)
    const dataCells = within(rows[0]).getAllByRole('cell');
    expect(dataCells).toHaveLength(5); // 2 inputs + 3 icons
    expect(within(dataCells[0]).getByRole('textbox').value).eq('1');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('Target 123');
  });

  test('should move the scan in the sequence when the up button is clicked', async () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_INITIAL_STATE}>
        <ScanSequence />
      </WithSbDefinitionFormContext>
    );

    let rows = getScanSequenceTableRows();

    // Assert initial value on the first row is as expected
    let dataCells = within(rows[0]).getAllByRole('cell');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('Target 123');

    // Assert initial value on the second row is as expected
    dataCells = within(rows[1]).getAllByRole('cell');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('Target 456');

    // Act  - click the up button on the second content row
    const upButton = screen.getByRole('button', {
      name: `${PAGE_BASE_NAME}.1.move-up`
    });

    await userEvent.click(upButton);

    rows = getScanSequenceTableRows();

    // Assert what was initially in the second content row is now be in the first
    dataCells = within(rows[0]).getAllByRole('cell');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('Target 456');

    // Assert what was initially in the first content row is now be in the second
    dataCells = within(rows[1]).getAllByRole('cell');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('Target 123');
  });

  test('should move the scan in the sequence when the down button is clicked', async () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_INITIAL_STATE}>
        <ScanSequence />
      </WithSbDefinitionFormContext>
    );

    let rows = getScanSequenceTableRows();

    // Assert initial value on the second row is as expected
    let dataCells = within(rows[1]).getAllByRole('cell');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('Target 456');

    // Assert initial value on the third row is as expected
    dataCells = within(rows[2]).getAllByRole('cell');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('Target 789');

    // Act  - click the down button on the second content row
    const downButton = screen.getByRole('button', {
      name: `${PAGE_BASE_NAME}.1.move-down`
    });
    await userEvent.click(downButton);

    rows = getScanSequenceTableRows();

    // Assert what was initially in the second content row is now be in the third
    dataCells = within(rows[2]).getAllByRole('cell');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('Target 456');

    // Assert what was initially in the third content row is now be in the second
    dataCells = within(rows[1]).getAllByRole('cell');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('Target 789');
  });

  test('should delete the correct scan definition row when the delete icon is clicked', async () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_INITIAL_STATE}>
        <ScanSequence />
      </WithSbDefinitionFormContext>
    );

    let rows = getScanSequenceTableRows();

    // Assert initial value on the second row is as expected
    let dataCells = within(rows[1]).getAllByRole('cell');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('Target 456');
    expect(rows).toHaveLength(3);

    const deleteButton = screen.getByRole('button', {
      name: `${PAGE_BASE_NAME}.1.delete`
    });

    await userEvent.click(deleteButton);
    rows = getScanSequenceTableRows();
    expect(rows).toHaveLength(2);
    // expect the second row now to have what used to be on the third row
    dataCells = within(rows[1]).getAllByRole('cell');
    expect(within(dataCells[1]).getByRole('textbox').value).eq('Target 789');
  });
});

const getScanSequenceTableRows = () => {
  const scanSeqGroup = screen.getByRole('group', { name: 'Scan Sequence' });
  const tableBody = within(scanSeqGroup).getAllByRole('rowgroup')[1];
  return within(tableBody).getAllByRole('row');
};
