import React from 'react';
import { OsoFieldSet } from '../../shared/osoFieldSet';

import { useFieldArray, useFormContext } from 'react-hook-form';
import {
  createTableDataFromFormValues,
  DisplayType,
  OsoDataTable,
  OsoDataTableColumnDefinitions
} from '../../shared';
import { ScanDefinitionType, ScanSequenceItemType } from '../../../models';
import { TargetType } from '../../../models/sbDefinition/targets/target';

const ctrlName: string = 'scans.scanSequence';

export const ScanSequence = (): JSX.Element => {
  const { fields, remove, move } = useFieldArray({
    name: ctrlName
  });

  const moveUpFn = (index: number) => move(index, index - 1);
  const moveDownFn = (index: number) => move(index, index + 1);
  const deleteFn = (index: number) => remove(index);

  const columns: OsoDataTableColumnDefinitions = [
    {
      displayType: [DisplayType.HiddenField],
      title: '',
      columnName: 'scanSequenceId',
      isIdField: true
    },
    {
      displayType: [DisplayType.StaticField],
      title: 'Scan',
      columnName: 'index'
    },
    {
      displayType: [DisplayType.MoveUpButton],
      description: 'Click to move this scan sequence up a row',
      title: '',
      columnName: 'move-up',
      onClick: moveUpFn
    },
    {
      displayType: [DisplayType.MoveDownButton],
      description: 'Click to move this scan sequence down a row',
      title: '',
      columnName: 'move-down',
      onClick: moveDownFn
    },
    {
      displayType: [DisplayType.DeleteButton],
      description: 'Click to delete this scan sequence',
      title: '',
      columnName: 'delete',
      onClick: deleteFn
    }
  ];
  const tableData = createTableDataFromFormValues(columns, fields);

  // TODO: This is a temporary workaround until we have a set way to reference fields from the form
  //  context. For now we manually retrieve the target name based on target ID referenced by the Scan
  //  Definition.
  // Add target names to display to user (not part of the ScanSequence form)
  const { getValues } = useFormContext();
  const scanDefsInSequence = getValues('scans.scanSequence').map(
    (scanSeq: ScanSequenceItemType) => scanSeq.scanDefinitionId
  );
  const targetIdsForScans = scanDefsInSequence.map((scanDefId) => {
    return getValues('scans.scanDefinitions').find(
      (scanDefinition: ScanDefinitionType) =>
        scanDefinition.scanDefinitionId === scanDefId
    ).target;
  });
  const targetNamesForScans = targetIdsForScans.map((targetId) => {
    return getValues('targets').find(
      (target: TargetType) => target.id === targetId
    ).name;
  });
  const targetIndexInTable = 2;
  columns.splice(targetIndexInTable, 0, {
    displayType: [DisplayType.StaticField],
    title: 'Target',
    columnName: 'target'
  });
  tableData.forEach((row, index) =>
    row.splice(targetIndexInTable, 0, {
      columnName: 'target',
      value: targetNamesForScans[index]
    })
  );

  return (
    <OsoFieldSet title={'Scan Sequence'}>
      <OsoDataTable baseName={ctrlName} data={tableData} columns={columns} />
    </OsoFieldSet>
  );
};
