import React from 'react';
import { act, fireEvent, render, screen, within } from '@testing-library/react';
import { ScanDefinitions } from './scanDefinitions';
import {
  TEST_FORM_MID_SIGNAL_PROCESSOR,
  TEST_FORM_SCANS,
  TEST_FORM_TARGETS,
  WithSbDefinitionFormContext
} from '../../testUtils';

// The page uses targets and signal processor data that also needs to be in the form
const TEST_INITIAL_STATE = {
  ...TEST_FORM_SCANS,
  ...TEST_FORM_TARGETS,
  ...TEST_FORM_MID_SIGNAL_PROCESSOR
};

describe('ScanDefinitions component', () => {
  test('should display the data from the store in the correct fields', () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_INITIAL_STATE}>
        <ScanDefinitions />
      </WithSbDefinitionFormContext>
    );

    const rows = getScanDefinitionsTableRows();

    expect(rows.length).toBe(3); // 3 scan definitions
    const dataCells = within(rows[0]).getAllByRole('cell');
    expect(dataCells.length).toBe(5); // 3 inputs + 2 icons
    expect(within(dataCells[0]).getByRole('combobox').textContent).toBe(
      'Target 123'
    );
    expect(within(dataCells[1]).getByRole('combobox').textContent).toBe(
      'Config 123'
    );
    expect(within(dataCells[2]).getByRole('spinbutton').value).eq('14');
  });

  test('should add a scan definition when the add button is clicked', () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_INITIAL_STATE}>
        <ScanDefinitions />
      </WithSbDefinitionFormContext>
    );

    const addButton = screen.getByRole('button', {
      name: 'Add New Scan Definition'
    });

    act(() => {
      fireEvent.click(addButton);
    });

    const rows = getScanDefinitionsTableRows();

    expect(rows).toHaveLength(4); // Initial 2 + newly added 1
  });

  test('should not be able to delete a scan definition that is in the scan sequence', () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_INITIAL_STATE}>
        <ScanDefinitions />
      </WithSbDefinitionFormContext>
    );

    const rows = getScanDefinitionsTableRows();

    const deleteButton = within(rows[1]).getByRole('button', {
      name: 'scans.scanDefinitions.1.delete'
    });

    act(() => {
      fireEvent.click(deleteButton);
    });
    const warningDialog = screen.getByRole('dialog');
    expect(warningDialog.textContent).toContain(
      'Scan definition is currently used in the scan sequence.'
    );
  });
});

test('should delete the correct scan definition row when the delete icon is clicked', () => {
  const initialState = { ...TEST_INITIAL_STATE };
  initialState.scans.scanDefinitions[0].scanDefinitionId =
    'unused-scan-definition-id';
  render(
    <WithSbDefinitionFormContext initialState={initialState}>
      <ScanDefinitions />
    </WithSbDefinitionFormContext>
  );

  let rows = getScanDefinitionsTableRows();

  const deleteButton = within(rows[0]).getByRole('button', {
    name: 'scans.scanDefinitions.0.delete'
  });

  act(() => {
    fireEvent.click(deleteButton);
  });
  rows = getScanDefinitionsTableRows();
  expect(rows).toHaveLength(2);
});

const getScanDefinitionsTableRows = () => {
  const scanSeqGroup = screen.getByRole('group', { name: 'Scan Definitions' });
  const tableBody = within(scanSeqGroup).getAllByRole('rowgroup')[1];
  return within(tableBody).getAllByRole('row');
};
