import { fireEvent, render, screen, waitFor } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import {
  LowArrayAA05P1,
  LowArrayAA05P2,
  stationIDMap
} from '../../../models/sbDefinition/array/arrayConfig';
import { LowArrayComponent } from './lowArrayConfig';
import userEvent from '@testing-library/user-event';
import {
  TEST_FORM_LOW_ARRAY,
  WithSbDefinitionFormContext
} from '../../testUtils';
import React from 'react';

describe('LowArrayComponent', () => {
  test('should return appropriate components with default values', () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_LOW_ARRAY}>
        <LowArrayComponent />
      </WithSbDefinitionFormContext>
    );

    const configuration = screen.getByLabelText(
      'Configuration'
    ) as HTMLSelectElement;
    expect(configuration).toBeInTheDocument();
    expect(configuration).toHaveTextContent('AA0.5 (Phase 1)');

    const numberOfStations = screen.getByLabelText('Number of Stations');
    expect(numberOfStations).toBeInTheDocument();
    expect(numberOfStations).toHaveValue('4');

    const stations = screen.getByLabelText('Stations');
    expect(stations).toBeInTheDocument();
    expect(stations).toHaveValue(
      LowArrayAA05P1.map((stationID) => stationIDMap.get(stationID)).join(', ')
    );
  });

  test('should change default stations when different arrays are selected', async () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_LOW_ARRAY}>
        <LowArrayComponent />
      </WithSbDefinitionFormContext>
    );

    const configurationDropdown = screen.getByLabelText(
      'Configuration'
    ) as HTMLSelectElement;
    await userEvent.click(configurationDropdown);
    await userEvent.click(screen.getByText('AA0.5 (Phase 2)'));
    expect(configurationDropdown).toHaveTextContent('AA0.5 (Phase 2)');

    const numberOfStations = screen.getByLabelText('Number of Stations');
    expect(numberOfStations).toHaveValue('6');

    const stations = screen.getByLabelText('Stations');
    expect(stations).toHaveValue(
      LowArrayAA05P2.map((stationID) => stationIDMap.get(stationID)).join(', ')
    );
  });

  // TODO: As part of figuring out the testing strategy for these components, we need to figure out how
  //  to type with UseEvent. For some reason this test sometimes fails because it won't type the full
  //  string in user.type(). But we have the same test in the General component and it works there.
  //  Looks like there's an issue open for this on user-event but no resolution as of yet:
  //  https://github.com/testing-library/user-event/issues/1150

  test('should allow custom stations to be specified', async () => {
    const user = userEvent.setup({ delay: null });
    render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_LOW_ARRAY}>
        <LowArrayComponent />
      </WithSbDefinitionFormContext>
    );
    const configurationDropdown = screen.getByLabelText(
      'Configuration'
    ) as HTMLSelectElement;
    await user.click(configurationDropdown);
    await user.click(screen.getByText('Custom'));
    expect(configurationDropdown).toHaveTextContent('Custom');

    const stations = screen.getByLabelText('Stations');
    await user.clear(stations);
    // The below line does not work (see note above), user fireEvent instead
    //await user.type(stations, 'S1-2 N4-5');
    fireEvent.change(stations, { target: { value: 'S1-2, N4-5' } });
    await waitFor(() => expect(stations).toHaveValue('S1-2, N4-5'));

    const numberOfStations = screen.getByLabelText('Number of Stations');
    await waitFor(() => expect(numberOfStations).toHaveValue('2'));
  });
});
