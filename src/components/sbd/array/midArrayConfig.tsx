import React, { useState } from 'react';
import { Card, CardContent, type SelectChangeEvent } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { useFormContext } from 'react-hook-form';
import {
  MidArrayConfigTypeOptions,
  MidArrayConfigTypeEnum,
  DefaultMidArrayAA05
} from '../../../models/sbDefinition/array/arrayConfig';
import { ControlledSelect, ControlledTextField } from '../../shared/controlled';
import { OsoStaticInput } from '../../shared/osoStaticInput';
import { OsoFieldSet } from '../../shared/osoFieldSet';

const ctrlName = 'array' as const;

export const MidArrayComponent = (): JSX.Element => {
  const { setValue, getValues } = useFormContext();

  const arrayConfigValue = getValues(ctrlName);

  // Store array type separately instead of always displaying the stored label so that
  // user Custom selection is temporarily stored even if dish IDs match an existing array
  const [arrayLabel, setArrayLabel] = useState<string>(
    arrayConfigValue.configuration
  );

  const subarrayOnChangeHandle = (e: SelectChangeEvent): void => {
    const newArray = e.target.value as keyof typeof MidArrayConfigTypeEnum;
    // Currently Mid only supports two array types, Custom and AA0.5
    // If type is not custom, set dishes to AA0.5 list (expand the logic
    // here in the future to allow different array types)
    setValue(ctrlName, {
      dishes:
        newArray !== MidArrayConfigTypeEnum.Custom
          ? DefaultMidArrayAA05.join(', ')
          : arrayConfigValue.dishes,
      configuration: newArray
    });
    setArrayLabel(newArray);
  };
  const disabledInput: boolean = arrayLabel !== MidArrayConfigTypeEnum.Custom;

  return (
    <Card data-testid="sbMidArrayConfig" variant="outlined">
      <CardContent sx={{ width: '100%' }}>
        <OsoFieldSet title={'Configuration'}>
          <Grid container direction="row" width="100%" alignItems={'center'}>
            <Grid size={{ xs: 3 }} paddingRight="10px">
              <ControlledSelect
                name={`${ctrlName}.configuration`}
                label="Configuration"
                options={MidArrayConfigTypeOptions}
                onChange={subarrayOnChangeHandle}
              />
            </Grid>
            <Grid size={{ xs: 7 }} paddingLeft="10px">
              <ControlledTextField
                name={`${ctrlName}.dishes`}
                label="Dishes"
                disabled={disabledInput}
              />
            </Grid>
            <Grid size={{ xs: 2 }} paddingLeft="10px">
              <OsoStaticInput
                label="Number of Dishes"
                name={`${ctrlName}.numberOfDishes`}
                value={
                  arrayConfigValue.dishes
                    ? arrayConfigValue.dishes.split(',').length
                    : 0
                }
              />
            </Grid>
          </Grid>
        </OsoFieldSet>
      </CardContent>
    </Card>
  );
};
