import { Card, CardContent } from '@mui/material';
import Grid from '@mui/material/Grid2';
import React, { useState } from 'react';
import HubOutlinedIcon from '@mui/icons-material/HubOutlined';
import { OsoFieldSet } from '../../shared/osoFieldSet';
import { useFormContext } from 'react-hook-form';
import {
  DisplayType,
  OsoDataTable,
  OsoDataTableColumnDefinitions
} from '../../shared';
import { OsoListComponent } from '../../shared/list';
import { createTableDataFromFormValues } from '../../shared/dataTable';
import { ApertureSchema } from '../../../models/sbDefinition/array/arrayConfig';
import { OperationsButtonGroup } from '../../shared/operationButtonGroup';

type ApertureTableProps = {
  selectedStation: string;
};

const ApertureTable = ({
  selectedStation
}: ApertureTableProps): JSX.Element => {
  const ctrlName = `array.subarrayBeam.apertures.${selectedStation}`;
  const { getValues, setValue } = useFormContext();

  const addAperture = () => {
    const newAperture = ApertureSchema.parse({});
    setValue(ctrlName, [...(getValues(ctrlName) ?? []), newAperture]);
  };

  const deleteAperture = (index: number) => {
    setValue(ctrlName, getValues(ctrlName).toSpliced(index, 1));
  };

  const columns: OsoDataTableColumnDefinitions = [
    {
      displayType: [DisplayType.HiddenField],
      title: '',
      columnName: 'substationID',
      isIdField: true
    },
    {
      displayType: [DisplayType.StaticField],
      title: 'Substation',
      columnName: 'substation'
    },
    {
      displayType: [DisplayType.TextField],
      title: 'Weighting Key',
      columnName: 'weightingKey',
      id: 'weightingKey'
    },
    {
      displayType: [DisplayType.DeleteButton],
      title: '',
      columnName: 'delete',
      description: 'Delete this aperture entry',
      onClick: deleteAperture,
      disabled: getValues(ctrlName).length === 1
    }
  ];

  const tableContent = createTableDataFromFormValues(
    columns,
    getValues(ctrlName)
  );

  return (
    <div>
      <OsoDataTable baseName={ctrlName} data={tableContent} columns={columns} />
      <OperationsButtonGroup
        title="Aperture"
        count={1}
        handleAdd={addAperture}
      />
    </div>
  );
};

export const SubarrayBeamComponent = () => {
  const ctrlName = 'array.subarrayBeam';
  const { getValues } = useFormContext();
  const subarrayBeam = getValues(ctrlName);
  const [selectedStation, setSelectedStation] = useState<number>(0);
  if (Object.keys(subarrayBeam.apertures).length <= selectedStation) {
    setSelectedStation(0);
  }

  return (
    <Card data-testid="sbSubarratBeamConfig" variant="outlined">
      <CardContent>
        <OsoFieldSet title={subarrayBeam.subarrayBeamID}>
          <Grid container direction="row" width="100%" padding="5px">
            <Grid size={{ xs: 3 }}>
              <OsoListComponent
                title="Stations"
                name="stations-table"
                selectedIndex={selectedStation}
                setSelectedIndex={setSelectedStation}
                names={Object.keys(subarrayBeam.apertures)}
                Icon={HubOutlinedIcon}
                withDelete={false}
                disabledDelete={true}
              />
            </Grid>
            <Grid size={{ xs: 9 }} padding="10px">
              <ApertureTable
                selectedStation={
                  Object.keys(subarrayBeam.apertures)[selectedStation]
                }
              />
            </Grid>
          </Grid>
        </OsoFieldSet>
      </CardContent>
    </Card>
  );
};
