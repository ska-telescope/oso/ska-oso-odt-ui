import { Card, CardContent } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { useFormContext } from 'react-hook-form';
import { useState } from 'react';
import {
  LowArrayConfigTypeOptions,
  LowArrayConfigTypeEnum,
  DEFAULT_WEIGHTING_KEY_REF,
  StationApertureType,
  getLowDefaultConfig
} from '../../../models/sbDefinition/array/arrayConfig';
import { SubarrayBeamComponent } from './subarrayBeam';
import { ControlledSelect, ControlledTextField } from '../../shared/controlled';
import { OsoStaticInput } from '../../shared/osoStaticInput';
import { ConfirmDialog } from '../../shared/dialogs';

const ctrlName = 'array' as const;

export const LowArrayComponent = () => {
  const { setValue, getValues, handleSubmit } = useFormContext();

  const arrayConfigValue = getValues(ctrlName);

  const onManualStationChange = async () => {
    /****
     * When the user modifies stations manually (when custom array is selected), the apertures within
     * the subarray beam have to be modified to match the changed stations. This function loops through
     * the stations defined by the user, keeps any existing station apertures, deletes apertures for
     * stations that no longer exist and adds a default aperture for any new stations.
     * */
    const existingApertures = getValues(ctrlName).subarrayBeam.apertures;
    const stations = getValues(ctrlName).stations;
    const newApertures: StationApertureType = {};
    if (stations.length > 0) {
      stations.split(/\s*,\s*/).forEach((station: string) => {
        if (station in existingApertures) {
          newApertures[station] = existingApertures[station];
        } else {
          newApertures[station] = [
            {
              substationID: 1,
              weightingKey: DEFAULT_WEIGHTING_KEY_REF
            }
          ];
        }
      });
      setValue(`${ctrlName}.subarrayBeam.apertures`, newApertures);
    }
  };

  const [open, setOpen] = useState<boolean>(false);
  // Used to store the configuration the user has selected to change to while they confirm/cancel the dialog, before the form state has updated
  const [configurationToChangeTo, setConfigurationToChangeTo] =
    useState<string>('');

  const openArrayConfigConfirmationDialog = (
    configurationToChangeTo: string
  ): void => {
    setConfigurationToChangeTo(configurationToChangeTo);
    setOpen(true);
  };

  const handleConfirmClose = (): void => {
    // User has confirmed that any changes can be overwritten when array has changed,
    // set the form to default values for the new array
    const newConfig = getLowDefaultConfig(configurationToChangeTo);
    setValue(ctrlName, newConfig);
    setOpen(false);
  };

  const handleCancelClose = (): void => {
    // Change the selection back to the previous array configuration value
    setValue(`${ctrlName}.configuration`, arrayConfigValue.configuration);
    setOpen(false);
  };

  const onChangeConfiguration = (event): void => {
    // Check if any station apertures have been changed from the defaults, if yes,
    // open a dialog box to confirm with the user that changing the array will erase
    // these changes.
    const newConfiguration = event.target.value;
    if (
      !Object.values(getValues(ctrlName).subarrayBeam.apertures).every(
        (stationApertures) =>
          stationApertures.length === 1 &&
          stationApertures[0].weightingKey === DEFAULT_WEIGHTING_KEY_REF
      )
    ) {
      openArrayConfigConfirmationDialog(newConfiguration);
    } else {
      // User has made no changes to station apertures
      const newConfig = getLowDefaultConfig(newConfiguration);
      setValue(ctrlName, newConfig);
    }
  };

  const disableStationInput =
    arrayConfigValue.configuration != LowArrayConfigTypeEnum.Custom;

  return (
    <>
      <Card data-testid="sbArrayConfig" variant="outlined">
        <CardContent>
          <Grid container direction="row" width="100%" alignItems="center">
            <Grid size={{ xs: 3 }} padding="5px">
              <ControlledSelect
                name={`${ctrlName}.configuration`}
                label="Configuration"
                options={LowArrayConfigTypeOptions}
                onChange={onChangeConfiguration}
                withFormUpdate={false}
              />
            </Grid>
            <Grid size={{ xs: 7 }} padding="5px">
              <ControlledTextField
                id="stations"
                label="Stations"
                name={`${ctrlName}.stations`}
                onChange={
                  !disableStationInput
                    ? handleSubmit(onManualStationChange)
                    : undefined
                }
                disabled={disableStationInput}
              />
            </Grid>
            <Grid size={{ xs: 2 }} padding="5px">
              <OsoStaticInput
                name={`${ctrlName}.numberOfStations`}
                value={
                  arrayConfigValue.stations
                    ? arrayConfigValue.stations.split(',').length
                    : 0
                }
                label="Number of Stations"
              />
            </Grid>
          </Grid>
          <SubarrayBeamComponent />
        </CardContent>
      </Card>
      <ConfirmDialog
        title={'Overwriting changes'}
        content={
          'Changing the array selection will reset any existing changes.'
        }
        open={open}
        onCancelClose={handleCancelClose}
        onConfirmClose={handleConfirmClose}
      />
    </>
  );
};
