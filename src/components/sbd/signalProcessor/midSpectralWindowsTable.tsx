import React from 'react';
import { useFormContext } from 'react-hook-form';
import { range } from '@mui/x-data-grid/internals';

import { OperationsButtonGroup } from '../../shared/operationButtonGroup';
import {
  createTableDataFromFormValues,
  DisplayType,
  OsoDataTable,
  OsoDataTableColumnDefinitions,
  SelectOption
} from '../../shared';
import { MID_CHANNEL_WIDTH_KHZ } from '../../../lib/osd';
import { MidSpectralWindowSchema } from '../../../models/sbDefinition/signalProcessor/spectralWindow';
import { getMidResolutionDisplayValue } from '../../../store/mappers/signalProcessorMapper';
import { MidBandsDefaults } from '../../../models/sbDefinition/signalProcessor/spectralWindow';

interface Props {
  selectedSignalProcessorIndex: number;
}

export const MidSpectralWindowsTable = ({
  selectedSignalProcessorIndex
}: Props): JSX.Element => {
  const ctrlName =
    `signalProcessor.${selectedSignalProcessorIndex}.spectralWindows` as const;

  const { getValues, setValue } = useFormContext();

  const addMidSpectralWindow = () => {
    const band = getValues(
      `signalProcessor.${selectedSignalProcessorIndex}.band`
    );
    const bandDefaults = MidBandsDefaults.get(band);
    const newSpectralWindow = MidSpectralWindowSchema.parse({
      centreFrequencyMhz: bandDefaults?.centreMhz,
      bandwidthMhz: bandDefaults?.bandwidthMhz
    });
    setValue(ctrlName, [...(getValues(ctrlName) ?? []), newSpectralWindow]);
  };

  const deleteMidSpectralWindow = (index: number) => {
    setValue(ctrlName, getValues(ctrlName).toSpliced(index, 1));
  };

  const columns: OsoDataTableColumnDefinitions = [
    {
      displayType: [DisplayType.HiddenField],
      title: '',
      columnName: 'spwId',
      isIdField: true
    },
    {
      displayType: [DisplayType.StaticField],
      title: 'SPW',
      columnName: 'spw'
    },
    {
      displayType: [DisplayType.TextField],
      title: 'Mode',
      columnName: 'mode',
      disabled: true
    },
    {
      displayType: [DisplayType.NumberField],
      title: 'Centre Frequency (MHz)',
      columnName: 'centreFrequencyMhz'
    },
    {
      displayType: [DisplayType.NumberField],
      title: 'Bandwidth (MHz)',
      columnName: 'bandwidthMhz',
      stepSize: 20 * MID_CHANNEL_WIDTH_KHZ * 1e-3
    },
    {
      displayType: [DisplayType.SelectField],
      title: 'Av. Time (s)',
      columnName: 'averageTimeFactor',
      options: averageTimeOptions
    },
    {
      displayType: [DisplayType.DeleteButton],
      title: '',
      description: 'Click to delete this spectral window entry',
      columnName: 'delete',
      onClick: deleteMidSpectralWindow
    }
  ];

  // TODO this is a similar hack to the scans page, where a change to the form (in this case the centre frequency)
  // needs to trigger a change elsewhere in the form (the resolution). Previously this was done at the store level and
  // then rewritten to the form, but we removed that part of the loop.
  const tableContent = createTableDataFromFormValues(
    columns,
    getValues(ctrlName)
  );
  const resolutionIndexInTable = 5;
  const resolutions = getValues(ctrlName).map((spectralWindow) =>
    getMidResolutionDisplayValue(spectralWindow.centreFrequencyMhz * 1e6)
  );
  columns.splice(resolutionIndexInTable, 0, {
    displayType: [DisplayType.StaticField],
    title: 'Resolution (kHz, km/s)',
    columnName: 'resolution'
  });

  tableContent.forEach((row, index) =>
    row.splice(resolutionIndexInTable, 0, {
      columnName: 'resolution',
      value: resolutions[index]
    })
  );

  return (
    <div>
      <OsoDataTable baseName={ctrlName} data={tableContent} columns={columns} />
      <OperationsButtonGroup
        title="Correlation SPW"
        count={1}
        handleAdd={addMidSpectralWindow}
      />
    </div>
  );
};

const averageTimeOptions: SelectOption[] = range(1, 11).map((value) => ({
  value: value.toString(),
  label: (0.142 * value).toFixed(3).toString()
}));
