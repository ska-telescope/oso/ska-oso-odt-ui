import React from 'react';
import { render, screen } from '@testing-library/react';
import { LowSignalProcessorDetails } from './lowSignalProcessorDetails';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_LOW_SIGNAL_PROCESSOR
} from 'src/components/testUtils';

describe('LowSignalProcessorDetails component', () => {
  test('should display the data from the store in the correct fields', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_LOW_SIGNAL_PROCESSOR}
      >
        <LowSignalProcessorDetails selectedSignalProcessorIndex={0} />
      </WithSbDefinitionFormContext>
    );

    expect(screen.getByLabelText('Name').value).eq('CSP Config 123');
    expect(screen.getByLabelText('Do PST').value).eq('false');
  });

  test('should disable doPst when there are multiple SPWs', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_LOW_SIGNAL_PROCESSOR}
      >
        <LowSignalProcessorDetails selectedSignalProcessorIndex={0} />
      </WithSbDefinitionFormContext>
    );

    expect(screen.getByLabelText('Do PST')).toBeDisabled();
  });
});
