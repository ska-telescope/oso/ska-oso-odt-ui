import React from 'react';
import {
  ControlledCheckbox,
  ControlledTextField
} from '../../shared/controlled';
import Grid from '@mui/material/Grid2';
import { LowSpectralWindowsTable } from './lowSpectralWindowsTable';
import { useFormContext } from 'react-hook-form';
import {
  LowSpectralWindowSchemaType,
  SpectralWindowMode
} from '../../../models/sbDefinition/signalProcessor/spectralWindow';

interface Props {
  selectedSignalProcessorIndex: number;
}

export const LowSignalProcessorDetails = ({
  selectedSignalProcessorIndex
}: Props): JSX.Element => {
  const ctrlName = `signalProcessor.${selectedSignalProcessorIndex}` as const;

  const { getValues } = useFormContext();

  const doPstDisabled =
    getValues(ctrlName).spectralWindows.filter(
      (spectralWindow: LowSpectralWindowSchemaType) =>
        spectralWindow.mode === SpectralWindowMode.enum.CORR
    ).length > 1;

  return (
    <Grid
      key={selectedSignalProcessorIndex}
      container
      direction="row"
      rowSpacing={4}
      columnSpacing={1}
    >
      <Grid size={{ xs: 4 }}>
        <ControlledTextField label="Name" name={`${ctrlName}.name`} />
      </Grid>
      <Grid size={{ xs: 4 }}>
        <ControlledCheckbox
          label="Do PST"
          name={`${ctrlName}.doPst`}
          disabled={doPstDisabled}
        />
      </Grid>
      <Grid size={{ xs: 4 }} />
      <Grid size={{ xs: 12 }}>
        <LowSpectralWindowsTable
          selectedSignalProcessorIndex={selectedSignalProcessorIndex}
        ></LowSpectralWindowsTable>
      </Grid>
    </Grid>
  );
};
