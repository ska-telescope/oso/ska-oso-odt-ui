import React from 'react';
import { act, fireEvent, render, screen, within } from '@testing-library/react';
import { MidSignalProcessorDetails } from './midSignalProcessorDetails';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_MID_SIGNAL_PROCESSOR
} from 'src/components/testUtils';
import { getSpectralWindowsTableRows } from './midSpectralWindowsTable.test';

describe('MidSignalProcessorDetails component', () => {
  test('should display the data from the store in the correct fields', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_MID_SIGNAL_PROCESSOR}
      >
        <MidSignalProcessorDetails selectedSignalProcessorIndex={0} />
      </WithSbDefinitionFormContext>
    );

    expect(screen.getByLabelText('Name').value).eq('Config 123');
    expect(screen.getByLabelText('Band').textContent).eq('1');
    expect(screen.getByLabelText('FS Offset (MHz)').value).eq('0');
  });

  test('changing the band should change the bandwidth of the spectralWindows to the default', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_MID_SIGNAL_PROCESSOR}
      >
        <MidSignalProcessorDetails selectedSignalProcessorIndex={0} />
      </WithSbDefinitionFormContext>
    );

    const bandDropdown = screen.getByLabelText('Band');
    act(() => {
      fireEvent.mouseDown(bandDropdown);
    });
    const band2Option = screen.getByText('2');
    act(() => {
      fireEvent.click(band2Option);
    });

    const rows = getSpectralWindowsTableRows();
    const dataCells = within(rows[1]).getAllByRole('cell');
    expect(within(dataCells[3]).getByRole('spinbutton').value).eq('669.8496');
  });
});
