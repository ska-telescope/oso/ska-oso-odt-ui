import { SvgIcon, TextField, TextFieldProps } from '@mui/material';
import TableHead from '@mui/material/TableHead';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import DoubleArrowIcon from '@mui/icons-material/DoubleArrow';
import DeleteIcon from '@mui/icons-material/DeleteOutlined';
import KeyboardDoubleArrowDownIcon from '@mui/icons-material/KeyboardDoubleArrowDown';
import EditIcon from '@mui/icons-material/Edit';
import KeyboardDoubleArrowUpIcon from '@mui/icons-material/KeyboardDoubleArrowUp';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import { Paper } from '@mui/material';
import Table from '@mui/material/Table';
import { FormControlProps } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import {
  ControlledTextField,
  ControlledNumberField,
  ControlledTimeField,
  ControlledSelect
} from './controlled';
import { SelectOption } from './select';
import { OsoIconButton } from './iconButton';

type OsoDataTableHeadProps = {
  titles: string[];
  name?: string;
};

const OsoDataTableHead = ({
  titles,
  name
}: OsoDataTableHeadProps): JSX.Element => {
  const theme = useTheme();
  return (
    <TableHead
      sx={{
        backgroundColor: theme.palette.primary.dark,
        color: theme.palette.secondary.contrastText,
        fontWeight: 900
      }}
    >
      <TableRow key={name}>
        {titles.map((header: string, index: number) => (
          <TableCell
            key={index}
            component="th"
            scope="row"
            style={{ fontWeight: 800 }}
          >
            {header}
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export enum DisplayType {
  /***
   *  Describes the list of available component types to include within
   *  the OsoTable.
   **/
  StaticField = 'StaticField',
  TextField = 'TextField',
  SelectField = 'SelectField',
  NumberField = 'NumberField',
  TimeField = 'TimeField',
  HiddenField = 'HiddenField',
  DeleteButton = 'DeleteButton',
  AddButton = 'AddButton',
  EditButton = 'EditButton',
  MoveUpButton = 'MoveUpButton',
  MoveDownButton = 'MoveDownButton'
}

type OsoDataTableBaseColumnDefinition = {
  displayType: [DisplayType] | [DisplayType, DisplayType];
  title: string;
  columnName: string;
  isIdField?: boolean;
  id?: string;
  displayTypeCondition?: ValueCondition;
  disabled?: boolean;
  // A simple check to see if a field within the same row has a certain value. Overrides the disabled prop.
  disabledByValueCondition?: ValueCondition;
};

type OsoDataTableControlColumnDefinition = {
  options?: SelectOption[];
  stepSize?: number;
  min?: number;
  max?: number;
  onChange?: () => void;
} & FormControlProps &
  OsoDataTableBaseColumnDefinition;

type OsoDataTableButtonColumnDefinition = {
  description?: string;
  onClick?: (index: number) => void;
} & OsoDataTableBaseColumnDefinition;

export type OsoDataTableColumnDefinitions = (
  | OsoDataTableControlColumnDefinition
  | OsoDataTableButtonColumnDefinition
)[];

type OsoDataTableCellContent = {
  columnName: string;
  value?: string | number;
};

export type ValueCondition = {
  // Input to a check if the fieldName has the given value
  value: string;
  fieldName: string;
  negate?: boolean;
};

const checkConditionOnRow = (
  row: OsoDataTableCellContent[],
  valueCondition: ValueCondition
): boolean => {
  /***
   *  Applies the ValueCondition to the row.
   *
   *  i.e. check if the given data row has a field with the fieldName from the condition, then checks if the
   *  value of that field matches the value in the condition (or does not match, if the negate flag is true).
   *
   **/
  // check the field value against the string
  const matched =
    row.find((cellData) => cellData.columnName == valueCondition.fieldName)
      ?.value === valueCondition.value;
  // check whether to negate the result
  return valueCondition.negate ? !matched : matched;
};

export const createTableDataFromFormValues = (
  columns: OsoDataTableColumnDefinitions,
  formData: object[]
): OsoDataTableCellContent[][] => {
  /***
   *  Converts RHF form values into a structure required by the OsoTable based on given set of column definitions.
   **/
  return formData.map((formField: object) =>
    columns.map((column) => ({
      columnName: column.columnName,
      // The index column (if exists) is not part of the form fields, in this case leave the value empty
      // as it cannot be retrieved from the form values and will be filled automatically by the table
      ...(column.columnName in formField
        ? { value: formField[column.columnName as keyof typeof formField] }
        : {})
    }))
  );
};

const isCellDisabled = (
  rowData: OsoDataTableCellContent[],
  disabledByValueCondition: ValueCondition
): boolean => {
  return checkConditionOnRow(rowData, disabledByValueCondition);
};

type OsoComponentByTypeProps = {
  baseName: string;
  index: number;
  rowData: OsoDataTableCellContent[];
  cell: OsoDataTableControlColumnDefinition;
};

const isMoveArrowDisabled = (
  displayType: DisplayType,
  index: number,
  max: number
) => {
  // For the arrows to move rows up and down in the table, we want the up arrow to be disabled for the top row and the down
  // arrow disabled for the bottom row
  if (displayType === DisplayType.MoveUpButton) {
    return index === 0;
  } else if (displayType === DisplayType.MoveDownButton) {
    return index === max - 1;
  }
  return false;
};

const OsoComponentByType = ({
  index,
  baseName,
  rowData,
  cell
}: OsoComponentByTypeProps): JSX.Element => {
  const {
    displayType,
    displayTypeCondition,
    disabled,
    disabledByValueCondition,
    // If not extracted here, isIdField would be passed to the
    // component as part of 'otherProps', so disable lint warning
    // eslint-disable-next-line @typescript-eslint/no-unused-vars
    isIdField,
    options,
    columnName,
    onChange,
    stepSize,
    min,
    max,
    ...otherProps
  } = cell;
  const cellData = rowData.find(
    (cellData) => cellData.columnName == columnName
  );
  if (cellData === undefined) {
    throw new Error(`No data found in row ${index} for column ${columnName}`);
  }

  let cellDisabled = disabled ?? false;
  if (disabledByValueCondition) {
    cellDisabled = isCellDisabled(rowData, disabledByValueCondition);
  }

  const cellDisplayType = displayTypeCondition
    ? checkConditionOnRow(rowData, displayTypeCondition)
      ? displayType[0]
      : displayType[1]
    : displayType[0];

  const ctrlName = `${baseName}.${index}.${cellData.columnName}`;

  switch (cellDisplayType) {
    case DisplayType.StaticField:
      return (
        <TextField
          sx={{ width: '100%' }}
          disabled
          value={cellData.value ?? index + 1}
          {...(otherProps as TextFieldProps)}
        />
      );

    case DisplayType.TextField:
      return (
        <ControlledTextField
          disabled={cellDisabled}
          name={ctrlName}
          onChange={onChange}
          {...(otherProps as FormControlProps)}
        />
      );

    case DisplayType.NumberField:
      return (
        <ControlledNumberField
          showArrows={true}
          stepSize={stepSize}
          min={min}
          max={max}
          disabled={cellDisabled}
          name={ctrlName}
          onChange={onChange}
          {...(otherProps as FormControlProps)}
        />
      );

    case DisplayType.TimeField:
      return (
        <ControlledTimeField
          showArrows={true}
          disabled={cellDisabled}
          name={ctrlName}
          onChange={onChange}
          {...(otherProps as FormControlProps)}
        />
      );

    case DisplayType.SelectField: {
      if (Array.isArray(options)) {
        return (
          <ControlledSelect
            disabled={cellDisabled}
            name={ctrlName}
            options={options}
            onChange={onChange}
            {...(otherProps as FormControlProps)}
          />
        );
      } else {
        throw new Error(`Bad options for Select. options=${options}`);
      }
    }
    default:
      throw new Error(
        `Unable to determine component display type: '${displayType}'`
      );
  }
};

type OsoButtonComponentByTypeProps = {
  index: number;
  baseName: string;
  rowData: OsoDataTableCellContent[];
  cell: OsoDataTableButtonColumnDefinition;
  max: number;
};

const OsoButtonToIconMap = new Map<DisplayType, typeof SvgIcon>([
  [DisplayType.DeleteButton, DeleteIcon],
  [DisplayType.AddButton, DoubleArrowIcon],
  [DisplayType.EditButton, EditIcon],
  [DisplayType.MoveUpButton, KeyboardDoubleArrowUpIcon],
  [DisplayType.MoveDownButton, KeyboardDoubleArrowDownIcon]
]);

const OsoButtonComponentByType = ({
  index,
  baseName,
  rowData,
  cell,
  max
}: OsoButtonComponentByTypeProps): JSX.Element => {
  const { description, onClick, disabled, disabledByValueCondition } = cell;
  const displayType = cell.displayType[0];
  const btnName = `${baseName}.${index}.${cell.columnName}`;

  let cellDisabled = disabled || isMoveArrowDisabled(displayType, index, max);
  if (disabledByValueCondition) {
    cellDisabled = isCellDisabled(rowData, disabledByValueCondition);
  }

  return (
    <OsoIconButton
      index={index}
      name={btnName}
      disabled={cellDisabled}
      handleClick={!cellDisabled ? onClick : undefined}
      description={description}
      Icon={OsoButtonToIconMap.get(displayType)!}
    />
  );
};

type OsoDataTableBodyProps = {
  baseName: string;
  data: OsoDataTableCellContent[][];
  columns: OsoDataTableColumnDefinitions;
};

const OsoDataTableBody = ({
  baseName,
  data = [],
  columns = []
}: OsoDataTableBodyProps): JSX.Element => {
  const idColumnName = columns.find((column) => column.isIdField)?.columnName;
  if (!idColumnName) {
    throw new Error(
      "No ID field found. A column with unique values has to be marked as an ID field by setting 'isIdField' to true"
    );
  }
  const max = data.length;
  return (
    <TableBody>
      {data &&
        data.map((row: OsoDataTableCellContent[], index: number) => {
          const idColumnValue = row.find(
            (field) => field.columnName == idColumnName
          )?.value;
          return (
            <TableRow key={`${baseName}-${idColumnValue}`}>
              {columns &&
                columns.map(
                  (
                    cell:
                      | OsoDataTableControlColumnDefinition
                      | OsoDataTableButtonColumnDefinition,
                    key: number
                  ) =>
                    cell.displayType[0] != DisplayType.HiddenField && (
                      <TableCell
                        key={`${baseName}-${idColumnValue}-${key}`}
                        sx={{
                          ['&.MuiTableCell-root']: {
                            padding: '2px'
                          }
                        }}
                      >
                        {OsoButtonToIconMap.has(cell.displayType[0]) ? (
                          <OsoButtonComponentByType
                            baseName={baseName}
                            index={index}
                            cell={cell as OsoDataTableButtonColumnDefinition}
                            rowData={row}
                            max={max}
                          />
                        ) : (
                          <OsoComponentByType
                            baseName={baseName}
                            index={index}
                            rowData={row}
                            cell={cell as OsoDataTableControlColumnDefinition}
                          />
                        )}
                      </TableCell>
                    )
                )}
            </TableRow>
          );
        })}
    </TableBody>
  );
};

export type OsoDataTableProps = {
  baseName: string;
  data: OsoDataTableCellContent[][];
  columns: OsoDataTableColumnDefinitions;
};

export const OsoDataTable = ({
  baseName,
  data = [],
  columns = []
}: OsoDataTableProps): JSX.Element => {
  /***
   *  Creates a data table where each column is either an RHF Controlled components or an IconButton that performs an
   *  action for the row.
   *
   *  @param baseName - the base name for the controlled components within the table, e.g. 'signalProcessor.1.spectralWindows'
   *    for the spectral window data of the second signal processor defined within the form
   *  @param data - the data to be displayed within the table as an array of rows where each row is an array containing
   *    a value for each column.
   *  @param columns - an array describing details for each column within the table. Each column should be either a controlled
   *    component (OsoDataTableControlColumnDefinition) or an action IconButton (OsoDataTableButtonColumnDefinition). The
   *    columnName for a controlled component column should match the form field name.
   * */
  // Filter out titles for hidden columns
  const titles = columns
    .filter((column) => column.displayType[0] != DisplayType.HiddenField)
    .map((column) => column.title);
  return (
    <TableContainer component={Paper}>
      <Table aria-label={baseName}>
        <OsoDataTableHead titles={titles} />
        <OsoDataTableBody baseName={baseName} data={data} columns={columns} />
      </Table>
    </TableContainer>
  );
};
