import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test, vi } from 'vitest';
import { ProjectInfo } from './projectInfo';
import { TelescopeType } from '../../generated/models/telescope-type';
import { useStore } from '../../store/store';
import { useProjectStore } from '../../store/projectStore';

const { setPdmSbDefinition } = useStore.getState();
const { setPdmProject } = useProjectStore.getState();

vi.mock('react-router-dom', async () => {
  const actual =
    await vi.importActual<typeof import('react-router-dom')>(
      'react-router-dom'
    );
  return {
    ...actual,
    useLocation: () => ({
      pathname: '/sbd/'
    })
  };
});

describe('ProjectInfo', () => {
  test('low telescope and blank fields should return appropriate components', () => {
    setPdmSbDefinition({
      telescope: TelescopeType.SkaLow,
      sbd_id: 'sbd-123'
    });
    setPdmProject({
      prj_id: 'prj-123',
      name: 'Test Project'
    });

    const { container } = render(<ProjectInfo />);

    const spans = container.querySelectorAll('span');
    expect(spans[0].innerHTML).toBe('Test Project (prj-123) - sbd-123');
    const telescopeLabel = screen.getByText('Low');
    expect(telescopeLabel).toBeInTheDocument();
    expect(telescopeLabel).toBeVisible();
  });
});
