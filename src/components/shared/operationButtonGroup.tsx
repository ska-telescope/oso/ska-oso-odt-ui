import React from 'react';
import { Button, Typography } from '@mui/material';
import Grid from '@mui/material/Grid2';
export type PopupDialogProps = {
  showTitle?: boolean;
  title: string;
  count?: number;
  handleAdd: () => void | null;
  handleDelete?: () => void | null;
  disabled?: boolean;
};

export const OperationsButtonGroup = ({
  showTitle,
  title,
  count = 0,
  handleAdd,
  handleDelete,
  disabled = false
}: PopupDialogProps): JSX.Element => {
  const displayTitle = showTitle ?? false;
  const nameOnly = title.replaceAll(' ', '');
  const name = nameOnly.toLowerCase();

  return (
    <Grid
      container
      padding={2}
      direction="row"
      paddingTop="15px"
      paddingLeft="0px"
      alignItems="center"
    >
      {displayTitle && (
        <>
          <Typography
            data-testid={`${name}Title`}
            variant="body1"
            fontWeight="bolder"
          >
            {count === 0 ? title : `${title} (${count})`}
          </Typography>
          &nbsp;&nbsp;&nbsp;&nbsp;
        </>
      )}
      <Button
        type="button"
        aria-label={`Add New ${title}`}
        data-testid={`btnAdd${nameOnly}`}
        variant="contained"
        color="secondary"
        onClick={handleAdd}
        disabled={disabled}
      >
        ADD {title.toUpperCase()}
      </Button>
      &nbsp;&nbsp;&nbsp;&nbsp;
      {handleDelete && (
        <Button
          type="button"
          aria-label={`Delete Existing ${title}`}
          data-testid={`btnDelete${nameOnly}`}
          variant="contained"
          color="warning"
          disabled={disabled || count <= 0}
          onClick={handleDelete}
        >
          DELETE {title.toUpperCase()}
        </Button>
      )}
    </Grid>
  );
};
