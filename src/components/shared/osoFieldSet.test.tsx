import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { OsoFieldSet } from './osoFieldSet';

describe('OsoTitleWrapper', () => {
  test('should render with title and child component', () => {
    const title = 'FieldSet Test Title';
    render(
      <OsoFieldSet title={title}>
        <div></div>
      </OsoFieldSet>
    );
    const fieldSetComponent = screen.getByRole('group', { name: title });
    expect(fieldSetComponent).toBeInTheDocument();
    // There should be two children, a title legend and div
    expect(fieldSetComponent.children).toHaveLength(2);
  });

  test('should render without title', () => {
    render(
      <OsoFieldSet>
        <p id="xyz"></p>
      </OsoFieldSet>
    );
    const fieldSetComponent = screen.getByRole('group');
    expect(fieldSetComponent).toBeInTheDocument();
    // There should be only one child since title legend should not exist
    expect(fieldSetComponent.children).toHaveLength(1);
  });
});
