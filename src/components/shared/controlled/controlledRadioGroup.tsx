import React from 'react';
import { FormControlLabel, Radio, RadioGroup } from '@mui/material';
import type { SyntheticEvent } from 'react';
import { SelectOption } from '../ui/select';
import { Controller, useFormContext } from 'react-hook-form';
import { OsoFieldSet } from '../index';

type RadioButtonGroupProps = {
  name: string;
  options: SelectOption[];
  /* optional fields */
  title?: string;
  value?: string;
  testId?: string;
  horizontal?: boolean;
  handleChange?: (optionValue: string) => void;
  children?: string | JSX.Element | JSX.Element[];
};

export const ControlledRadioButtonGroup = ({
  name,
  options,
  testId,
  title,
  handleChange,
  children,
  horizontal
}: RadioButtonGroupProps) => {
  const testIdUsed = testId ?? name;
  const { control } = useFormContext();
  return (
    <Controller
      control={control}
      name={name}
      render={({ field }) => (
        <OsoFieldSet title={title}>
          <RadioGroup
            row={horizontal ?? false}
            name={name}
            value={field.value}
            onChange={(evt: SyntheticEvent) => {
              field.onChange(evt);
              if (handleChange) {
                handleChange((evt.target as HTMLInputElement).value);
              }
            }}
          >
            {options
              .filter(Boolean)
              .map((option: SelectOption, index: number) => {
                const optionId =
                  testIdUsed + '-radio-' + option.value.replaceAll(' ', '');
                return (
                  <FormControlLabel
                    style={{
                      marginRight: '20px'
                    }}
                    data-cy={optionId}
                    id={optionId}
                    key={index}
                    value={option.value}
                    control={<Radio color={'default'} />}
                    label={option.label}
                  />
                );
              })}
          </RadioGroup>
          {children ?? null}
        </OsoFieldSet>
      )}
    />
  );
};
