import React from 'react';
import { useTheme } from '@mui/material/styles';

type TitleWrapperProps = {
  title?: string;
  testId?: string;
  children?: string | JSX.Element | JSX.Element[];
};

export const OsoFieldSet = ({
  title,
  testId,
  children,
  ...otherProps
}: TitleWrapperProps) => {
  const theme = useTheme();

  return (
    <fieldset
      style={{
        marginTop: '0px',
        borderRadius: '10px',
        border: '1px solid ' + theme.palette.primary.dark,
        width: '100%'
      }}
      {...otherProps}
    >
      {title && (
        <OsoTitle
          title={title}
          testId={testId ?? `${title?.replaceAll(' ', '')}-title`}
        />
      )}
      {children}
    </fieldset>
  );
};

const OsoTitle = ({ title, testId }: TitleWrapperProps) => {
  const theme = useTheme();
  return title ? (
    <legend
      style={{
        fontSize: '12px',
        color: theme.palette.primary.dark,
        marginBottom: '5px'
      }}
      id={testId}
      data-testid={testId}
      data-cy={testId}
    >
      &nbsp;{title}&nbsp;
    </legend>
  ) : null;
};
