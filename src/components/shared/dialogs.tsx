import React from 'react';
import {
  Button,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle
} from '@mui/material';
import DialogContentText from '@mui/material/DialogContentText';

const ConfirmDialog = ({
  title,
  content,
  open,
  onCancelClose,
  onConfirmClose
}: {
  title: string;
  content: string;
  open: boolean;
  onCancelClose: () => void;
  onConfirmClose: () => void;
}) => {
  return (
    <Dialog
      open={open}
      onClose={onCancelClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {content}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="primary" variant="contained" onClick={onCancelClose}>
          Cancel
        </Button>
        <Button
          color="primary"
          variant="contained"
          onClick={onConfirmClose}
          autoFocus
        >
          OK
        </Button>
      </DialogActions>
    </Dialog>
  );
};

const AlertDialog = ({
  title,
  content,
  open,
  onClose
}: {
  title: string;
  content: string;
  open: boolean;
  onClose: () => void;
}) => {
  return (
    <Dialog
      open={open}
      onClose={onClose}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{title}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {content}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button color="primary" variant="contained" onClick={onClose}>
          OK
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export { AlertDialog, ConfirmDialog };
