import '@testing-library/jest-dom';
import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { OsoContentTitle } from './contentTitle';
import { Variant } from '@mui/material/styles/createTypography';

describe('OsoContentTitle', () => {
  const title = 'Ska Radio Group Box';
  const id = 'content-test-id';
  const variant: Variant = 'h3';

  const checkContent = (
    container: HTMLElement,
    searchTitle: string,
    testId: string,
    headingType: string
  ) => {
    const titleField = screen.getByRole('heading', { name: searchTitle });
    expect(titleField).toBeInTheDocument();
    expect(titleField).toBeVisible();
    expect(titleField).toHaveAttribute('data-testid', testId);
    const h5 = container.querySelector(headingType);
    expect(h5).toBeInTheDocument();
    expect(h5).toBeVisible();
    expect(h5).toHaveAttribute('data-testid', testId);
  };

  test('with title only should return appropriate components', () => {
    const { container } = render(<OsoContentTitle title={title} />);

    checkContent(container, title, 'functionId', 'h5');
  });

  test('with title & testId should return appropriate components', () => {
    const { container } = render(<OsoContentTitle title={title} testId={id} />);

    checkContent(container, title, id, 'h5');
  });

  test('with title, testId & variant should return appropriate components', () => {
    const { container } = render(
      <OsoContentTitle title={title} testId={id} variant={variant} />
    );

    checkContent(container, title, id, variant);
  });
});
