import React from 'react';
import { TextField, TextFieldProps } from '@mui/material';

export const OsoStaticInput = ({
  label,
  name,
  value,
  variant = 'outlined',
  fullWidth = true,
  type = 'text',
  id = undefined,
  ...otherProps
}: TextFieldProps): JSX.Element => {
  return (
    <TextField
      value={value ?? ''}
      label={label}
      type={type}
      variant={variant}
      fullWidth={fullWidth}
      id={id ?? name}
      disabled
      {...otherProps}
    />
  );
};
