import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { OdtHeader } from './odtHeader';

describe('OdtHeader', () => {
  test('dark mode should return appropriate components', () => {
    render(
      <OdtHeader mode="dark" title="Observation Design Tool" skao="SKAO" />
    );
    const logo = screen.getByRole('img', { name: 'SKAO Logo' });
    expect(logo).toBeInTheDocument();
    expect(logo).toBeVisible();
    expect(logo).not.toBeNull();

    const HelpIcon = screen.getByRole('button', { name: /document icon/i });
    expect(HelpIcon).toBeInTheDocument();
    expect(HelpIcon).toBeVisible();
  });

  test('light mode should return appropriate components', () => {
    render(
      <OdtHeader mode="light" title="Observation Design Tool" skao="SKAO" />
    );

    // light mode icons appear
    const HelpIcon = screen.getByRole('button', { name: /document icon/i });
    expect(HelpIcon).toBeInTheDocument();
    expect(HelpIcon).toBeVisible();
  });

  test('click help button should return appropriate components', () => {
    render(
      <OdtHeader mode="light" title="Observation Design Tool" skao="SKAO" />
    );
    // two button components
    const helpButton = screen.getByRole('button', {
      name: /document icon/i
    });
    helpButton.click();
  });
});
