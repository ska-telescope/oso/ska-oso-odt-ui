import { Header } from '@ska-telescope/ska-gui-components';

const ODT_DOCUMENT =
  'https://developer.skao.int/projects/ska-oso-odt-ui/en/latest/';

interface OdtHeaderProps {
  title: string;
  skao: string;
  themeMode: string;
  toggleThemeMode: typeof Function;
}

// Interfaces to ska-gui-components
// Trevor will add exports and we can import directly
// and remove these:
interface Help {
  content: typeof Object;
  component: typeof Object;
  showHelp: boolean;
}

interface Telescope {
  code: string;
  name: string;
  location: string;
  position: {
    lat: number;
    lon: number;
  };
  image: string;
}

interface Storage {
  help?: Help;
  helpToggle?: typeof Function;
  telescope?: Telescope;
  themeMode: string;
  toggleTheme: typeof Function;
  updateTelescope?: typeof Function;
}

// /end ska-gui-component interfaces

export const OdtHeader = ({
  title,
  skao,
  themeMode,
  toggleThemeMode
}: OdtHeaderProps): JSX.Element => {
  const skaHeaderProps: Storage = {
    themeMode: themeMode,
    toggleTheme: toggleThemeMode
  };

  return (
    <Header
      aria-description="SKA ODT header"
      testId="skaoLogo"
      data-cy="skaoLogo"
      title={title}
      toolTip={{ skao, mode: themeMode }}
      selectTelescope={false}
      showHelp={false}
      docs={{
        tooltip: 'SKA OSO ODT UI Guide',
        url: ODT_DOCUMENT
      }}
      storage={skaHeaderProps}
    />
  );
};
