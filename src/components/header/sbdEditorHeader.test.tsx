import { render, screen } from '@testing-library/react';
import { describe, expect, test, vi } from 'vitest';
import { SbdEditorHeader } from './sbdEditorHeader';
import {
  TEST_FORM_MID_SB_DEFINITION,
  WithSbDefinitionFormContext
} from '../testUtils';
import { TelescopeType } from '../../generated/models/telescope-type';
import { mockedUseNavigate } from '../../setUpTests';

vi.mock('react-router-dom', async () => {
  const actual =
    await vi.importActual<typeof import('react-router-dom')>(
      'react-router-dom'
    );
  return {
    ...actual,
    useLocation: () => ({
      pathname: '/sbd/'
    }),
    useNavigate: () => mockedUseNavigate
  };
});
describe('SbdEditorHeader', () => {
  test('dark mode should return appropriate components', () => {
    render(
      <WithSbDefinitionFormContext
        telescopeType={TelescopeType.SkaMid}
        initialState={TEST_FORM_MID_SB_DEFINITION}
      >
        <SbdEditorHeader />
      </WithSbDefinitionFormContext>
    );

    const projectInfo = screen.getByText(/SKA Project/i);
    expect(projectInfo).toBeInTheDocument();
    expect(projectInfo).toBeVisible();

    const telescope = screen.getByText(/mid/i);
    expect(telescope).toBeInTheDocument();
    expect(telescope).toBeVisible();

    const homeButton = screen.getByRole('button', {
      name: /home/i
    });
    expect(homeButton).toBeInTheDocument();
    expect(homeButton).toBeVisible();
    expect(homeButton).toHaveAttribute('data-testid', 'btnHome');
    expect(homeButton).toHaveAttribute(
      'aria-describedby',
      'Click to go to the home page'
    );
    expect(homeButton).toHaveAttribute('aria-label', 'Home');
    expect(homeButton).toHaveAttribute('type', 'submit');

    const saveOdaButton = screen.getByRole('button', {
      name: /save to oda/i
    });
    expect(saveOdaButton).toBeInTheDocument();
    expect(saveOdaButton).toBeVisible();
    expect(saveOdaButton).toHaveAttribute('data-testid', 'save-sb');
    expect(saveOdaButton).toHaveAttribute(
      'aria-describedby',
      'Click to save the changes back to backend'
    );
    expect(saveOdaButton).toHaveAttribute('aria-label', 'Save to ODA');
    expect(saveOdaButton).toHaveAttribute('type', 'submit');

    const validateButton = screen.getByRole('button', {
      name: /validate/i
    });
    expect(validateButton).toBeInTheDocument();
    expect(validateButton).toHaveAttribute('data-testid', 'validate-sb');
    expect(validateButton).toHaveAttribute(
      'aria-describedby',
      'Click to validate current SB'
    );
    expect(validateButton).toHaveAttribute('aria-label', 'Validate');
    expect(validateButton).toHaveAttribute('type', 'submit');

    const markToReadyButton = screen.getByRole('button', {
      name: /mark as read/i
    });
    expect(markToReadyButton).toBeInTheDocument();
    expect(markToReadyButton).toHaveAttribute(
      'data-testid',
      'mark-sbd-as-ready'
    );
    expect(markToReadyButton).toHaveAttribute(
      'aria-describedby',
      'Mark the current SBDefinition as ready.'
    );
    expect(markToReadyButton).toHaveAttribute('aria-label', 'Mark as ready');
    expect(markToReadyButton).toHaveAttribute('type', 'submit');
  });
});
