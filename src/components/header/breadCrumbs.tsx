import React from 'react';
import { Step, StepLabel, Stepper, useTheme } from '@mui/material';
import { useLocation, useNavigate } from 'react-router-dom';
import { RoutePaths } from '../app/routes';

type BreadCrumbsObject = {
  title: string;
  path: string;
};

export const BreadCrumbs = () => {
  const { pathname } = useLocation();
  const navigate = useNavigate();

  const BreadCrumbsList: BreadCrumbsObject[] = [
    {
      title: 'General',
      path: RoutePaths.SbdEditor.General
    },
    {
      title: 'Scripts',
      path: RoutePaths.SbdEditor.Scripts
    },
    {
      title: 'Array',
      path: RoutePaths.SbdEditor.Array
    },
    {
      title: 'Targets',
      path: RoutePaths.SbdEditor.Targets
    },
    {
      title: 'Signal Processor',
      path: RoutePaths.SbdEditor.SignalProcessor
    },
    {
      title: 'Scans',
      path: RoutePaths.SbdEditor.Scans
    },
    {
      title: 'Data Processing',
      path: RoutePaths.SbdEditor.DataProcessing
    }
  ];

  const checkUrl = (pathToNavigate: string) => {
    if (pathToNavigate !== pathname) {
      navigate(pathToNavigate);
    }
  };

  const createNumber = (index: number, name: string, path: string) => {
    const theme = useTheme();

    const style = {
      '& .MuiStepLabel-label': {
        color:
          pathname === path
            ? theme.palette.secondary.main
            : theme.palette.primary.dark
      },
      '& .MuiStepIcon-text': {
        fill: theme.palette.primary.main
      },
      '& .MuiStepIcon-root': {
        color:
          pathname === path
            ? theme.palette.secondary.main
            : theme.palette.primary.dark
      },
      marginTop: '20px'
    };
    return (
      <Step
        key={index}
        active={pathname === path}
        sx={style}
        onClick={() => checkUrl(path)}
      >
        <StepLabel data-testid={name} sx={{ cursor: 'pointer' }}>
          {name}
        </StepLabel>
      </Step>
    );
  };

  return (
    <Stepper
      // activeStep={matchedStep}
      variant="outlined"
      alternativeLabel
      // sx={{cursor: 'pointer'}}
    >
      {BreadCrumbsList.map((item: BreadCrumbsObject, index: number) =>
        createNumber(index + 1, item.title, item.path)
      )}
    </Stepper>
  );
};
