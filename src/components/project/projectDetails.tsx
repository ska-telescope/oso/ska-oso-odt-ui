import { Stack } from '@mui/material';
import { OsoFieldSet } from '../shared';
import { ControlledTextField } from '../shared/controlled';

export const ProjectDetails = () => {
  return (
    <OsoFieldSet title="General">
      <Stack spacing={2}>
        <ControlledTextField name={`name`} label="Name" />
        <ControlledTextField name={`code`} label="Code" disabled />
        <ControlledTextField name={`type`} label="Type" />
        <ControlledTextField name={`status`} label="Status" disabled />
      </Stack>
    </OsoFieldSet>
  );
};
