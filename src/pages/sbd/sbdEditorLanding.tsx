import React from 'react';
import { Box, Typography } from '@mui/material';
import Grid from '@mui/material/Grid2';
import {
  DropDown,
  Button,
  ButtonVariantTypes,
  ButtonColorTypes
} from '@ska-telescope/ska-gui-components';
import { useNavigate } from 'react-router-dom';
import { useStore } from '../../store/store';
import { OsoContentTitle } from '../../components/shared/contentTitle';
import { OsoFieldSet } from '../../components/shared/osoFieldSet';
import ImportSb from '../../components/sbd/importSb';
import { TelescopeType } from '../../generated/models/telescope-type';
import { RoutePaths } from '../../components/app/routes';
import { DEFAULT_INITIALISED_SB } from '../../store/defaults';
import { createSelectOptions } from 'src/components/shared/controlled/controlledSelect';

export const SbdEditorLanding = (): JSX.Element => {
  const navigate = useNavigate();
  const [sbDefinition, newSbDefinition] = useStore((state) => [
    state.sbDefinition,
    state.newSbDefinition
  ]);

  const createANewSB = (type: string) => {
    // This landing page does not sit under a form context, so we do not need to update the form
    newSbDefinition(type as TelescopeType);
    navigate(RoutePaths.SbdEditor.General);
  };

  return (
    <Grid container rowSpacing={2} columnSpacing={3} pr={5} pl={5} pt={4}>
      <Grid size={{ xs: 12, md: 6, lg: 4 }}>
        <OsoFieldSet>
          <OsoContentTitle
            title="Continue with existing Scheduling Block Definition"
            sx={{ textAlign: 'center' }}
          />
          <Grid container alignItems="center" spacing={4} pt={2} pb={2}>
            <Grid size={{ xs: 12 }}>
              {sbDefinition !== DEFAULT_INITIALISED_SB ? (
                <Box textAlign={'center'} pt={2}>
                  {' '}
                  <Button
                    testId="btn-continue-sb"
                    data-testid="btn-continue-sb"
                    label="CLICK HERE"
                    onClick={() => navigate(RoutePaths.SbdEditor.General)}
                    color={ButtonColorTypes.Secondary}
                    variant={ButtonVariantTypes.Contained}
                  />
                </Box>
              ) : (
                <Typography>Local storage is currently empty.</Typography>
              )}
            </Grid>
          </Grid>
        </OsoFieldSet>
      </Grid>
      <Grid size={{ xs: 12, md: 6, lg: 4 }}>
        <OsoFieldSet>
          <OsoContentTitle
            title="Create a new Scheduling Block Definition"
            sx={{ textAlign: 'center' }}
          />
          <Grid
            container
            alignItems="center"
            spacing={4}
            pt={2}
            pb={2}
            data-testid="selectSb"
          >
            <Grid size={{ xs: 6 }}>
              <Typography>Select Telescope Type:</Typography>
            </Grid>
            <Grid size={{ xs: 6 }}>
              <DropDown
                ariaTitle="Choose Telescope"
                ariaDescription="Select the telescope type"
                testId="sbType"
                label=""
                value=""
                setValue={createANewSB}
                options={createSelectOptions(TelescopeType)}
              />
            </Grid>
          </Grid>
        </OsoFieldSet>
      </Grid>
      <Grid size={{ xs: 12, md: 6, lg: 4 }}>
        <OsoFieldSet>
          <OsoContentTitle
            testId="importTitle"
            title="Import Scheduling Block Definition"
            sx={{ textAlign: 'center' }}
          />
          <ImportSb />
        </OsoFieldSet>
      </Grid>
    </Grid>
  );
};
