import React from 'react';
import { act, fireEvent, render, screen, within } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { useStore } from '../../store/store';
import { SbdEditorLanding } from './sbdEditorLanding';
import { getDefaultSbDefinition } from '../../store/defaults';
import { TelescopeType } from '../../generated/models/telescope-type';

const { setPdmSbDefinition } = useStore.getState();

describe('SbdEditorLanding', () => {
  test('should return appropriate components', () => {
    render(<SbdEditorLanding />);

    expect(
      screen.getByRole('heading', {
        name: /continue with existing scheduling block definition/i
      })
    ).toBeInTheDocument();
    expect(
      screen.getByText(/local storage is currently empty\./i)
    ).toBeInTheDocument();

    expect(
      screen.getByRole('heading', {
        name: /create a new scheduling block definition/i
      })
    ).toBeInTheDocument();
    expect(screen.getByText(/select telescope type:/i)).toBeInTheDocument();

    const telescopeOptions = screen.getByRole('combobox');
    expect(telescopeOptions).toBeInTheDocument();
    expect(telescopeOptions).toBeVisible();
    expect(telescopeOptions).toHaveAttribute('aria-haspopup', 'listbox');
    expect(telescopeOptions).toHaveAttribute('aria-labelledby', 'dropDownId');
    expect(telescopeOptions).toHaveAttribute('id', 'dropDownId');

    const view = screen.getByTestId('sbType');

    const hiddenTextbox = within(view).getByRole('textbox', {
      hidden: true
    });
    expect(hiddenTextbox).toBeInTheDocument();
    expect(hiddenTextbox).toHaveAttribute('name', '');
    expect(hiddenTextbox).toHaveAttribute('value', '');

    expect(
      screen.getByRole('heading', {
        name: /import scheduling block definition/i
      })
    ).toBeInTheDocument();
    expect(screen.getByText(/load from oda:/i)).toBeInTheDocument();

    expect(screen.getByText(/enter an sbd identifier/i)).toBeInTheDocument();
    const loadSbButton = screen.getByRole('button', {
      name: /load sb/i
    });
    expect(loadSbButton).toBeInTheDocument();
    expect(loadSbButton).toBeVisible();
    expect(loadSbButton).toHaveAttribute('data-testid', 'btn-load-sb');
    expect(loadSbButton).toHaveAttribute('aria-label', 'LOAD SB');
    expect(loadSbButton).toHaveAttribute('type', 'submit');

    expect(
      screen.getByText(/import from local filesystem:/i)
    ).toBeInTheDocument();

    const filePicker = screen.getByTestId('filepicker');
    expect(filePicker).toBeInTheDocument();
    expect(filePicker).toBeVisible();
  });

  test('With ska_low in store and click CLICK HERE should redirect to low general page', () => {
    setPdmSbDefinition(getDefaultSbDefinition(TelescopeType.SkaLow));
    render(<SbdEditorLanding />);

    const clickHereButton = screen.getByRole('button', {
      name: /click here/i
    });
    expect(clickHereButton).toBeInTheDocument();
    expect(clickHereButton).toBeVisible();

    act(() => {
      fireEvent.click(clickHereButton);
    });
    // expect(window.location.href).eq('http://localhost:3000/low/general');
  });

  test('With ska_mid in store and click CLICK HERE should redirect to mid general page', () => {
    setPdmSbDefinition(getDefaultSbDefinition(TelescopeType.SkaMid));
    render(<SbdEditorLanding />);

    const clickHereButton = screen.getByRole('button', {
      name: /click here/i
    });
    expect(clickHereButton).toBeInTheDocument();
    expect(clickHereButton).toBeVisible();

    act(() => {
      fireEvent.click(clickHereButton);
    });
    // expect(window.location.href).eq('http://localhost:3000/mid/general');
  });

  test('select ska_mid should redirect to mid general page', async () => {
    render(<SbdEditorLanding />);

    const telescopeInput = screen.getByRole('combobox');
    act(() => {
      fireEvent.mouseDown(telescopeInput);
    });
    const options = await screen.findAllByRole('option');
    expect(options).toHaveLength(2);
    expect(options[0]).toHaveAttribute('data-value', 'ska_mid');
    expect(options[1]).toHaveAttribute('data-value', 'ska_low');

    act(() => {
      fireEvent.click(options[1]);
    });
    // expect(window.location.href).eq('http://localhost:3000/mid/general');
  });

  test('select ska_low should redirect to low general page', async () => {
    render(<SbdEditorLanding />);

    const telescopeInput = screen.getByRole('combobox');
    act(() => {
      fireEvent.mouseDown(telescopeInput);
    });
    const options = await screen.findAllByRole('option');
    expect(options).toHaveLength(2);
    expect(options[0]).toHaveAttribute('data-value', 'ska_mid');
    expect(options[1]).toHaveAttribute('data-value', 'ska_low');
    act(() => {
      fireEvent.click(options[0]);
    });
    // expect(window.location.href).eq('http://localhost:3000/low/general');
  });
});
