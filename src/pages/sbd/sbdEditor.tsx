import { createContext } from 'react';
import { Box } from '@mui/material';

import useDeepCompareEffect from 'use-deep-compare-effect';
import { isEqual } from 'lodash';
import { useStore } from '../../store/store';
import { FormProvider, useForm, useWatch } from 'react-hook-form';
import { zodResolver } from '@hookform/resolvers/zod';
import {
  MidSbDefinitionSchema,
  LowSbDefinitionSchema,
  SbDefinitionType
} from '../../models/sbDefinition/sbDefinition';
import { sbDefinitionSelector } from '../../store/selectors';
import { SbdEditorRoutes } from '../../components/app/routes';
import { SbdEditorHeader } from '../../components/header/sbdEditorHeader';
import { TelescopeType } from '../../generated/models/telescope-type';

export const TelescopeContext = createContext<TelescopeType | undefined>(
  undefined
);

export const SbdEditor = () => {
  const sbDefinition = useStore(sbDefinitionSelector);
  const setSbDefinition = useStore((state) => state.updateSbDefinition);
  const telescopeIsMid =
    useStore((state) => state.sbDefinition.telescope) == TelescopeType.SkaMid;

  const formMethods = useForm<SbDefinitionType>({
    mode: 'all',
    resolver: zodResolver(
      telescopeIsMid ? MidSbDefinitionSchema : LowSbDefinitionSchema
    ),
    defaultValues: sbDefinition
  });

  const { handleSubmit, control } = formMethods;

  const formValues = useWatch<SbDefinitionType>({
    control
  });

  // useWatch returns a new object each time, so useEffect would be triggered every time
  // (not that it matters really, as we are watching the whole form so will always care about the change)
  // There is some discussion about other ways to do this here
  // - should revisit at some point https://github.com/react-hook-form/react-hook-form/issues/7068
  useDeepCompareEffect(() => {
    if (!isEqual(formValues, sbDefinition)) {
      handleSubmit(() => setSbDefinition(formValues))();
    }
  }, [formValues]);

  return (
    <Box>
      <TelescopeContext.Provider
        value={telescopeIsMid ? TelescopeType.SkaMid : TelescopeType.SkaLow}
      >
        <FormProvider {...formMethods}>
          <SbdEditorHeader />
          <SbdEditorRoutes />
        </FormProvider>
      </TelescopeContext.Provider>
    </Box>
  );
};
