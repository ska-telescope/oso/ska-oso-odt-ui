import React, { useContext } from 'react';
import { Card } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { OsoContentTitle } from '../../components/shared/contentTitle';
import { MidArrayComponent } from '../../components/sbd/array/midArrayConfig';
import { LowArrayComponent } from '../../components/sbd/array/lowArrayConfig';
import { TelescopeContext } from './sbdEditor';
import { TelescopeType } from '../../generated/models/telescope-type';

export const Array = (): JSX.Element => {
  const telescopeType = useContext(TelescopeContext);

  return (
    <Card data-testid="contentId" variant="outlined">
      <Grid
        container
        direction="row"
        display="flex"
        alignItems="start"
        marginLeft="1em"
        marginTop="1em"
      >
        <OsoContentTitle title="Array" />
      </Grid>
      {telescopeType === TelescopeType.SkaMid ? (
        <MidArrayComponent />
      ) : (
        <LowArrayComponent />
      )}
    </Card>
  );
};
