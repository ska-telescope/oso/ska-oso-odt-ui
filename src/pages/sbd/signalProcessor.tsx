import React, { useContext } from 'react';
import { Card } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { OsoContentTitle } from '../../components/shared/contentTitle';
import { OsoFieldSet } from '../../components/shared';
import { MidSignalProcessorDetails } from '../../components/sbd/signalProcessor/midSignalProcessorDetails';
import { LowSignalProcessorDetails } from '../../components/sbd/signalProcessor/lowSignalProcessorDetails';
import { SignalProcessorList } from '../../components/sbd/signalProcessor/signalProcessorList';
import { useFormContext } from 'react-hook-form';
import { TelescopeContext } from './sbdEditor';
import { TelescopeType } from '../../generated/models/telescope-type';

const DEFAULT_SELECTION = 0;
const ctrlName = 'signalProcessor';

export const SignalProcessor = (): JSX.Element => {
  const [selectedSignalProcessorIndex, setSelectedSignalProcessorIndex] =
    React.useState<number>(DEFAULT_SELECTION);

  const { getValues } = useFormContext();

  const telescopeType = useContext(TelescopeContext);

  // TODO sort the formatting and make the structure less complex/nested
  return (
    <Card variant="outlined">
      <Grid
        container
        direction="row"
        display="flex"
        alignItems="start"
        marginLeft="1em"
        marginTop="1em"
      >
        <OsoContentTitle title="Signal Processor" />
      </Grid>
      <Grid
        container
        rowSpacing={1}
        columnSpacing={3}
        direction="row"
        width="100%"
      >
        <Grid size={{ xs: 12, md: 3, lg: 2 }}>
          <SignalProcessorList
            selectedSignalProcessorIndex={selectedSignalProcessorIndex}
            setSelectedSignalProcessorIndex={setSelectedSignalProcessorIndex}
          ></SignalProcessorList>
        </Grid>
        <Grid size={{ xs: 11, sx: 12, md: 9, lg: 10 }}>
          <OsoFieldSet>
            {getValues(ctrlName)[selectedSignalProcessorIndex] &&
              (telescopeType === TelescopeType.SkaMid ? (
                <MidSignalProcessorDetails
                  selectedSignalProcessorIndex={selectedSignalProcessorIndex}
                />
              ) : (
                <LowSignalProcessorDetails
                  selectedSignalProcessorIndex={selectedSignalProcessorIndex}
                />
              ))}
          </OsoFieldSet>
        </Grid>
      </Grid>
    </Card>
  );
};
