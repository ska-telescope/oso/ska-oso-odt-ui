import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { Scans } from './scans';
import {
  TEST_FORM_MID_SIGNAL_PROCESSOR,
  TEST_FORM_SCANS,
  TEST_FORM_TARGETS,
  WithSbDefinitionFormContext
} from '../../components/testUtils';

const TEST_INITIAL_STATE = {
  ...TEST_FORM_SCANS,
  ...TEST_FORM_TARGETS,
  ...TEST_FORM_MID_SIGNAL_PROCESSOR
};

describe('Scans', () => {
  test('should return heading and content', () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_INITIAL_STATE}>
        <Scans />
      </WithSbDefinitionFormContext>
    );
    const heading = screen.getByRole('heading', { name: /scans/i });
    expect(heading).toHaveAttribute('data-testid', 'scan-title');
  });
});
