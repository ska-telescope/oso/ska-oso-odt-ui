import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { Array } from './array';
import {
  TEST_FORM_LOW_ARRAY,
  WithSbDefinitionFormContext
} from '../../components/testUtils';

describe('Array', () => {
  test('should return heading', () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_LOW_ARRAY}>
        <Array />
      </WithSbDefinitionFormContext>
    );

    // Assertion to have Array title string
    const heading = screen.getByRole('heading', { name: 'Array' });
    expect(heading).toHaveAttribute('data-testid', 'functionId');
  });
});
