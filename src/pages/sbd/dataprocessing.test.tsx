import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test } from 'vitest';
import { DataProcessing } from './dataprocessing';

describe('DataProcessing', () => {
  test('should return appropriate components', () => {
    render(<DataProcessing />);

    const heading = screen.getByRole('heading', {
      name: 'Data Processing'
    });
    expect(heading).toHaveAttribute('data-testid', 'functionId');
    expect(
      screen.getByText(
        'Controls for data processing will appear in a future version of the ODT.'
      )
    ).toBeInTheDocument();
  });
});
