import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import { describe, expect, test } from 'vitest';
import { Targets } from './targets';
import { getTargetsListItems } from '../../components/sbd/target/targetsList.test';
import React from 'react';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_TARGETS
} from 'src/components/testUtils';

describe('Targets component', () => {
  test('should display the details of the correct Target when the row is clicked', async () => {
    render(
      <WithSbDefinitionFormContext initialState={TEST_FORM_TARGETS}>
        <Targets />
      </WithSbDefinitionFormContext>
    );

    let nameFieldInDetails = screen.getByLabelText('Name');

    expect(nameFieldInDetails).toHaveValue(TEST_FORM_TARGETS.targets[0].name);

    const listItems = getTargetsListItems();

    await userEvent.click(listItems[1]);

    nameFieldInDetails = screen.getByLabelText('Name');

    expect(nameFieldInDetails).toHaveValue(TEST_FORM_TARGETS.targets[1].name);
  });
});
