import React from 'react';
import { render, screen } from '@testing-library/react';
import { describe, expect, test, vi } from 'vitest';
import { General } from './general';
import { RoutePaths } from '../../components/app/routes';
import { WithSbDefinitionFormContext } from 'src/components/testUtils';

describe('General', () => {
  // Mocking the pathname for window location
  const setupUrl = (uri: string) => {
    const location = new URL('https://www.test.com' + uri);
    location.assign = vi.fn();
    location.replace = vi.fn();
    location.reload = vi.fn();

    delete window.location;
    window.location = location;
  };

  test('with Mid path should return appropriate components', () => {
    const path = RoutePaths.SbdEditor.General;
    setupUrl(path);
    render(
      <WithSbDefinitionFormContext>
        <General />
      </WithSbDefinitionFormContext>
    );

    const heading = screen.getByRole('heading', {
      name: 'General',
      level: 5
    });
    expect(heading).toBeInTheDocument();
    expect(heading).toBeVisible();
    expect(heading).toHaveAttribute('data-testid', 'functionId');
  });
  test('with Low path should return appropriate components', () => {
    const path = RoutePaths.SbdEditor.General;
    setupUrl(path);

    render(
      <WithSbDefinitionFormContext>
        <General />
      </WithSbDefinitionFormContext>
    );

    const heading = screen.getByRole('heading', {
      name: 'General',
      level: 5
    });
    expect(heading).toBeInTheDocument();
    expect(heading).toBeVisible();
    expect(heading).toHaveAttribute('data-testid', 'functionId');
  });
});
