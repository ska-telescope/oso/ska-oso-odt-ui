import React from 'react';
import { act, fireEvent, render, screen } from '@testing-library/react';
import { SignalProcessor } from './signalProcessor';
import { getSignalProcessorListItems } from '../../components/sbd/signalProcessor/signalProcessorList.test';
import {
  WithSbDefinitionFormContext,
  TEST_FORM_MID_SIGNAL_PROCESSOR
} from 'src/components/testUtils';

describe('SignalProcessor component', () => {
  test('should display the details of the correct CSP configuration when the row is clicked', () => {
    render(
      <WithSbDefinitionFormContext
        initialState={TEST_FORM_MID_SIGNAL_PROCESSOR}
      >
        <SignalProcessor />
      </WithSbDefinitionFormContext>
    );

    let nameFieldInDetails = screen.getByLabelText('Name');

    expect(nameFieldInDetails.value).eq('Config 123');

    const listItems = getSignalProcessorListItems();

    act(() => {
      fireEvent.click(listItems[1]);
    });

    nameFieldInDetails = screen.getByLabelText('Name');

    expect(nameFieldInDetails.value).eq('Config 456');
  });
});
