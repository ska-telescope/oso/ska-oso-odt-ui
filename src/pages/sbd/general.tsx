import React from 'react';
import { Card } from '@mui/material';
import Grid from '@mui/material/Grid2';
import { GeneralDetails } from '../../components';
import { OsoContentTitle } from '../../components/shared/contentTitle';

export const General = (): JSX.Element => {
  return (
    <Card data-testid="contentId" variant="outlined">
      <Grid
        container
        direction="row"
        display="flex"
        alignItems="start"
        marginLeft="1em"
        marginTop="1em"
      >
        <OsoContentTitle title="General" />
      </Grid>
      <GeneralDetails />
    </Card>
  );
};
