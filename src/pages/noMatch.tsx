import { Card, Typography } from '@mui/material';
import Grid from '@mui/material/Grid2';

const noMatch = () => (
  <Card data-testid="noRouting">
    <Grid>
      <Grid container direction="row" display="flex" alignItems="center">
        <Typography
          data-testid="missingRoutingMessage"
          variant="h4"
          color="red"
          m={20}
        >
          Something went wrong! Unable to locate {window.location.href} page.
        </Typography>
      </Grid>
    </Grid>
  </Card>
);

export default noMatch;
