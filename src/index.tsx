import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { createRoot } from 'react-dom/client';
import { StyledEngineProvider } from '@mui/material/styles';

import App from './components/app/App';

const container = document.getElementById('root');
const root = createRoot(container!);
const queryClient = new QueryClient();

root.render(
  <StyledEngineProvider injectFirst>
    <QueryClientProvider client={queryClient}>
      <App />
    </QueryClientProvider>
  </StyledEngineProvider>
);
