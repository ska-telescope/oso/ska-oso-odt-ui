/* tslint:disable */
/* eslint-disable */
/**
 * FastAPI
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import { AltAzCoordinates } from './alt-az-coordinates';
// May contain unused imports in some cases
// @ts-ignore
import { Dec } from './dec';
// May contain unused imports in some cases
// @ts-ignore
import { EquatorialCoordinates } from './equatorial-coordinates';
// May contain unused imports in some cases
// @ts-ignore
import { GalacticCoordinates } from './galactic-coordinates';
// May contain unused imports in some cases
// @ts-ignore
import { HorizontalCoordinates } from './horizontal-coordinates';
// May contain unused imports in some cases
// @ts-ignore
import { HorizontalCoordinatesReferenceFrame } from './horizontal-coordinates-reference-frame';
// May contain unused imports in some cases
// @ts-ignore
import { IcrsCoordinates } from './icrs-coordinates';
import { Ra } from './ra';
// May contain unused imports in some cases
// @ts-ignore
import { SolarSystemObject } from './solar-system-object';
// May contain unused imports in some cases
// @ts-ignore
import { SolarSystemObjectName } from './solar-system-object-name';
// May contain unused imports in some cases
// @ts-ignore
import { SpecialCoordinates } from './special-coordinates';

/**
 * @type ReferenceCoordinate
 * @export
 */
export type ReferenceCoordinate = AltAzCoordinates | EquatorialCoordinates | GalacticCoordinates | HorizontalCoordinates | IcrsCoordinates | SolarSystemObject | SpecialCoordinates;


