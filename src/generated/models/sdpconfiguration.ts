/* tslint:disable */
/* eslint-disable */
/**
 * FastAPI
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 0.1.0
 * 
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


// May contain unused imports in some cases
// @ts-ignore
import { ExecutionBlock } from './execution-block';
// May contain unused imports in some cases
// @ts-ignore
import { ProcessingBlock } from './processing-block';
// May contain unused imports in some cases
// @ts-ignore
import { Resources } from './resources';

/**
 * SDPConfiguration captures the SDP resources and pipeline configuration required to process an execution block.  :param execution_block: the SDP ExecutionBlock object :param resources: external resources :param processing_blocks: list of SDP ProcessingBlock objects
 * @export
 * @interface SDPConfiguration
 */
export interface SDPConfiguration {
    /**
     * 
     * @type {ExecutionBlock}
     * @memberof SDPConfiguration
     */
    'execution_block'?: ExecutionBlock;
    /**
     * 
     * @type {Resources}
     * @memberof SDPConfiguration
     */
    'resources'?: Resources;
    /**
     * 
     * @type {Array<ProcessingBlock>}
     * @memberof SDPConfiguration
     */
    'processing_blocks'?: Array<ProcessingBlock>;
}

