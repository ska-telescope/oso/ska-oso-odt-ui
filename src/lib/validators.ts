/**
 * This module contains pure functions that perform individual pieces of validation.
 * A guiding principle is that they could be used in any astronomy front end project, so
 * should use import things like our Zod models. They are expected to be used by the Zod
 * models in the 'refinements'
 *
 * They should all return a ValidationResult.
 * */
import { MidBand } from 'src/models/sbDefinition/signalProcessor/signalProcessor';
import { MID_MAX_BANDWIDTH_MHZ, LOW_MAX_BANDWIDTH_MHZ } from './osd';
import { getOsd } from './osd';

export interface ValidationResult {
  valid: boolean;
  message?: string;
}

const getMidFrequencyBandDetails = (band: MidBand) =>
  getOsd().ska_mid.frequency_band.find(
    (bandDetails) => bandDetails.rx_id.includes(band) // OSD returns bands as strings in the form 'Band_1'
  );

export const validateWindowIsInMidBand = (
  centreFrequencyMhz: number,
  bandwidthMhz: number,
  band: MidBand
): ValidationResult => {
  const limits = getMidFrequencyBandDetails(band);
  if (limits == undefined) {
    return { valid: false, message: `Band ${band} not supported for Mid.` };
  }
  const windowOutsideLowerLimit =
    centreFrequencyMhz - 0.5 * bandwidthMhz < limits.min_frequency_hz * 1e-6;
  const windowOutsideUpperLimit =
    centreFrequencyMhz + 0.5 * bandwidthMhz > limits.max_frequency_hz * 1e-6;

  if (windowOutsideLowerLimit || windowOutsideUpperLimit) {
    return { valid: false, message: `Window is outside of Band ${band}.` };
  }
  return { valid: true };
};

export const validateWindowIsInLowBand = (
  centreFrequencyMhz: number,
  bandwidthMhz: number
): ValidationResult => {
  const windowOutsideLowerLimit =
    centreFrequencyMhz - 0.5 * bandwidthMhz <
    getOsd().ska_low.frequency_band.min_frequency_hz * 1e-6;
  const windowOutsideUpperLimit =
    centreFrequencyMhz + 0.5 * bandwidthMhz >
    getOsd().ska_low.frequency_band.max_frequency_hz * 1e-6;

  if (windowOutsideLowerLimit || windowOutsideUpperLimit) {
    return { valid: false, message: `Window is outside of allowed range.` };
  }
  return { valid: true };
};

export const validateCentreFrequencyIsInMidBand = (
  centreFrequencyMhz: number,
  band: MidBand
): ValidationResult => {
  const limits = getMidFrequencyBandDetails(band);
  if (limits == undefined) {
    return { valid: false, message: `Band ${band} not supported for Mid.` };
  }

  if (
    centreFrequencyMhz < limits.min_frequency_hz * 1e-6 ||
    centreFrequencyMhz > limits.max_frequency_hz * 1e-6
  ) {
    return {
      valid: false,
      message: `Centre frequency is outside of Band ${band}.`
    };
  }
  return { valid: true };
};

export const validateCentreFrequencyIsInLowBand = (
  centreFrequencyMhz: number
): ValidationResult => {
  if (
    centreFrequencyMhz <
      getOsd().ska_low.frequency_band.min_frequency_hz * 1e-6 ||
    centreFrequencyMhz > getOsd().ska_low.frequency_band.max_frequency_hz * 1e-6
  ) {
    return {
      valid: false,
      message: `Centre frequency is outside of allowed range.`
    };
  }
  return { valid: true };
};

export const validateBandwidthForMidBand = (
  bandwidthMhz: number,
  band: MidBand
): ValidationResult => {
  const limits = getMidFrequencyBandDetails(band);
  if (limits == undefined) {
    return { valid: false, message: `Band ${band} not supported for Mid.` };
  }
  if (
    bandwidthMhz >
    limits.max_frequency_hz * 1e-6 - limits.min_frequency_hz * 1e-6
  ) {
    // todo use custom object rahter than generated one?
    return { valid: false, message: `Bandwidth is wider than the Band.` };
  }
  if (bandwidthMhz > MID_MAX_BANDWIDTH_MHZ) {
    return {
      valid: false,
      message: `Bandwidth is wider than ${MID_MAX_BANDWIDTH_MHZ} MHz.`
    };
  }
  return { valid: true };
};

export const validateBandwidthForLowBand = (
  bandwidthMhz: number,
  totalBandwidth = false
): ValidationResult => {
  const messageStart = totalBandwidth ? 'Total bandwidth' : 'Bandwidth';
  if (
    bandwidthMhz >
    getOsd().ska_low.frequency_band.max_frequency_hz * 1e-6 -
      getOsd().ska_low.frequency_band.min_frequency_hz * 1e-6
  ) {
    return { valid: false, message: `${messageStart} is wider than the Band.` };
  }
  if (bandwidthMhz > LOW_MAX_BANDWIDTH_MHZ) {
    return {
      valid: false,
      message: `${messageStart} is wider than ${LOW_MAX_BANDWIDTH_MHZ} MHz.`
    };
  }
  return { valid: true };
};

export const validateValueIsUnique = (
  valueToValidate: string | number,
  allValues: (string | number)[]
) => {
  const matchingValues = allValues.filter((value) => valueToValidate === value);

  if (matchingValues.length > 1) {
    return {
      valid: false,
      message: `Field must be unique.`
    };
  }
  return { valid: true };
};

interface ObjectWithName {
  name: string;
}

export const validateNameIsUnique = (
  objectToValidate: ObjectWithName,
  allValues: ObjectWithName[]
): ValidationResult => {
  const objectsWithSameName = allValues.filter(
    (value) => objectToValidate.name === value.name
  );

  if (objectsWithSameName.length > 1) {
    return {
      valid: false,
      message: `Name must be unique.`
    };
  }
  return { valid: true };
};
