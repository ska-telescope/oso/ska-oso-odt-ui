import { generate, mapping, string } from './helpers';
import { describe, expect } from 'vitest';

describe('helpers', () => {
  describe('mapping', () => {
    describe('arrayToString', () => {
      test('with null should return null', () => {
        expect(mapping.arrayToString(null)).toBeNull();
      });
      test('with empty array should return empty string', () => {
        expect(mapping.arrayToString([])).toBe('');
      });
      test('with string array should return a joined string', () => {
        expect(mapping.arrayToString(['1', '2', '3'])).toBe('1,2,3');
      });
    });

    describe('stringToArray', () => {
      test('with null string should return null', () => {
        expect(mapping.stringToArray(null)).toBeNull();
      });
      test('with empty string should return an array with an empty string', () => {
        expect(mapping.stringToArray('')).toStrictEqual(['']);
      });
      test('with comma joined string should return  string array', () => {
        expect(mapping.stringToArray('1,2,3')).toStrictEqual(['1', '2', '3']);
      });
    });
  });

  describe('generate', () => {
    describe('numberArrayStartWith', () => {
      test('with invalid number should return emptry array', () => {
        expect(generate.numberArrayStartWith(-1, 0)).toStrictEqual([]);
        expect(generate.numberArrayStartWith(0, 1)).toStrictEqual([]);
      });
      test('with valid number and started index should return a number array started with index', () => {
        expect(generate.numberArrayStartWith(4, 5)).toStrictEqual([5, 6, 7, 8]);
      });
    });
    describe('numberArrayStartWith0', () => {
      test('with invalid number should return emptry array', () => {
        expect(generate.numberArrayStartWith0(-1)).toStrictEqual([]);
        expect(generate.numberArrayStartWith0(0)).toStrictEqual([]);
      });
      test('with valid number should return a number array started with zero', () => {
        expect(generate.numberArrayStartWith0(4)).toStrictEqual([0, 1, 2, 3]);
      });
    });
    describe('numberArrayStartWith1', () => {
      test('with invalid number should return emptry array', () => {
        expect(generate.numberArrayStartWith1(-1)).toStrictEqual([]);
        expect(generate.numberArrayStartWith1(0)).toStrictEqual([]);
      });
      test('with valid number should return a number array started with one', () => {
        expect(generate.numberArrayStartWith1(4)).toStrictEqual([1, 2, 3, 4]);
      });
    });
  });

  describe('string', () => {
    describe('removeBlankOrNull', () => {
      test('with null should return null', () => {
        expect(string.removeBlankOrNull(null)).toBeNull();
      });
      test('with an array should return an array without null and empty string', () => {
        expect(string.removeBlankOrNull([])).toStrictEqual([]);
        expect(string.removeBlankOrNull(['1', '', '2', '', '3'])).toStrictEqual(
          ['1', '2', '3']
        );
      });
    });
    describe('capitalizeFirstLetter', () => {
      test('with null or undefined parameter should return emoty string', () => {
        expect(string.capitalizeFirstLetter(null)).toBe('');
        expect(string.capitalizeFirstLetter(undefined)).toBe('');
      });
      test('with alphabet string should return string with 1st capital character', () => {
        expect(string.capitalizeFirstLetter('abc')).toBe('Abc');
        expect(string.capitalizeFirstLetter('one two three')).toBe(
          'One two three'
        );
      });
      test('with number string should return same number', () => {
        expect(string.capitalizeFirstLetter('123')).toBe('123');
      });
    });
  });
});
