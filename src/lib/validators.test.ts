import { describe, expect, test } from 'vitest';
import {
  validateWindowIsInMidBand,
  validateWindowIsInLowBand,
  validateCentreFrequencyIsInMidBand,
  validateCentreFrequencyIsInLowBand,
  validateBandwidthForMidBand,
  validateBandwidthForLowBand
} from './validators';
import { MidBand } from 'src/models/sbDefinition/signalProcessor/spectralWindow';

describe('validateWindowIsInBand-', () => {
  describe('-ForMid', () => {
    test('it should return false if the band is not supported', () => {
      expect(validateWindowIsInMidBand(10, 10, '42' as MidBand)).toStrictEqual({
        valid: false,
        message: 'Band 42 not supported for Mid.'
      });
    });

    test.each([
      [350, 10, MidBand.BAND_1],
      [1050, 10, MidBand.BAND_1],
      [400, 200, MidBand.BAND_1],
      [1000, 200, MidBand.BAND_1],
      [950, 10, MidBand.BAND_2],
      [1760, 10, MidBand.BAND_2],
      [1200, 1000, MidBand.BAND_2],
      [600, 10, MidBand.BAND_2]
    ])(
      'should return false if the window is outside the band',
      (centreFrequencyMHz: number, bandwidthHz: number, band: MidBand) => {
        expect(
          validateWindowIsInMidBand(centreFrequencyMHz, bandwidthHz, band)
        ).toStrictEqual({
          valid: false,
          message: `Window is outside of Band ${band}.`
        });
      }
    );

    test('should return true if the window is inside the band', () => {
      expect(validateWindowIsInMidBand(700, 240, MidBand.BAND_1)).toStrictEqual(
        {
          valid: true
        }
      );
    });
  });

  describe('-ForLow', () => {
    test.each([
      [50, 10],
      [350, 10],
      [100, 105],
      [300, 200]
    ])(
      'should return false if the window is outside the band',
      (centreFrequencyHz: number, bandwidthHz: number) => {
        expect(
          validateWindowIsInLowBand(centreFrequencyHz, bandwidthHz)
        ).toStrictEqual({
          valid: false,
          message: `Window is outside of allowed range.`
        });
      }
    );

    test('should return true if the window is inside the band', () => {
      expect(validateWindowIsInLowBand(300, 100)).toStrictEqual({
        valid: true
      });
    });
  });
});

describe('validateCentreFrequencyIsInBand-', () => {
  describe('-ForMid', () => {
    test('it should return false if the band is not supported', () => {
      expect(
        validateCentreFrequencyIsInMidBand(10, '42' as MidBand)
      ).toStrictEqual({
        valid: false,
        message: 'Band 42 not supported for Mid.'
      });
    });

    test.each([
      [349, MidBand.BAND_1],
      [1060, MidBand.BAND_1],
      [949, MidBand.BAND_2],
      [2000, MidBand.BAND_2]
    ])(
      'should return false if the centre frequency is outside the band',
      (centreFrequencyHz: number, band: string) => {
        expect(
          validateCentreFrequencyIsInMidBand(centreFrequencyHz, band as MidBand)
        ).toStrictEqual({
          valid: false,
          message: `Centre frequency is outside of Band ${band}.`
        });
      }
    );

    test('should return true if the centre frequency is inside the band', () => {
      expect(
        validateCentreFrequencyIsInMidBand(700, MidBand.BAND_1)
      ).toStrictEqual({
        valid: true
      });
    });
  });

  describe('-ForLow', () => {
    test.each([[47], [400]])(
      'should return false if the centre frequency is outside the band',
      (centreFrequencyHz: number) => {
        expect(
          validateCentreFrequencyIsInLowBand(centreFrequencyHz)
        ).toStrictEqual({
          valid: false,
          message: `Centre frequency is outside of allowed range.`
        });
      }
    );

    test('should return true if the centre frequency is inside the band', () => {
      expect(validateWindowIsInLowBand(300, 100)).toStrictEqual({
        valid: true
      });
    });
  });
});

describe('validateBandwidth-', () => {
  describe('-ForMid', () => {
    test.each([
      [349, '42', 'Band 42 not supported for Mid.'],
      [750, MidBand.BAND_1, 'Bandwidth is wider than the Band.'],
      [2000, MidBand.BAND_2, 'Bandwidth is wider than the Band.'],
      [900, MidBand.BAND_1, 'Bandwidth is wider than the Band.']
    ])(
      'should return false and the correct error message',
      (centreFrequencyHz: number, band: string, errorMessage: string) => {
        expect(
          validateBandwidthForMidBand(centreFrequencyHz, band as MidBand)
        ).toStrictEqual({
          valid: false,
          message: errorMessage
        });
      }
    );

    test('should return true if the bandwidth is valid', () => {
      expect(validateBandwidthForMidBand(100, MidBand.BAND_1)).toStrictEqual({
        valid: true
      });
    });
  });
  describe('-ForLow', () => {
    test.each([
      [750, 'Bandwidth is wider than the Band.'],
      [100, 'Bandwidth is wider than 75 MHz.']
    ])(
      'should return false and the correct error message',
      (centreFrequencyHz: number, errorMessage: string) => {
        expect(validateBandwidthForLowBand(centreFrequencyHz)).toStrictEqual({
          valid: false,
          message: errorMessage
        });
      }
    );
  });

  test('should return true if the bandwidth is valid', () => {
    expect(validateBandwidthForLowBand(60)).toStrictEqual({
      valid: true
    });
  });
});
