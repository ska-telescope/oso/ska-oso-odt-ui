export const mapping = {
  arrayToString: (value: string[] | null): string | null =>
    Array.isArray(value) ? value.join(',') : null,
  stringToArray: (value: string | null): string[] | null =>
    value !== null ? value.split(',') : null
};

export const generate = {
  numberArrayStartWith: (max: number, startedIndex: number): number[] =>
    max > 0 ? [...Array(max).keys()].map((i) => i + startedIndex) : [],
  numberArrayStartWith0: (max: number): number[] =>
    generate.numberArrayStartWith(max, 0),
  numberArrayStartWith1: (max: number): number[] =>
    generate.numberArrayStartWith(max, 1)
};

export const string = {
  removeBlankOrNull: (array: string[] | null): string[] | null =>
    array ? array.filter((item) => Boolean(item)) : null,

  capitalizeFirstLetter: (str: string | null | undefined) =>
    str ? str.charAt(0).toUpperCase() + str.slice(1) : ''
};
