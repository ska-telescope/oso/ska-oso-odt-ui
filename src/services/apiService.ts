import { AxiosError } from 'axios';
import {
  CommonOSOAPIEndpointsApiFactory,
  Configuration,
  ODTAPIApiFactory
} from '../generated';
import { SBDefinitionInput } from '../generated/models/sbdefinition-input';
import { ValidationResponse } from '../generated/models/validation-response';
import { ProjectInput } from '../generated/models/project-input';
import { Configuration as OsdConfiguration } from '../generated/models/configuration';
import { PrjSBDLinkResponse } from '../generated/models/prj-sbdlink-response';
import { ResponseGetSystemcoordinatesIntegrationSkaOsoOdtUiOsoApiV0CoordinatesIdentifierReferenceFrameGet } from '../generated/models/response-get-systemcoordinates-integration-ska-oso-odt-ui-oso-api-v0-coordinates-identifier-reference-frame-get';
import { ReferenceFrame } from '../generated/models/reference-frame';

export type ApiResult<T> = {
  data?: T;
  error?: string;
};

export function handleApiError<T>(error: AxiosError): ApiResult<T> {
  if (error.code === 'ERR_NETWORK') {
    // This error means the server isn't reachable and hasn't returned a response.
    return {
      error: `Server not reachable. Ensure the OSO Services instance at ${apiService.baseURL()} is running.`
    };
  }
  if (error.status === 404 && error?.response?.data?.detail === 'Not Found') {
    // This 404 means the server can be reached but the path is not available on it. It is distinct
    // from a 404 that the server returns when the requested resource cannot be found in the ODA.
    return {
      error: `Not found. Check the URL path in\n ${error.request.responseURL}\n is correct.`
    };
  }
  // If it reaches here the response data should be one of our ErrorResponse objects.
  // We fall back on the axios error.message just in case it isn't.
  return { error: error.response?.data?.detail ?? error.message };
}

const apiService = {
  baseURL: () => window.env.BACKEND_URL,

  loadOsd: async (): Promise<ApiResult<OsdConfiguration>> => {
    const basePath = apiService.baseURL();
    try {
      const result = await ODTAPIApiFactory({
        basePath
      }).configurationGetSkaOsoServicesOsoApiV0OdtConfigurationGet();
      return { data: result.data };
    } catch (err) {
      return handleApiError(err as AxiosError);
    }
  },

  loadSBFn: async (sbdId: string): Promise<ApiResult<SBDefinitionInput>> => {
    const baseUrl = apiService.baseURL();
    try {
      const result = await ODTAPIApiFactory({
        basePath: baseUrl
      } as Configuration).sbdsGetSkaOsoServicesOsoApiV0OdtSbdsIdentifierGet(
        sbdId
      );

      return {
        data: result.data
      };
    } catch (err) {
      return handleApiError(err as AxiosError);
    }
  },

  loadProject: async (prjId: string): Promise<ApiResult<ProjectInput>> => {
    const baseUrl = apiService.baseURL();
    try {
      const result = await ODTAPIApiFactory({
        basePath: baseUrl
      } as Configuration).prjsGetSkaOsoServicesOsoApiV0OdtPrjsIdentifierGet(
        prjId
      );

      return {
        data: result.data
      };
    } catch (err) {
      return handleApiError(err as AxiosError);
    }
  },

  saveSBFn: async (
    schedulingBlockData: SBDefinitionInput
  ): Promise<ApiResult<SBDefinitionInput>> => {
    const baseUrl = apiService.baseURL();
    try {
      const result = await ODTAPIApiFactory({
        basePath: baseUrl
      } as Configuration).sbdsPostSkaOsoServicesOsoApiV0OdtSbdsPost(
        schedulingBlockData
      ); // TODO for now this seems to work as the types look the same, however we need to reconcile the generated SBDefinition and the SchedulingBlockType

      return {
        data: result.data
      };
    } catch (err) {
      return handleApiError(err as AxiosError);
    }
  },

  updateSBFn: async (
    schedulingBlockData: SBDefinitionInput
  ): Promise<ApiResult<SBDefinitionInput>> => {
    const baseUrl = apiService.baseURL();

    try {
      const result = await ODTAPIApiFactory({
        basePath: baseUrl
      } as Configuration).sbdsPutSkaOsoServicesOsoApiV0OdtSbdsIdentifierPut(
        schedulingBlockData.sbd_id!,
        schedulingBlockData
      );

      return {
        data: result.data
      };
    } catch (err) {
      return handleApiError(err as AxiosError);
    }
  },

  generateSBFn: async (): Promise<ApiResult<SBDefinitionInput>> => {
    const baseUrl = apiService.baseURL();
    try {
      const result = await ODTAPIApiFactory({
        basePath: baseUrl
      } as Configuration).sbdsCreateSkaOsoServicesOsoApiV0OdtSbdsCreateGet();

      return {
        data: result.data
      };
    } catch (err) {
      return handleApiError(err as AxiosError);
    }
  },

  validateSBFn: async (
    schedulingBlockData: JSON
  ): Promise<ApiResult<ValidationResponse>> => {
    const baseUrl = apiService.baseURL();
    try {
      const result = await ODTAPIApiFactory({
        basePath: baseUrl
      } as Configuration).sbdsValidateSkaOsoServicesOsoApiV0OdtSbdsValidatePost(
        schedulingBlockData,
        { headers: { 'Content-Type': 'application/json' } }
      );
      return {
        data: result.data
      };
    } catch (err) {
      return handleApiError(err as AxiosError);
    }
  },

  saveProjectFn: async (
    project: ProjectInput
  ): Promise<ApiResult<ProjectInput>> => {
    const baseUrl = apiService.baseURL();
    try {
      const result = await ODTAPIApiFactory({
        basePath: baseUrl
      } as Configuration).prjsPostSkaOsoServicesOsoApiV0OdtPrjsPost(project);

      return {
        data: result.data
      };
    } catch (err) {
      return handleApiError(err as AxiosError);
    }
  },

  updateProjectFn: async (
    project: ProjectInput
  ): Promise<ApiResult<ProjectInput>> => {
    const baseUrl = apiService.baseURL();
    try {
      const result = await ODTAPIApiFactory({
        basePath: baseUrl
      } as Configuration).prjsPutSkaOsoServicesOsoApiV0OdtPrjsIdentifierPut(
        project.prj_id!,
        project
      );

      return {
        data: result.data
      };
    } catch (err) {
      return handleApiError(err as AxiosError);
    }
  },

  createSbDefinitionInProjectFn: async (
    prjId: string,
    obsBlockId: string,
    sbDefinitionInput: SBDefinitionInput
  ): Promise<ApiResult<PrjSBDLinkResponse | null>> => {
    const baseUrl = apiService.baseURL();
    try {
      const result = await ODTAPIApiFactory({
        basePath: baseUrl
      } as Configuration).prjsSbdsPostSkaOsoServicesOsoApiV0OdtPrjsIdentifierObsBlockIdSbdsPost(
        prjId,
        obsBlockId,
        sbDefinitionInput
      );

      return {
        data: result.data
      };
    } catch (err) {
      return handleApiError(err as AxiosError);
    }
  },

  getSystemCoordinates: async (
    targetName: string,
    referenceFrame: ReferenceFrame = ReferenceFrame.Equatorial
  ): Promise<
    ApiResult<ResponseGetSystemcoordinatesIntegrationSkaOsoOdtUiOsoApiV0CoordinatesIdentifierReferenceFrameGet | null>
  > => {
    try {
      const result = await CommonOSOAPIEndpointsApiFactory({
        basePath: apiService.baseURL()
      } as Configuration).getSystemcoordinatesIntegrationSkaOsoOdtUiOsoApiV0CoordinatesIdentifierReferenceFrameGet(
        targetName,
        referenceFrame
      );
      return {
        data: result.data
      } as ApiResult<ResponseGetSystemcoordinatesIntegrationSkaOsoOdtUiOsoApiV0CoordinatesIdentifierReferenceFrameGet>;
    } catch (err) {
      return handleApiError(err as AxiosError);
    }
  }
};

export default apiService;
