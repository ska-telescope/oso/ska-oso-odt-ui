import { util, z } from 'zod';
import isInteger = util.isInteger;
import { zodRequired } from '../../validation/common';
import {
  LOW_CHANNEL_BLOCK_MHZ,
  LOW_STATION_CHANNEL_WIDTH_MHZ,
  MID_CHANNEL_WIDTH_KHZ
} from '../../../lib/osd';

export const LOW_SPECTRAL_WINDOW_CENTRE_FREQUENCY_MIN_MHZ = 51.171875;
export const LOW_SPECTRAL_WINDOW_CENTRE_FREQUENCY_MAX_MHZ = 350;
export const LOW_SPECTRAL_WINDOW_BANDWIDTH_MIN_MHZ = LOW_CHANNEL_BLOCK_MHZ;
export const LOW_SPECTRAL_WINDOW_BANDWIDTH_MAX_MHZ = 300;

export const SpectralWindowMode = z.enum(['CORR', 'PST']);

export enum MidBand {
  BAND_1 = '1',
  BAND_2 = '2'
}

interface BandDefaults {
  centreMhz: number;
  bandwidthMhz: number;
  fsOffsetMhz?: number;
}

export const MidBandsDefaults = new Map<MidBand, BandDefaults>([
  [
    MidBand.BAND_1,
    {
      centreMhz: 700,
      bandwidthMhz: 52080 * MID_CHANNEL_WIDTH_KHZ * 1e-3, // bandwidth = channels * channel width
      fsOffsetMhz: 0
    }
  ],
  [
    MidBand.BAND_2,
    {
      centreMhz: 1425,
      bandwidthMhz: 49840 * MID_CHANNEL_WIDTH_KHZ * 1e-3,
      fsOffsetMhz: 0
    }
  ]
]);

export const LowBandDefaults: BandDefaults = {
  centreMhz: 199.609375,
  bandwidthMhz: 75
};

const CommonSpectralWindowSchema = z.object({
  spectralWindowId: z.number().default(Math.random),
  mode: SpectralWindowMode.default('CORR'),
  resolution: z.string().trim().optional()
});

export const MidSpectralWindowSchema = CommonSpectralWindowSchema.merge(
  z.object({
    bandwidthMhz: zodRequired(
      z.coerce
        .number()
        .gt(0)
        .refine(
          (value) =>
            // 20 channels is only a restriction until AA2. After than it can be removed and
            // error message changed to 'Must be an integer number of channels'
            isInteger((value * 1e6) / (20 * MID_CHANNEL_WIDTH_KHZ * 1e3)),
          {
            message: 'Must be a multiple of 20 channels.'
          }
        )
    ),
    centreFrequencyMhz: zodRequired(z.coerce.number().gt(0)),
    averageTimeFactor: z.coerce.number().default(10)
  })
);

export const LowSpectralWindowSchema = CommonSpectralWindowSchema.merge(
  z.object({
    bandwidthMhz: zodRequired(
      z.coerce
        .number()
        .min(LOW_SPECTRAL_WINDOW_BANDWIDTH_MIN_MHZ)
        .max(LOW_SPECTRAL_WINDOW_BANDWIDTH_MAX_MHZ)
        .refine((value) => isInteger(value / LOW_CHANNEL_BLOCK_MHZ), {
          message: `Must be a multiple of ${LOW_CHANNEL_BLOCK_MHZ} MHz`
        })
    ).default(LowBandDefaults.bandwidthMhz),
    centreFrequencyMhz: zodRequired(
      z.coerce
        .number()
        .min(LOW_SPECTRAL_WINDOW_CENTRE_FREQUENCY_MIN_MHZ)
        .max(LOW_SPECTRAL_WINDOW_CENTRE_FREQUENCY_MAX_MHZ)
        .refine(
          (value) =>
            isInteger(
              (value + 0.5 * LOW_STATION_CHANNEL_WIDTH_MHZ) /
                LOW_STATION_CHANNEL_WIDTH_MHZ
            ),
          {
            message: 'The first channel of the SPW must be even'
          }
        )
    ).default(LowBandDefaults.centreMhz),
    averageTimeS: z.coerce.number().default(0.849)
  })
);

export type MidSpectralWindowSchemaType = z.infer<
  typeof MidSpectralWindowSchema
>;
export type LowSpectralWindowSchemaType = z.infer<
  typeof LowSpectralWindowSchema
>;
