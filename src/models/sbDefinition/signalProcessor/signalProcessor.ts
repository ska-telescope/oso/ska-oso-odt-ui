import { z } from 'zod';

import {
  MidSpectralWindowSchema,
  LowSpectralWindowSchema,
  MidBandsDefaults,
  MidBand
} from './spectralWindow';
import { zodRequiredNumber, zodRequiredString } from '../../validation/common';
import {
  lowSignalProcessorRefinement,
  midSignalProcessorRefinement
} from './refinement';

const CommonSignalProcessorSchema = z.object({
  id: zodRequiredString,
  name: zodRequiredString
});

const midDefaultBand = MidBand.BAND_1;

export const MidSignalProcessorSchema = CommonSignalProcessorSchema.merge(
  z.object({
    band: z.nativeEnum(MidBand).default(midDefaultBand),
    fsOffsetMhz: zodRequiredNumber.default(
      MidBandsDefaults.get(midDefaultBand)!.fsOffsetMhz!
    ),
    spectralWindows: z.array(MidSpectralWindowSchema).default([])
  })
).superRefine(midSignalProcessorRefinement);

export const LowSignalProcessorSchema = CommonSignalProcessorSchema.merge(
  z.object({
    doPst: z.boolean().default(false),
    spectralWindows: z.array(LowSpectralWindowSchema).default([])
  })
).superRefine(lowSignalProcessorRefinement);

export type LowSignalProcessorType = z.infer<typeof LowSignalProcessorSchema>;

export type MidSignalProcessorType = z.infer<typeof MidSignalProcessorSchema>;
