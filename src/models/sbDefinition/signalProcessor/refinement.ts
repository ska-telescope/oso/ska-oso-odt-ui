import { RefinementCtx } from 'zod';
import {
  validateBandwidthForLowBand,
  validateBandwidthForMidBand,
  validateCentreFrequencyIsInLowBand,
  validateCentreFrequencyIsInMidBand,
  validateWindowIsInLowBand,
  validateWindowIsInMidBand
} from '../../../lib/validators';
import { addErrorToCtx } from '../../validation/common';
import {
  LowSignalProcessorType,
  MidSignalProcessorType
} from './signalProcessor';
import { SpectralWindowMode } from './spectralWindow';

export const midSignalProcessorRefinement = (
  value: MidSignalProcessorType,
  ctx: RefinementCtx
) => {
  validateBandAndBandwidthAndCentreFrequencyForMid(value, ctx);
};

export const lowSignalProcessorRefinement = (
  value: LowSignalProcessorType,
  ctx: RefinementCtx
) => {
  validateBandwidthAndCentreFrequencyForLow(value, ctx);
};

const validateBandAndBandwidthAndCentreFrequencyForMid = (
  value: MidSignalProcessorType,
  ctx: RefinementCtx
) => {
  value.spectralWindows.forEach((spectralWindow, index) => {
    const bandwidthValidationResult = validateBandwidthForMidBand(
      spectralWindow.bandwidthMhz,
      value.band
    );
    addErrorToCtx(ctx, bandwidthValidationResult, [
      'spectralWindows',
      index,
      'bandwidthMhz'
    ]);

    if (!spectralWindow.centreFrequencyMhz) {
      // If user hasn't yet input a value, do not validate
      return;
    }

    const centreFrequencyValidationResult = validateCentreFrequencyIsInMidBand(
      spectralWindow.centreFrequencyMhz,
      value.band
    );
    addErrorToCtx(ctx, centreFrequencyValidationResult, [
      'spectralWindows',
      index,
      'centreFrequencyMhz'
    ]);

    if (
      bandwidthValidationResult.valid &&
      centreFrequencyValidationResult.valid
    ) {
      // Only validate the combination if the two are independently valid
      const windowValidationResult = validateWindowIsInMidBand(
        spectralWindow.centreFrequencyMhz,
        spectralWindow.bandwidthMhz,
        value.band
      );
      addErrorToCtx(ctx, windowValidationResult, [
        'spectralWindows',
        index,
        'bandwidthMhz'
      ]);
      addErrorToCtx(ctx, windowValidationResult, [
        'spectralWindows',
        index,
        'centreFrequencyMhz'
      ]);
    }
  });
};

const validateBandwidthAndCentreFrequencyForLow = (
  value: LowSignalProcessorType,
  ctx: RefinementCtx
) => {
  const totalBandwidth = value.spectralWindows
    .filter(
      (spectralWindow) => spectralWindow.mode === SpectralWindowMode.enum.CORR
    )
    .reduce(
      (accumulator, spectralWindow) =>
        accumulator + spectralWindow.bandwidthMhz,
      0
    );
  const totalBandwidthValidationResult = validateBandwidthForLowBand(
    totalBandwidth,
    true
  );

  // TODO the validation for the individual spectral window level should probably be moved into the spectralWindows model refinement,
  // rather than the parent
  value.spectralWindows.forEach((spectralWindow, index) => {
    const bandwidthValidationResult = validateBandwidthForLowBand(
      spectralWindow.bandwidthMhz
    );
    addErrorToCtx(ctx, bandwidthValidationResult, [
      'spectralWindows',
      index,
      'bandwidthMhz'
    ]);

    const centreFrequencyValidationResult = validateCentreFrequencyIsInLowBand(
      spectralWindow.centreFrequencyMhz
    );
    addErrorToCtx(ctx, centreFrequencyValidationResult, [
      'spectralWindows',
      index,
      'centreFrequencyMhz'
    ]);

    if (
      bandwidthValidationResult.valid &&
      centreFrequencyValidationResult.valid
    ) {
      // Only validate the combination if the two are independently valid
      const windowValidationResult = validateWindowIsInLowBand(
        spectralWindow.centreFrequencyMhz,
        spectralWindow.bandwidthMhz
      );
      addErrorToCtx(ctx, windowValidationResult, [
        'spectralWindows',
        index,
        'bandwidthMhz'
      ]);
      addErrorToCtx(ctx, windowValidationResult, [
        'spectralWindows',
        index,
        'centreFrequencyMhz'
      ]);
    }
    addErrorToCtx(ctx, totalBandwidthValidationResult, [
      'spectralWindows',
      index,
      'bandwidthMhz'
    ]);
  });
};
