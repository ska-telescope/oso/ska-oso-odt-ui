import { z } from 'zod';

export const ScanSequenceItemSchema = z.object({
  scanSequenceId: z.number().default(Math.random),
  scanDefinitionId: z.string().trim(),
  target: z.string().trim()
});

export const ScanSequenceSchema = z.array(ScanSequenceItemSchema);

export type ScanSequenceItemType = z.infer<typeof ScanSequenceItemSchema>;
export type ScanSequenceType = z.infer<typeof ScanSequenceSchema>;
