import { z } from 'zod';
import { zodRequiredNumber } from '../../validation/common';

export const ScanDefinitionSchema = z.object({
  scanDefinitionId: z.string().trim(),
  target: z.string().trim().optional(),
  csp: z.string().trim().optional(),
  durationS: zodRequiredNumber.pipe(z.number().gt(0))
});

export const ScanDefinitionsSchema = z.array(ScanDefinitionSchema);

export type ScanDefinitionType = z.infer<typeof ScanDefinitionSchema>;
export type ScanDefinitionsType = z.infer<typeof ScanDefinitionsSchema>;
