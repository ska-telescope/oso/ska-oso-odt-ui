import { z } from 'zod';
import { ScanDefinitionsSchema } from './scanDefinitions';
import { ScanSequenceSchema } from './scanSequence';

export const ScansSchema = z.object({
  scanDefinitions: ScanDefinitionsSchema,
  scanSequence: ScanSequenceSchema
});

export type ScansType = z.infer<typeof ScansSchema>;
