import { z } from 'zod';

import {
  LowSignalProcessorSchema,
  MidSignalProcessorSchema
} from './signalProcessor/signalProcessor';
import { sbDefinitionRefinement } from './refinement';
import { GeneralSchema } from './general/general';
import { ScriptSchema } from './script/script';

import { targetsRefinement } from './targets/refinement';
import { TargetSchema } from './targets/target';
import { ScansSchema } from './scans/scans';
import {
  LowArrayConfigSchema,
  MidArrayConfigSchema
} from './array/arrayConfig';

const CommonSbDefinitionSchema = z.object({
  general: GeneralSchema,
  script: ScriptSchema,
  targets: TargetSchema.array().default([]).superRefine(targetsRefinement),
  scans: ScansSchema
});

export const MidSbDefinitionSchema = CommonSbDefinitionSchema.merge(
  z.object({
    array: MidArrayConfigSchema,
    signalProcessor: MidSignalProcessorSchema.array().default([])
  })
).superRefine(sbDefinitionRefinement);

export const LowSbDefinitionSchema = CommonSbDefinitionSchema.merge(
  z.object({
    array: LowArrayConfigSchema,
    signalProcessor: LowSignalProcessorSchema.array().default([])
  })
).superRefine(sbDefinitionRefinement);

export type LowSbDefinitionType = z.infer<typeof LowSbDefinitionSchema>;

export type MidSbDefinitionType = z.infer<typeof MidSbDefinitionSchema>;

export type SbDefinitionType = LowSbDefinitionType | MidSbDefinitionType;
