import { validateValueIsUnique } from '../../../lib/validators';
import { RefinementCtx } from 'zod';
import { addErrorToCtx } from '../../validation/common';

export const scriptArgRefinement = (
  args: { kw: string; value: string | number }[] | undefined,
  ctx: RefinementCtx
) => {
  const listOfKeywords = args?.map((argTuple) => argTuple.kw);
  listOfKeywords?.forEach((kw, index) => {
    addErrorToCtx(ctx, validateValueIsUnique(kw, listOfKeywords), [
      index,
      'kw'
    ]);
  });
};
