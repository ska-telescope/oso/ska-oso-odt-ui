import { z } from 'zod';
import { SelectOption } from '../components/shared/ui';
import { scriptArgRefinement } from './refinement';

export enum ScriptKind {
  git = 'git',
  filesystem = 'filesystem'
}

export const DEFAULT_GIT_BRANCH = 'master';

export const ScriptKindOptions: SelectOption[] = Object.keys(ScriptKind).map(
  (item) => ({
    label: ScriptKind[item as keyof typeof ScriptKind],
    value: ScriptKind[item as keyof typeof ScriptKind]
  })
);

export const ArgFormatSchema = z.object({
  id: z.number().default(Math.random()),
  kw: z.string().min(1, { message: 'Keyword is required' }),
  value: z.union([
    z.string().min(1, { message: 'Value is required' }),
    z.number()
  ])
});
export type ArgFormatType = z.infer<typeof ArgFormatSchema>;

const CommonScriptSchema = z.object({
  path: z.string().trim().min(1, { message: 'Path is required' }),
  initArgs: z
    .array(ArgFormatSchema)
    .optional()
    .superRefine(scriptArgRefinement),
  mainArgs: z.array(ArgFormatSchema).optional().superRefine(scriptArgRefinement)
});

export const FilesystemScriptSchema = CommonScriptSchema.merge(
  z.object({
    kind: z.literal(ScriptKind.filesystem)
  })
);
export type FilesystemScriptType = z.infer<typeof FilesystemScriptSchema>;

const BRANCH_COMMIT_OPTIONS = ['branch', 'commit'] as const;

export const GitScriptSchema = CommonScriptSchema.merge(
  z.object({
    kind: z.literal(ScriptKind.git),
    repo: z
      .string()
      .trim()
      .min(1, { message: 'Repository is required' })
      .url({ message: 'Git repository must be a valid URL' }),
    branch: z.string().optional().default(DEFAULT_GIT_BRANCH),
    commit: z.string().optional().default(''),
    selected: z.enum(BRANCH_COMMIT_OPTIONS).default('branch')
  })
);
export type GitScriptType = z.infer<typeof GitScriptSchema>;

export const ScriptSchema = z.union([FilesystemScriptSchema, GitScriptSchema]);

export type ScriptType = z.infer<typeof ScriptSchema>;
