import { z } from 'zod';

import { SolarSystemObjectName } from '../../../generated/models/solar-system-object-name';

type nonEmpty = [string, ...string[]];

export const ReferenceFrame = {
  LSRK: 'lsrk',
  BARY: 'bary',
  TOPO: 'topo'
} as const;

export type ReferenceFrameType =
  (typeof ReferenceFrame)[keyof typeof ReferenceFrame];

export const VelocityDefinition = {
  RADIO: 'Radio',
  OPTICAL: 'Optical',
  RELATIVISTIC: 'Relativistic'
} as const;

export type VelocityDefinitionType =
  (typeof VelocityDefinition)[keyof typeof VelocityDefinition];

export const RadialMotionKind = {
  VELOCITY: 'Velocity',
  REDSHIFT: 'Redshift'
} as const;

export const RadialMotionSchema = z.discriminatedUnion('kind', [
  z.object({
    kind: z.literal(RadialMotionKind.VELOCITY),
    velocity: z.coerce.number().default(0),
    referenceFrame: z
      .enum(Object.values(ReferenceFrame) as nonEmpty)
      .default(ReferenceFrame.LSRK),
    velocityDefinition: z
      .enum(Object.values(VelocityDefinition) as nonEmpty)
      .default(VelocityDefinition.OPTICAL)
  }),
  z.object({
    kind: z.literal(RadialMotionKind.REDSHIFT),
    redshift: z.coerce.number().default(0)
  })
]);

export type RadialMotionType = z.infer<typeof RadialMotionSchema>;

export const FieldPattern = {
  FIVEPOINT: 'Five Point',
  POINTINGCENTRES: 'Pointing Centres'
} as const;

export const OffsetsSchema = z.object({
  raOffset: z.coerce.number().default(0),
  decOffset: z.coerce.number().default(0)
});

export const FieldPatternSchema = z.discriminatedUnion('kind', [
  z.object({
    kind: z.literal(FieldPattern.FIVEPOINT),
    offset: z.coerce.number().gt(0).default(10)
  }),
  z.object({
    kind: z.literal(FieldPattern.POINTINGCENTRES),
    offsets: OffsetsSchema.array()
      .nonempty()
      .max(1)
      .default([OffsetsSchema.parse({})])
  })
]);

export type FieldPatternType = z.infer<typeof FieldPatternSchema>;

export const CoordinateKind = {
  ICRS: 'ICRS',
  SSO: 'Solar System Object'
} as const;

export const CoordinatesSchema = z.discriminatedUnion('kind', [
  z.object({
    kind: z.literal(CoordinateKind.ICRS),
    ra: z
      .string()
      .regex(/^\d{1,2}:\d{1,2}:\d{1,2}((\.?)|(\.\d+))$/)
      .default('00:00:00'),
    dec: z
      .string()
      .regex(/^[-+]?\d{1,2}:\d{1,2}:\d{1,2}((\.?)|(\.\d+))$/)
      .default('00:00:00')
  }),
  z.object({
    kind: z.literal('Solar System Object'),
    name: z.nativeEnum(SolarSystemObjectName).default(SolarSystemObjectName.Sun)
  })
]);

export type CoordinatesType = z.infer<typeof CoordinatesSchema>;

export const TargetSchema = z.object({
  id: z.string().trim().default(''),
  name: z.string().trim().default(''),
  addPstBeam: z.boolean().default(false),
  coordinate: CoordinatesSchema.default(
    CoordinatesSchema.parse({
      kind: CoordinateKind.ICRS
    })
  ),
  pointingPattern: FieldPatternSchema.default(
    FieldPatternSchema.parse({
      kind: FieldPattern.POINTINGCENTRES
    })
  ),
  radialMotion: RadialMotionSchema.default(
    RadialMotionSchema.parse({
      kind: RadialMotionKind.VELOCITY
    })
  )
});

export type TargetType = z.infer<typeof TargetSchema>;
