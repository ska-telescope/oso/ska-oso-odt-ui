import { describe, expect, test } from 'vitest';
import {
  TargetSchema,
  TargetType,
  CoordinateKind,
  FieldPattern,
  RadialMotionKind,
  ReferenceFrame,
  VelocityDefinition
} from './target';

describe('target', () => {
  describe('targetSchema', () => {
    test('should produce a target object with sensible defaults', () => {
      const result: TargetType = TargetSchema.parse({});
      expect(result.name).toBe('');
      expect(result.coordinate).toStrictEqual({
        kind: CoordinateKind.ICRS,
        ra: '00:00:00',
        dec: '00:00:00'
      });
      expect(result.pointingPattern).toStrictEqual({
        kind: FieldPattern.POINTINGCENTRES,
        offsets: [{ raOffset: 0, decOffset: 0 }]
      });
      expect(result.radialMotion).toStrictEqual({
        kind: RadialMotionKind.VELOCITY,
        referenceFrame: ReferenceFrame.LSRK,
        velocityDefinition: VelocityDefinition.OPTICAL,
        velocity: 0
      });
    });
  });
});
