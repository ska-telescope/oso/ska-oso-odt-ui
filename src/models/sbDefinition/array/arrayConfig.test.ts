import { describe, expect } from 'vitest';
import {
  ApertureSchema,
  SubarrayBeamSchema,
  MidArrayConfigSchema,
  LowArrayConfigSchema,
  createStationIDMap,
  LowArrayConfigTypeEnum,
  getLowDefaultConfig
  // ArrayConfigTypeEnum,
  // MidArrayConfigDishMapping,
  // ArrayConfigTypeOptions,
  // LowArrayConfigStationMapping,
  // DefaultLowArrayAA05,
  // DefaultMidArrayAA05,
  // DefaultArrayConfigAA05,
} from './arrayConfig';
import {
  EXAMPLE_LOW_AA05P2_ARRAY_INPUT,
  EXAMPLE_LOW_ARRAY_INPUT
} from 'src/store/mappers/arrayMapper.test';

const inValidArrayConfigTypeOptions = {
  substationID: '123',
  weightingKey: 1234
};

const validArrayConfigTypeOptions = {
  station_id: 123,
  aperture_id: 'abc123',
  weighting_key_ref: '1234'
};

describe('arrayConfig', () => {
  // TODO: Fix these
  // it("ArrayConfigTypeEnum should return appropriate enum values", () => {
  //   expect(ArrayConfigTypeEnum.AA05).toBe("AA0.5");
  //   expect(ArrayConfigTypeEnum.Custom).toBe("Custom");
  // });
  //
  // it("LowArrayConfigStationMapping should return appropriate string array", () => {
  //   expect(
  //     LowArrayConfigStationMapping[ArrayConfigTypeEnum.AA05],
  //   ).toStrictEqual(DefaultLowArrayAA05);
  //   expect(
  //     LowArrayConfigStationMapping[ArrayConfigTypeEnum.Custom],
  //   ).toStrictEqual([]);
  // });
  //
  // it("MidArrayConfigDishMapping should return appropriate string array", () => {
  //   expect(MidArrayConfigDishMapping[ArrayConfigTypeEnum.AA05]).toStrictEqual(
  //     DefaultMidArrayAA05,
  //   );
  //   expect(MidArrayConfigDishMapping[ArrayConfigTypeEnum.Custom]).toStrictEqual(
  //     [],
  //   );
  // });
  //
  // it("DefaultArrayConfigAA05 should return appropriate string array", () => {
  //   expect(DefaultArrayConfigAA05.stations()).toStrictEqual(
  //     DefaultLowArrayAA05.join(", "),
  //   );
  //   expect(DefaultArrayConfigAA05.dishes()).toStrictEqual(
  //     DefaultMidArrayAA05.join(", "),
  //   );
  // });
  //
  // it("ArrayConfigTypeOptions should return appropriate string array", () => {
  //   expect(ArrayConfigTypeOptions).toStrictEqual([
  //     { label: "AA0.5", value: "AA05" },
  //     { label: "Custom", value: "Custom" },
  //   ]);
  // });

  describe('ApertureSchema', () => {
    it('with invalid object should throw exception', () => {
      expect(() =>
        ApertureSchema.parse(inValidArrayConfigTypeOptions)
      ).toThrowError();
    });

    it('with valid object should return true/successful', () => {
      expect(() =>
        ApertureSchema.parse(validArrayConfigTypeOptions)
      ).toBeTruthy();
    });
  });

  describe('SubarrayBeamSchema', () => {
    it('with invalid object should throw exception', () => {
      expect(() =>
        SubarrayBeamSchema.parse({
          subarray_beam_id: 123,
          bandwidth_in_mhz: 1234,
          apertures: [inValidArrayConfigTypeOptions],
          station: []
        })
      ).toThrowError();
    });

    it('with valid object should return true/successful', () => {
      expect(() =>
        SubarrayBeamSchema.parse({
          subarray_beam_id: '123',
          bandwidth_in_mhz: 0.78125,
          apertures: [validArrayConfigTypeOptions],
          station: []
        })
      ).toBeTruthy();
    });
  });

  describe('createStationIDMap', () => {
    it('generates expected station ID to label mapping', () => {
      const expectedStationIDMap = new Map<number, string>([
        [1, 'C1'],
        [224, 'C224'],
        [225, 'E1-1'],
        [259, 'N2-5'],
        [344, 'S7-6'],
        [512, 'N16-6']
      ]);
      const generatedStationIDMap = createStationIDMap([
        1, 224, 225, 259, 344, 512
      ]);
      expect(generatedStationIDMap).toStrictEqual(expectedStationIDMap);
    });
  });

  describe('MidArrayConfigSchema', () => {
    it('with empty dishes id should throw an required exception', () => {
      expect(() =>
        MidArrayConfigSchema.parse({
          subarray_configuration: 'ABC',
          dishes: ''
        })
      ).toThrowError();
    });

    it('with invalid object should throw field expression exception', () => {
      expect(() =>
        MidArrayConfigSchema.parse({
          subarray_configuration: 'ABC',
          dishes: '123'
        })
      ).toThrowError();
    });

    it('with valid object should return true/successful', () => {
      expect(() =>
        MidArrayConfigSchema.parse({
          subarray_configuration: 'AA0.5',
          dishes: 'SKA0001, , MKT0062, '
        })
      ).toBeTruthy();
    });
  });

  describe('LowArrayConfigSchema', () => {
    it('with empty stations should throw a required exception', () => {
      expect(() =>
        LowArrayConfigSchema.parse({
          subarray_configuration: 'ABC',
          stations: ''
        })
      ).toThrowError();
    });

    it('with invalid object should throw express failure exception', () => {
      expect(() =>
        LowArrayConfigSchema.parse({
          subarray_configuration: 'ABC',
          stations: 'ANX123'
        })
      ).toThrowError();
    });

    it('with valid object should return true/successful', () => {
      expect(() =>
        LowArrayConfigSchema.parse({
          subarray_configuration: 'AA0.5',
          stations: '0001, , 0062'
        })
      ).toBeTruthy();
    });
  });

  describe('getLowDefaultConfig', () => {
    test.each([
      [LowArrayConfigTypeEnum.AA05P1, EXAMPLE_LOW_ARRAY_INPUT],
      [
        LowArrayConfigTypeEnum.Custom,
        {
          ...EXAMPLE_LOW_ARRAY_INPUT,
          configuration: LowArrayConfigTypeEnum.Custom
        }
      ],
      [LowArrayConfigTypeEnum.AA05P2, EXAMPLE_LOW_AA05P2_ARRAY_INPUT]
    ])(
      'should return default SubarrayBeam and Aperture setup for the given array',
      (inputArray, expectedDefaultValue) => {
        const defaultConfig = getLowDefaultConfig(inputArray);
        expect(defaultConfig).toStrictEqual(expectedDefaultValue);
      }
    );
  });
});
