import { z } from 'zod';
import { ObservingBlockSchema } from './observingBlock';

const ProjectType = z.enum([
  'Science',
  'Commissioning',
  'Engineering',
  'Calibration'
]);

export const ProjectSchema = z.object({
  name: z.string(),
  code: z.string().optional(),
  type: ProjectType,
  status: z.string(),
  observingBlocks: z.array(ObservingBlockSchema)
});

export type ProjectType = z.infer<typeof ProjectSchema>;
