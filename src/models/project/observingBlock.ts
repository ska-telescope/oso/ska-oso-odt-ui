import { z } from 'zod';

export const ObsBlockSbDefinitionSchema = z.object({
  id: z.string(),
  status: z.string().default('TODO')
});

export const ObservingBlockSchema = z.object({
  id: z.string(),
  name: z.string(),
  status: z.string().default('TODO'),
  sbDefinitions: z.array(ObsBlockSbDefinitionSchema).default([])
});

export type ObservingBlockType = z.infer<typeof ObservingBlockSchema>;
