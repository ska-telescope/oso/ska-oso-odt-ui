/// <reference types="vitest" />
/// <reference types="vite/client" />
import path from 'path';
import { defineConfig, UserConfigExport } from 'vite';
import react from '@vitejs/plugin-react';
import viteTsconfigPaths from 'vite-tsconfig-paths';
import svgr from 'vite-plugin-svgr';

// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  plugins: [
    react(),
    svgr({
      include: '**/*.svg?react'
    }),
    viteTsconfigPaths()
  ],
  test: {
    coverage: {
      reporter: ['cobertura', 'text'],
      include: ['src'],
      exclude: [
        '**/*.cy.*',
        'src/setUpTests.ts',
        '**/test.ts*',
        'src/generated/**/*.*'
      ],
      thresholds: {
        lines: 75,
        functions: 70,
        branches: 75,
        statements: 75,
      },
    },
    deps: {
      moduleDirectories: ['node_modules', path.resolve('../../packages')]
    },
    environment: 'happy-dom',
    globals: true,
    reportOnFailure: true,
    setupFiles: './src/setUpTests.ts'
  }
} as UserConfigExport);
