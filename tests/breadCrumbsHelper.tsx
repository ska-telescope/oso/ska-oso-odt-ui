import { screen, within } from '@testing-library/react';
import { expect } from 'vitest';

const checkStepper = (stepNum: number) => {
  const stepper = within(
    screen.getByTestId('step' + stepNum.toString())
  ).getByText(stepNum);
  // expect(stepper).toHaveAttribute("x", "12");
  expect(stepper).toHaveAttribute('y', '12');
  expect(stepper).toHaveAttribute('text-anchor', 'middle');
  expect(stepper).toHaveAttribute('dominant-baseline', 'central');
};

export const checkBreadCrumbsSteps = () => {
  // check 7 x stepper circle
  [1, 2, 3, 4, 5, 6, 7].forEach((breadCrumb) => {
    checkStepper(breadCrumb);
  });
};

export const checkBreadCrumbsTitles = () => {
  // check 7 x stepper title
  [
    'Details',
    'Scripts',
    'Targets',
    'Array',
    'Signal Processor',
    'Scans',
    'Data Processing'
  ].forEach((title: string) => {
    expect(screen.getByText(title)).toBeInTheDocument();
  });
};

export const checkBreadCrumbs = () => {
  // checkBreadCrumbsSteps();
  checkBreadCrumbsTitles();
};
