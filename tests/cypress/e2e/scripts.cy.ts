import { getDefaultSbDefinition } from '../../../src/store/defaults';

context('Scripts testing', () => {
  beforeEach(() => {
    // Setup the local storage with expected data
    window.localStorage.clear();
    window.localStorage.setItem(
      'sbd-store',
      JSON.stringify({
        state: {
          sb: getDefaultSbDefinition('ska_low'),
          sbDefinition: getDefaultSbDefinition('ska_low')
        }
      })
    );

    cy.visit('/sbd/general');
  });

  it('in low sub-pages, click script bread crumb should redirect to the low scripts page', () => {
    // Check we are starting at the low general page
    cy.url().should('contain', '/sbd/general');
    // Is the Scripts breadcrumb visible
    cy.get('[data-testid="Scripts"]').should('be.visible');
    // Click the Scripts breadcrum to switch to the scripts page.
    cy.get('[data-testid="Scripts"]').click();
    // Check it switched correctly
    cy.url().should('contain', '/sbd/scripts');
    // Do we have the scripts form object displayed
    cy.get('[data-testid="sbScript"]').should('be.visible');
    // Get hold of the table object and change a value.
    // Note: this is WIP at this time.
    // cy.get('[testId="initStageArgsId"]').should("be.visible");
  });

  // Note: Example of entering tables values for the future.

  // it('should change the value of a specific cell', () => {
  //   // Locate the cell to edit (e.g., row 1, column "Name")
  //   cy.get('[data-rowindex="0"] [data-colindex="1"]') // Adjust the selectors as necessary
  //     .dblclick(); // Enter edit mode by double-clicking the cell

  //   // Type the new value
  //   cy.get('[data-rowindex="0"] [data-colindex="1"] input') // Adjust the selector for the input field within the cell
  //     .clear()
  //     .type('New Value')
  //     .blur(); // Save changes by triggering a blur event

  //   // Verify that the cell value has been updated
  //   cy.get('[data-rowindex="0"] [data-colindex="1"]')
  //     .should('contain', 'New Value');
  // });
});
