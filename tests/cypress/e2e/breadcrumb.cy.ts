import Mui from '../support/mui';
import Url from '../support/url';
import {
  getDefaultMidSBDefinition,
  getDefaultLowSBDefinition
} from '../../../src/store/defaults';

const midTests = [
  {
    url: '/sbd/scripts',
    id: '[data-testid="General"]',
    redirect: '/sbd/general'
  },
  {
    url: '/sbd/general',
    id: '[data-testid="Scripts"]',
    redirect: '/sbd/scripts'
  },
  { url: '/sbd/general', id: '[data-testid="Array"]', redirect: '/sbd/array' },
  {
    url: '/sbd/general',
    id: '[data-testid="Targets"]',
    redirect: '/sbd/targets'
  },
  {
    url: '/sbd/general',
    id: '[data-testid="Signal Processor"]',
    redirect: '/sbd/signalprocessor'
  },
  { url: '/sbd/general', id: '[data-testid="Scans"]', redirect: '/sbd/scans' },
  {
    url: '/sbd/general',
    id: '[data-testid="Data Processing"]',
    redirect: '/sbd/dataprocessing'
  }
];

context('MID Bread crumb navigation', () => {
  beforeEach(() => {
    window.localStorage.clear();
    window.localStorage.setItem(
      'sbd-store',
      JSON.stringify({
        state: {
          sb: getDefaultMidSBDefinition(),
          sbDefinition: getDefaultMidSBDefinition()
        }
      })
    );
    Url.openPage('/sbd/general');
  });

  it('in mid sub-pages, loop through all the sub pages should redirect to the appropriate page', () => {
    midTests.forEach((page) => {
      Url.openPage(page.url);
      Mui.click(page.id);
      Url.shouldContain(page.redirect);
    });
  });
});

const lowTests = [
  {
    url: '/sbd/scripts',
    id: '[data-testid="General"]',
    redirect: '/sbd/general'
  },
  {
    url: '/sbd/general',
    id: '[data-testid="Scripts"]',
    redirect: '/sbd/scripts'
  },
  { url: '/sbd/general', id: '[data-testid="Array"]', redirect: '/sbd/array' },
  {
    url: '/sbd/general',
    id: '[data-testid="Targets"]',
    redirect: '/sbd/targets'
  },
  {
    url: '/sbd/general',
    id: '[data-testid="Signal Processor"]',
    redirect: '/sbd/signalprocessor'
  },
  { url: '/sbd/general', id: '[data-testid="Scans"]', redirect: '/sbd/scans' },
  {
    url: '/sbd/general',
    id: '[data-testid="Data Processing"]',
    redirect: '/sbd/dataprocessing'
  }
];

context('LOW Bread crumb navigation', () => {
  beforeEach(() => {
    window.localStorage.clear();
    window.localStorage.setItem(
      'sbd-store',
      JSON.stringify({
        state: {
          sb: getDefaultLowSBDefinition(),
          sbDefinition: getDefaultLowSBDefinition()
        }
      })
    );

    Url.openPage('/sbd/general');
  });

  it('in low sub-pages, loop through all the sub pages should redirect to the appropriate page', () => {
    lowTests.forEach((page) => {
      Url.openPage(page.url);
      Mui.click(page.id);
      Url.shouldContain(page.redirect);
    });
  });
});
