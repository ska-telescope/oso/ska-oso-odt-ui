import Url from '../support/url';
import { getDefaultMidSBDefinition } from '../../../src/store/defaults';

context(
  'General page fields for Mid can be modified and are stored in ODA',
  () => {
    beforeEach(() => {
      window.localStorage.clear();
      window.localStorage.setItem(
        'sbd-store',
        JSON.stringify({
          state: {
            sb: getDefaultMidSBDefinition(),
            sbDefinition: getDefaultMidSBDefinition()
          }
        })
      );
      Url.openPage('/sbd/general');
    });

    it('Verify info is shown after saving to ODA', () => {
      cy.getByLabel('Created By')
        .should('be.disabled')
        .should('have.prop', 'value')
        .should('be.empty');
      cy.getByLabel('Created On')
        .should('be.disabled')
        .should('have.prop', 'value')
        .should('be.empty');
      cy.getByLabel('Last Modified On')
        .should('be.disabled')
        .should('have.prop', 'value')
        .should('be.empty');
      cy.getByLabel('Last Modified By')
        .should('be.disabled')
        .should('have.prop', 'value')
        .should('be.empty');
      cy.getByLabel('SBD ID')
        .should('be.disabled')
        .should('have.prop', 'value')
        .should('be.empty');
      cy.getByLabel('Version')
        .should('be.disabled')
        .should('have.prop', 'value')
        .should('be.empty');

      cy.contains('button', 'Save to ODA').click();

      // Saving to ODA should have populated static fields with metadata
      cy.getByLabel('Created By')
        .should('have.prop', 'value')
        .should('not.be.empty');
      cy.getByLabel('Created On')
        .should('have.prop', 'value')
        .should('not.be.empty');
      cy.getByLabel('Last Modified On')
        .should('have.prop', 'value')
        .should('not.be.empty');
      cy.getByLabel('Last Modified By')
        .should('have.prop', 'value')
        .should('not.be.empty');
      cy.getByLabel('SBD ID')
        .should('have.prop', 'value')
        .should('not.be.empty');
      cy.getByLabel('Version')
        .should('have.prop', 'value')
        .should('not.be.empty');
    });
  }
);
