class Url {
  static openPage(page) {
    cy.visit(page);
  }

  static shouldContain(path) {
    cy.url().should('contain', path);
  }

  static shouldEqual(path) {
    cy.url().should('eq', path);
  }

  static externalPathChanged(path) {
    cy.on('url:changed', () => {
      // Wait for the URL to change and match the expected path, with a custom timeout
      cy.location('pathname').should('eq', path);
    });
  }

  static pathChanged(path) {
    cy.url().should('include', path);
  }

  static loadingCompleted(done) {
    cy.on('window:load', () => {
      done();
    });
  }

  static log(message) {
    cy.task('log', '\t---> ' + message);
  }
}

export default Url;
