import Chainable = Cypress.Chainable;

declare namespace Cypress {
  interface Chainable {
    getByLabel(param: unknown): Chainable;
    getErrorByLabel(param: unknown): Chainable;
  }
}

function getByLabel(label: unknown, insideSelector: unknown = null): Chainable {
  // If insideSelector is given then the label will be searched for only within that element.
  // If not it will be searched from the root.
  const elementToSearch = insideSelector ? cy.get(insideSelector) : cy;
  return elementToSearch
    .contains('label', new RegExp('^' + label + '[ *]*$'))
    .invoke('attr', 'for')
    .then((id) => {
      cy.get('#' + id);
    });
}

function getErrorByLabel(label: unknown): Chainable {
  return cy
    .contains('label', new RegExp('^' + label + '[ *]*$'))
    .invoke('attr', 'for')
    .then((id) => {
      cy.get('#' + id)
        .invoke('attr', 'aria-describedby')
        .then((error_id) => {
          cy.get('#' + error_id);
        });
    });
}

Cypress.Commands.add('getByLabel', getByLabel);
Cypress.Commands.add('getErrorByLabel', getErrorByLabel);
