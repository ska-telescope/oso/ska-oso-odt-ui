Changelog
==========

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](http://semver.org/).

Unreleased
**********
* Added support for multiple apertures for SKA LOW

2.0.0
**********
* Added Home and OB page
* Added ability to get the source coordinates from the source name via Simbad or NED
* Added OSD integration - Frequency Band information is now obtained from OSD by querying the new oso-services endpoint

1.1.0
**********

* Update to backend using PDM version 17 and ODA version 7

1.0.0
**********

* Minimal AA0.5 version of the application, rewritten using React.
