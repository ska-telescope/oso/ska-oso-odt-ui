import { defineConfig } from 'cypress';
import vitePreprocessor from 'cypress-vite';

export default defineConfig({
  video: true,
  fixturesFolder: 'tests/cypress/fixtures',
  screenshotsFolder: 'tests/cypress/artefacts/screenshots',
  videosFolder: 'tests/cypress/artefacts/videos',
  downloadsFolder: 'tests/cypress/artefacts/downloads',
  e2e: {
    baseUrl: 'http://localhost:8090',
    supportFile: 'tests/cypress/support/e2e.ts',
    specPattern: [
      'tests/cypress/e2e/**/*.cy.{js,jsx,ts,tsx}'
    ],
    setupNodeEvents(on) {
      on('file:preprocessor', vitePreprocessor())
    }
  },
  retries: {
    runMode: 2,
    openMode: 0
  }
});
