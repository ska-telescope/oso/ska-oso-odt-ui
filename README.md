# SKA OSO ODT UI

This project is the User Interface for the Observation Design Tool. It allows users to create, edit and persist data about an observation in Scheduling Blocks.
The UI communicates with the ODT Services backend, which connects to the ODA. See RTD for more details.

## Local development and testing

### Config SKA repositories to be a part of the React packages search path

To allow for the SKA libraries to be picked up when you re-install packages,
run `yarn config set @ska-telescope:registry https://artefact.skao.int/repository/npm-internal/`

### Update to the latest SKA repositories in the project

Run `yarn skao:update` to pull in latest SKA repository dependencies to the project

### Installing project dependencies

Run `yarn install` to install the latest project dependencies from package.json and yarn.lock

### Running a front-end development server

Run `yarn start` for a dev server. Navigate to `http://localhost:8090/`.
The app will automatically reload if you change any of the source files.

### Connecting to ODT back-end from front-end development server

The `yarn start` script runs two commands: `make set-dev-env-vars` to set the URL for the ODT back-end 
and `webpack serve` to start the development server. The `make set-dev-env-vars` defaults the back-end
URL to `http://<minikube-ip>/ska-oso-odt-ui/odt/api/v1/sbds` which is where the back-end should be running 
if it has been started with `make k8s-install-chart`.

To use a back-end deployed elsewhere (e.g. localhost), include the back-end URL in the `yarn start` 
command, for example `BACKEND_URL=http://localhost/odt/api/v1/sbds yarn start`.

### Running tests

1. First start up the front-end development server using `yarn start`

2. Run `yarn cypress:run` to execute the Cypress tests.

### Running static code analysis

Run `yarn lint` to lint the code.

## Code Generation

The API for ska-oso-odt-services is defined in an OpenAPI specification. 
This is then used to generate an API client and the model interfaces for the UI project.
If the API definition has changed, the code generation needs to be redone in this project, by the following steps (there are a few manual steps in the process for now):

1. Get the OpenAPI JSON from `http://<minikube-ip>/<namespace>/oso/api/v0/openapi.json` and copy to `local` directory 
   at the root of this project
2. Convert JSON file to YAML with `yq -p json -o yaml local/odtopenapi.json >> local/odtopenapi.yaml`. NOTE: Modify the 
   `paths` in the YAML file to not include the namespace.
3. Run `make models` which will use the openapitools/openapi-generator-cli Docker image to generate the code in a container, 
   which is mounted so that the code will be available in the local src directory. NOTE: Currently some manual changes are 
   required to the generated files so check the generated files and undo any unexpected changes.

## Deploying to Kubernetes

The full production system will consist of the ODT UI using the ODT Services, which are
configured to connect to the ODA REST API which connects with a PostgreSQL instance.
To deploy all of these services, run:

```
make oci-build
```

The umbrella Helm chart can then be deployed with

```
make k8s-install-chart
```

and uninstalled with

```
make k8s-uninstall-chart
```

Once installed, the UI should then be available externally at http://<MINIKUBE_IP>/<KUBE_NAMESPACE>/odt/

If using minikube, run `minikube ip` to find the host IP. `KUBE_NAMESPACE` is set to `ska-oso-odt-ui` by default.  
The backend component will also be deployed to a separate pod, which the web application will make requests to.

### Minimal Developer Deployment

ODT UI depends on several other software components for correct functionality.
A lightweight developer deployment of the ODT UI and its dependencies (no
PostgreSQL, filesystem ODA backend, etc.) can be launched and stopped with the
`make dev-up` and `make dev-down` commands.

```
# launch services ODT UI depends on in a new Kubernetes namespace
make dev-up

# confirm backend is up by asking it to create a new SB
curl `minikube ip`/ska-oso-odt-ui/odt/api/v1/sbds/create

# stop dependencies, delete namespace
make dev-down
```

### Including an unpublished Helm chart for an ODT UI dependency

The ska-oso-odt-ui Helm chart depends on the Helm charts of several other components:

1. ska-oso-odt-services to provide ODT backend services
2. ska-ser-skuid for UID generation for new OSO entities (SBDs, projects, etc.)
3. ska-db-oda for real database persistence

To use a WIP chart for one of these dependencies, first create a Gitlab token with read_api privileges following
the instructions at https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html. Then, add the Gitlab
Helm repository for the project you want to source an unpublished chart from. For example, to add the Helm
chart repository for the ODT services project, run

```
helm repo add --username <username> --password <gitlab token> ska-oso-odt-services https://gitlab.com/api/v4/projects/19329547/packages/helm/dev
helm repo update
```

Finally, edit the umbrella chart definition (found in `charts/ska-oso-odt-ui-umbrella/Chart.yaml`), modifying
the version and repository definitions to point to the dependency chart you want to source. For instance, having
added the Gitlab Helm repository for ska-oso-odt-services above, we can now include an unofficial chart from
that project's ska-oso-odt-services Helm registry.

```
...
- name: ska-oso-odt-services
  version: 0.0.0-dev.c86eacb3f
  repository: https://gitlab.com/api/v4/projects/19329547/packages/helm/dev
...
```

## Documentation

[![Documentation Status](https://readthedocs.org/projects/ska-telescope-ska-oso-odt-ui/badge/?version=latest)](https://developer.skao.int/projects/ska-oso-odt-ui/en/latest/?badge=latest)

Documentation can be found in the `docs` folder. To build docs, install the
documentation specific requirements:

```
pip3 install -r docs/requirements.txt
```

and build the documentation (will be built in docs/build folder) with

```
make docs-build html
```
